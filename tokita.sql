-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2015 at 03:47 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tokita`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin@tokita.com', '$2y$10$CNoyE8UZrj7O.L1VPqdy4uRRSjZp671a92W5eWcXO0T2S8c7TCdmS', '2015-09-24 01:45:27', '2015-10-04 03:21:36');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `carts_customer_id_foreign` (`customer_id`),
  KEY `carts_item_id_foreign` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=57 ;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `quantity`, `customer_id`, `item_id`, `status`, `created_at`, `updated_at`) VALUES
(35, 3, 7, 4, 1, '2015-09-23 20:26:40', '2015-09-28 22:40:59'),
(36, 2, 7, 4, 1, '2015-09-28 23:18:07', '2015-09-29 01:07:32'),
(37, 2, 7, 4, 1, '2015-10-01 21:23:52', '2015-10-01 21:41:55'),
(38, 2, 7, 4, 1, '2015-10-01 21:47:48', '2015-10-01 21:48:56'),
(39, 2, 7, 3, 1, '2015-10-01 21:50:04', '2015-10-01 21:50:47'),
(40, 1, 7, 3, 1, '2015-10-02 00:05:49', '2015-10-02 00:06:32'),
(41, 1, 7, 3, 1, '2015-10-02 00:42:45', '2015-10-02 00:43:24'),
(42, 4, 7, 4, 1, '2015-10-02 01:49:30', '2015-10-02 01:50:20'),
(43, 3, 7, 3, 1, '2015-10-02 01:49:40', '2015-10-02 01:50:20'),
(44, 3, 7, 3, 1, '2015-10-02 01:56:41', '2015-10-02 04:31:01'),
(45, 1, 7, 4, 1, '2015-10-02 04:30:21', '2015-10-02 04:31:01'),
(46, 1, 7, 3, 1, '2015-10-02 04:31:44', '2015-10-02 04:32:57'),
(47, 1, 7, 4, 1, '2015-10-02 04:31:53', '2015-10-02 04:32:57'),
(48, 1, 7, 3, 1, '2015-10-02 22:24:55', '2015-10-02 22:25:38'),
(49, 1, 7, 3, 1, '2015-10-10 03:21:46', '2015-10-10 03:22:34'),
(50, 2, 7, 4, 1, '2015-10-10 03:28:16', '2015-10-10 03:28:54'),
(51, 2, 7, 4, 1, '2015-10-10 03:30:51', '2015-10-10 03:31:30'),
(52, 1, 7, 3, 1, '2015-10-10 03:49:53', '2015-10-10 03:50:35'),
(53, 2, 7, 694, 1, '2015-10-12 05:54:36', '2015-10-12 05:55:35'),
(54, 1, 7, 690, 1, '2015-10-12 05:57:57', '2015-10-12 05:58:30'),
(55, 1, 7, 696, 1, '2015-10-12 06:02:11', '2015-10-12 06:03:01'),
(56, 1, 7, 690, 1, '2015-10-12 06:04:32', '2015-10-12 06:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `cart_transactions`
--

CREATE TABLE IF NOT EXISTS `cart_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) unsigned NOT NULL,
  `transaction_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `cart_transactions_cart_id_foreign` (`cart_id`),
  KEY `cart_transactions_transaction_id_foreign` (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `cart_transactions`
--

INSERT INTO `cart_transactions` (`id`, `cart_id`, `transaction_id`, `created_at`, `updated_at`) VALUES
(1, 35, 1, '2015-09-28 22:40:58', '2015-09-28 22:40:58'),
(2, 36, 2, '2015-09-29 01:07:32', '2015-09-29 01:07:32'),
(3, 37, 3, '2015-10-01 21:41:55', '2015-10-01 21:41:55'),
(4, 38, 4, '2015-10-01 21:48:56', '2015-10-01 21:48:56'),
(5, 39, 5, '2015-10-01 21:50:47', '2015-10-01 21:50:47'),
(6, 40, 6, '2015-10-02 00:06:32', '2015-10-02 00:06:32'),
(7, 40, 7, '2015-10-02 00:07:36', '2015-10-02 00:07:36'),
(8, 40, 8, '2015-10-02 00:07:48', '2015-10-02 00:07:48'),
(9, 40, 9, '2015-10-02 00:10:51', '2015-10-02 00:10:51'),
(10, 40, 10, '2015-10-02 00:16:49', '2015-10-02 00:16:49'),
(11, 40, 11, '2015-10-02 00:18:26', '2015-10-02 00:18:26'),
(12, 40, 12, '2015-10-02 00:20:59', '2015-10-02 00:20:59'),
(13, 40, 13, '2015-10-02 00:21:16', '2015-10-02 00:21:16'),
(14, 40, 14, '2015-10-02 00:22:37', '2015-10-02 00:22:37'),
(15, 41, 15, '2015-10-02 00:43:24', '2015-10-02 00:43:24'),
(16, 41, 16, '2015-10-02 00:43:54', '2015-10-02 00:43:54'),
(17, 42, 17, '2015-10-02 01:50:20', '2015-10-02 01:50:20'),
(18, 43, 17, '2015-10-02 01:50:20', '2015-10-02 01:50:20'),
(19, 44, 18, '2015-10-02 04:31:01', '2015-10-02 04:31:01'),
(20, 45, 18, '2015-10-02 04:31:01', '2015-10-02 04:31:01'),
(21, 46, 19, '2015-10-02 04:32:57', '2015-10-02 04:32:57'),
(22, 47, 19, '2015-10-02 04:32:57', '2015-10-02 04:32:57'),
(23, 48, 20, '2015-10-02 22:25:38', '2015-10-02 22:25:38'),
(24, 49, 21, '2015-10-10 03:22:34', '2015-10-10 03:22:34'),
(25, 49, 22, '2015-10-10 03:23:57', '2015-10-10 03:23:57'),
(26, 50, 23, '2015-10-10 03:28:54', '2015-10-10 03:28:54'),
(27, 51, 24, '2015-10-10 03:31:30', '2015-10-10 03:31:30'),
(28, 52, 25, '2015-10-10 03:50:35', '2015-10-10 03:50:35'),
(29, 53, 26, '2015-10-12 05:55:34', '2015-10-12 05:55:34'),
(30, 54, 27, '2015-10-12 05:58:30', '2015-10-12 05:58:30'),
(31, 55, 28, '2015-10-12 06:03:01', '2015-10-12 06:03:01'),
(32, 55, 29, '2015-10-12 06:03:18', '2015-10-12 06:03:18'),
(33, 56, 30, '2015-10-12 06:07:00', '2015-10-12 06:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Minuman', '2015-06-28 04:32:01', '2015-09-13 23:43:52'),
(2, 'Makanan', '2015-06-28 04:32:15', '2015-06-28 04:32:15'),
(3, 'Aneka Mie', '2015-06-28 04:32:30', '2015-06-28 04:32:30'),
(4, 'Cemilan', '2015-06-28 04:32:47', '2015-06-28 04:32:47'),
(5, 'Sabun dan Kecantikan', '2015-06-28 04:33:03', '2015-06-28 04:33:03'),
(6, 'Peralatan RT', '2015-06-28 04:33:18', '2015-06-28 04:33:18'),
(7, 'Obat-Obatan', '2015-06-28 04:33:32', '2015-06-28 04:33:32'),
(8, 'Alat Tulis & Kantor', '2015-06-28 04:33:50', '2015-06-28 04:33:50'),
(9, 'Bayi & Anak', '2015-06-28 04:34:07', '2015-06-28 04:34:07'),
(10, 'Kebersihan Rumah', '2015-06-28 04:34:22', '2015-06-28 04:34:22'),
(11, 'Sembako', '2015-06-28 04:34:39', '2015-06-28 04:34:39'),
(12, 'Bahan & Bumbu', '2015-06-28 04:34:54', '2015-06-28 04:34:54'),
(13, 'Es Krim', '2015-06-28 04:35:10', '2015-06-28 04:35:10'),
(14, 'Lain-lain', '2015-06-28 04:35:24', '2015-06-28 04:35:24');

-- --------------------------------------------------------

--
-- Table structure for table `claimed_coupons`
--

CREATE TABLE IF NOT EXISTS `claimed_coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `coupon_id` int(10) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `claimed_coupons_customer_id_foreign` (`customer_id`),
  KEY `claimed_coupons_coupon_id_foreign` (`coupon_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `claimed_coupons`
--

INSERT INTO `claimed_coupons` (`id`, `customer_id`, `coupon_id`, `status`, `identifier`, `created_at`, `updated_at`) VALUES
(1, 7, 1, 2, '486bccc0-6657-11e5-8f66-c1c565c36aa7', '2015-09-28 20:07:56', '2015-09-29 01:07:32'),
(2, 7, 1, 5, 'c0c31360-68bf-11e5-974e-eb326d889edc', '2015-10-01 21:40:48', '2015-10-01 21:50:47'),
(3, 7, 1, 19, '37c12220-68f9-11e5-8658-69a4a4ac1d29', '2015-10-02 04:32:09', '2015-10-02 04:32:57'),
(4, 7, 1, 0, 'c03901a0-6975-11e5-acc9-19ebf1f5005b', '2015-10-02 19:23:36', '2015-10-02 19:23:36');

-- --------------------------------------------------------

--
-- Table structure for table `claimed_gifts`
--

CREATE TABLE IF NOT EXISTS `claimed_gifts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `gift_id` int(10) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `claimed_gifts_customer_id_foreign` (`customer_id`),
  KEY `claimed_gifts_gift_id_foreign` (`gift_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `claimed_gifts`
--

INSERT INTO `claimed_gifts` (`id`, `customer_id`, `gift_id`, `status`, `identifier`, `created_at`, `updated_at`) VALUES
(1, 7, 1, 1, 'a72a5a00-68b4-11e5-b048-679a454f8d2a', '2015-10-01 20:21:21', '2015-10-10 01:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nominal` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `point` int(11) NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `nominal`, `created_at`, `updated_at`, `point`, `thumbnail`) VALUES
(1, 10000, '2015-05-30 10:50:25', '2015-05-30 10:50:25', 5, 'coupon1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_remember` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `point` int(11) NOT NULL,
  `verify_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_name`, `customer_email`, `customer_password`, `customer_remember`, `customer_address`, `created_at`, `updated_at`, `point`, `verify_token`) VALUES
(7, 'Destra Bintang P', '13511057@std.stei.itb.ac.id', '$2y$10$mLuWcyal1ueWtV0WTsJzZe09eNM6r4AaflPQ9s7GiusN6wWtS2VfO', '', 'Jalan lagi.', '2015-06-07 09:26:27', '2015-10-12 06:07:00', 619, '$2y$10$d14a9wPyEng6CP1F69hsMOmR0zS14Q3yBGg36qLPnDhz799je3c.G'),
(8, 'Chrestella Stephanie', 'chrestellastephanie@gmail.com', '$2y$10$Vdfb8pb5jdkZEp3n70gAmuwV7uV2gJL3Z5Vd8jO74W6DyukpKP7jS', '', 'Komplek Perumahan xxx', '2015-06-07 09:56:30', '2015-06-07 09:56:30', 0, ''),
(9, 'a', 'destra.b.p@gmail.com', '$2y$10$J4Z7YclVdTjTJCSzIvbvheCC8o0PswSLBaXrSWIekaWcUA1cypK8W', '', '1234567890', '2015-10-12 05:38:40', '2015-10-12 05:38:40', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `favorites_customer_id_foreign` (`customer_id`),
  KEY `favorites_item_id_foreign` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `customer_id`, `item_id`, `created_at`, `updated_at`) VALUES
(2, 7, 3, '2015-10-04 03:51:41', '2015-10-04 03:51:41');

-- --------------------------------------------------------

--
-- Table structure for table `gifts`
--

CREATE TABLE IF NOT EXISTS `gifts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `point` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gifts`
--

INSERT INTO `gifts` (`id`, `created_at`, `updated_at`, `point`, `name`, `thumbnail`) VALUES
(1, '2015-05-30 10:51:42', '2015-05-30 10:51:42', 20, 'Piring Cantik', 'gift1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `image_sliders`
--

CREATE TABLE IF NOT EXISTS `image_sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `image_sliders`
--

INSERT INTO `image_sliders` (`id`, `name`, `image`, `created_at`, `updated_at`, `type`, `mobile_image`) VALUES
(1, 'slide1', 'slider1.jpg', '2015-10-04 14:41:44', '2015-10-04 14:41:44', 'slider', 'slider_m1.jpg'),
(2, 'slide2', 'slider2.jpg', '2015-10-04 14:41:56', '2015-10-04 14:41:56', 'slider', 'slider_m2.jpg'),
(3, 'slide3', 'slider3.jpg', '2015-10-04 14:42:06', '2015-10-04 14:42:06', 'slider', 'slider_m3.jpg'),
(4, 'slide4', 'slider4.jpg', '2015-10-04 14:42:15', '2015-10-04 14:42:15', 'banner', 'slider_m4.jpg'),
(5, 'banner1', 'banner1.jpg', '2015-10-05 02:47:50', '2015-10-05 02:47:50', 'banner', 'banner_m1.jpg'),
(6, 'banner2', 'banner2.jpg', '2015-10-05 02:47:59', '2015-10-05 02:47:59', 'banner', 'banner_m2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_price` int(11) NOT NULL,
  `discounted_item_price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `subcategory_id` int(10) unsigned NOT NULL,
  `stock` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `barcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `items_category_id_foreign` (`category_id`),
  KEY `items_subcategory_id_foreign` (`subcategory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1036 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_name`, `item_thumbnail`, `item_price`, `discounted_item_price`, `created_at`, `updated_at`, `description`, `category_id`, `subcategory_id`, `stock`, `views`, `barcode`) VALUES
(3, 'Milo', 'pr2.jpg', 50000, 40000, '2015-06-28 05:05:05', '2015-10-12 06:04:26', 'Milo 200 gram.', 11, 11, 0, 85, ''),
(4, 'Pulpy Oranges', 'pr1.jpg', 7000, 6000, '2015-06-28 05:06:22', '2015-10-12 05:57:51', 'Pulpy Orange 500 ml.', 1, 10, 0, 87, ''),
(5, 'TALI RAFIA SEDANG CAP IKAN LOUHAN', '', 1500, 1500, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 1, 10, 0, '000255022010'),
(6, 'GULA PASIR LOKAL 1 KG', '', 13850, 13850, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 2, 10, 0, '02880051'),
(7, 'KUSUKA RASA AYAM LADA HITAM 60 G ', '', 4300, 4300, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 3, 10, 0, '053314502227'),
(8, 'MIE TELUR CAP 3 AYAM 200 G', '', 3950, 3950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 4, 10, 0, '089686000061'),
(9, 'INDOMIE RASA AYAM BAWANG 69 G', '', 1950, 1950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 5, 10, 0, '089686010015'),
(10, 'INDOMIE RASA AYAM SPECIAL 68 G', '', 2000, 2000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 6, 10, 0, '089686010046'),
(11, 'INDOMIE SOTO MIE 70 G', '', 1950, 1950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 7, 10, 0, '089686010343'),
(12, 'INDOMIE RASA KARI AYAM 72 G', '', 2100, 2100, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 8, 10, 0, '089686010527'),
(13, 'INDOMIE GORENG RASA CABE IJO 85 G', '', 2250, 2250, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 9, 10, 0, '089686010718'),
(14, 'INDOMIE GORENG 85 G ', '', 2150, 2150, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 10, 10, 0, '089686010947'),
(15, 'INDOMIE GORENG PEDAS 79 G ', '', 2200, 2200, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 11, 11, 10, 0, '089686011036'),
(16, 'INDOMIE RASA MIE KOCOK BANDUNG 75 G', '', 2200, 2200, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 9, 12, 10, 0, '089686011289'),
(17, 'INDOMIE GORENG RASA DENDENG BALADO 90 G ', '', 2450, 2450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 3, 13, 10, 0, '089686043983'),
(18, 'INDOMIE RASA SOTO LAMONGAN 90 G', '', 2450, 2450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 9, 14, 10, 0, '089686043990'),
(19, 'POP MIE RASA AYAM 75 G', '', 3950, 3950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 3, 15, 10, 0, '089686060027'),
(20, 'POP MIE RASA BASO SAPI SPESIAL 75 G', '', 3950, 3950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 10, 16, 10, 0, '089686060164'),
(21, 'POP MIE RASA KARI AYAM 75 G', '', 3950, 3950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 4, 17, 10, 0, '089686060461'),
(22, 'INDOFOOD BUMBU KENTANG GORENG JAGUNG BAKAR  25 G', '', 3950, 3950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 10, 18, 10, 0, '089686385519'),
(23, 'INDOFOOD BUMBU KENTANG GORENG BBQ 25 G', '', 3950, 3950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 4, 19, 10, 0, '089686385564'),
(24, 'INDOFOOD BUMBU RACIK NASI GORENG SPESIAL SACHET 20 G', '', 1700, 1700, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 5, 20, 10, 0, '089686386028'),
(25, 'INDOFOOD BUMBU RACIK AYAM GORENG SACHET 26 G', '', 1450, 1450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 5, 21, 10, 0, '089686386066'),
(26, 'INDOFOOD SAMBAL PEDAS MANIS BOTOL 340 ML', '', 11450, 11450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 12, 22, 10, 0, '089686400564'),
(27, 'INDOFOOD SAUS TOMAT BOTOL 140 ML', '', 4550, 4550, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 12, 23, 10, 0, '089686401721'),
(28, 'INDOFOOD BUMBU GULAI SACHET 45 G', '', 4650, 4650, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 6, 24, 10, 0, '089686440089'),
(29, 'INDOFOOD BUMBU NASI GORENG ORIENTAL FRIED RICE 45 G', '', 4500, 4500, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 13, 25, 10, 0, '089686440188'),
(30, 'INDOFOOD BUMBU OPOR AYAM SACHET 45 G', '', 4650, 4650, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 13, 26, 10, 0, '089686440287'),
(31, 'INDOFOOD BUMBU RENDANG SACHET 50 G', '', 4650, 4650, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 6, 27, 10, 0, '089686440430'),
(32, 'INDOFOOD BUMBU SOTO AYAM SACHET 45 G', '', 4650, 4650, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 14, 28, 10, 0, '089686440584'),
(33, 'SUN BUBUR LANJUTAN TIM INSTAN AYAM & SAYUR 100 G', '', 10450, 10450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 14, 29, 10, 0, '089686537635'),
(34, 'CHIKI BALLS RASA KEJU 10 G', '', 1050, 1050, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 7, 30, 10, 0, '089686590036'),
(35, 'CHIKI BALLS RASA AYAM 10 G', '', 1050, 1050, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 7, 31, 10, 0, '089686590135'),
(36, 'CHIKI BALLS RASA COKLAT 10 G', '', 1050, 1050, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 8, 32, 10, 0, '089686591231'),
(37, 'LAYS RUMPUT LAUT 14 G', '', 2000, 2000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 8, 33, 10, 0, '089686596427'),
(38, 'LAYS RUMPUT LAUT 75 G', '', 9750, 9750, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 1, 10, 0, '089686596465'),
(39, 'LAYS RASA SALMON TERIYAKI 68 G', '', 9750, 9750, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 2, 10, 0, '089686596779'),
(40, 'LAYS RASA AYAM PANGGANG MAYO 68 G', '', 10000, 10000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 3, 10, 0, '089686596823'),
(41, 'LAYS RASA AYAM PANGGANG PAPRIKA 68 G', '', 10000, 10000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 4, 10, 0, '089686596885'),
(42, 'CHITATO RASA SAPI PANGGANG 68 G', '', 10150, 10150, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 5, 10, 0, '089686598056'),
(43, 'CHITATO RASA AYAM BUMBU 35 G', '', 5650, 5650, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 6, 10, 0, '089686598421'),
(44, 'CHITATO RASA AYAM BUMBU 68 G', '', 9450, 9450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 7, 10, 0, '089686598476'),
(45, 'CHITATO ASIAN CUISINE RASA OKONOMIYAKI 85 G', '', 9950, 9950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 8, 10, 0, '089686598650'),
(46, 'CHITATO ASIAN CUISINE SPICY BULGOGI 85 G', '', 9950, 9950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 9, 10, 0, '089686598667'),
(47, 'CHEETOS AYAM BAKAR 15 G', '', 1450, 1450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 10, 10, 0, '089686600025'),
(48, 'JET Z STICK CHOCOFIESTA 12 G', '', 1150, 1150, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 11, 11, 10, 0, '089686604443'),
(49, 'QTELA TEMPE RASA CABAI RAWIT 60 G', '', 5250, 5250, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 9, 12, 10, 0, '089686611854'),
(50, 'QTELA TEMPE RASA RUMPUT LAUT 60 G', '', 5250, 5250, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 3, 13, 10, 0, '089686611878'),
(51, 'QTELA UBI UNGU ORIGINAL 45 G', '', 5250, 5250, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 9, 14, 10, 0, '089686611892'),
(52, 'QTELA TEMPE CHIPS RASA ORIGNAL 60 G', '', 5250, 5250, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 3, 15, 10, 0, '089686611908'),
(53, 'QTELA TEMPE RASA DAUN JERUK 60 G ', '', 5250, 5250, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 10, 16, 10, 0, '089686611922'),
(54, 'CLUB MINERAL WATER BOTOL 600 ML ', '', 1850, 1850, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 4, 17, 10, 0, '0896867700326'),
(55, 'INDOMIE RASA EMPAL GENTONG 75 G', '', 2200, 2200, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 10, 18, 10, 0, '089686910308'),
(56, 'INDOMIE GORENG RASA RENDANG 91 G', '', 2100, 2100, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 4, 19, 10, 0, '089686910704'),
(57, 'ABC KECAP MANIS MANTAAP SACHET 6 X 13 ML', '', 2000, 2000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 5, 20, 10, 0, '100001'),
(58, 'BOX GUNINDO TRANS ', '', 4000, 4000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 5, 21, 10, 0, '100003'),
(59, 'BUAH LEMON KANTONG PLASTIK PUTIH 12 X 25 1 IKAT', '', 1500, 1500, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 12, 22, 10, 0, '100010'),
(60, 'SINAR KANTONG PLASTIK PUTIH 10 X 20 1 IKAT', '', 1200, 1200, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 12, 23, 10, 0, '100011'),
(61, 'TELUR AYAM 1 KG', '', 19950, 19950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 6, 24, 10, 0, '100012'),
(62, 'KOMET NATURAL PACK TUPPERWARE 3.5 L', '', 14000, 14000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 13, 25, 10, 0, '100013'),
(63, 'DETERGEN MATIC CURAH 1 KG ', '', 10000, 10000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 13, 26, 10, 0, '100015'),
(64, 'BALLPOINT FASTER C600', '', 2450, 2450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 6, 27, 10, 0, '100019'),
(65, 'BALLPOINT PILOT BPTP', '', 1950, 1950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 14, 28, 10, 0, '100020'),
(66, 'FILTRA BIHUN BERAS 130 G', '', 2450, 2450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 14, 29, 10, 0, '100023'),
(67, 'SELAI BLUEBERRY CURAH 250 G', '', 7000, 7000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 7, 30, 10, 0, '100026'),
(68, 'TISU SAKU PASEO SMART 2 PLY 12 SHEETS', '', 1000, 1000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 7, 31, 10, 0, '100032'),
(69, 'MERICA BUBUK MERAK BOTOL', '', 3500, 3500, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 8, 32, 10, 0, '100039'),
(70, 'KEMIRI CURAH 100 G', '', 4350, 4350, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 8, 33, 10, 0, '100045'),
(71, 'PRIMADE PALMSUIKER BIRU 250 G', '', 8450, 8450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 1, 10, 0, '100050'),
(72, 'BAWANG GORENG ISTIMEWA KURNIA SARI 100 G', '', 2950, 2950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 2, 10, 0, '100052'),
(73, 'SUS KEJU CEMIL CEMIL 100 G', '', 7950, 7950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 3, 10, 0, '100063'),
(74, 'MYOGHURT 200 ML', '', 7500, 7500, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 4, 10, 0, '100066'),
(75, 'KACANG BANDUNG CEMIL CEMIL 300 G', '', 13450, 13450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 5, 10, 0, '100069'),
(76, 'ANEKA MAKANAN RINGAN HR PUTRA', '', 2000, 2000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 6, 10, 0, '100076'),
(77, 'TEPUNG ROTI TAWAR TIGA MELATI', '', 10000, 10000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 7, 10, 0, '100082'),
(78, 'KULIT LUMPIA 10 LEMBAR', '', 3000, 3000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 8, 10, 0, '100092'),
(79, 'BERAS CURAH 5 KG', '', 55450, 55450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 9, 10, 0, '100094'),
(80, 'KERIPIK BELUT CEMIL CEMIL 100 G', '', 12500, 12500, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 10, 10, 0, '100096'),
(81, 'KERUPUK KULIT SAPI CEMIL CEMIL 100 G', '', 13950, 13950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 11, 11, 10, 0, '100097'),
(82, 'LPG PERTAMINA 3 KG', '', 20000, 20000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 9, 12, 10, 0, '100098'),
(83, 'ES LILIN KITA KITA', '', 3000, 3000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 3, 13, 10, 0, '100099'),
(84, 'TEPUNG TERIGU PITA MERAH 1 KG', '', 8200, 8200, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 9, 14, 10, 0, '100100'),
(85, 'BAWANG GORENG BUNGKUSAN 50 G', '', 3950, 3950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 3, 15, 10, 0, '100101'),
(86, 'REMPEYEK KACANG BAROKAH 160 G', '', 11450, 11450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 10, 16, 10, 0, '100102'),
(87, 'CISTIK SELEDRI BAROKAH 200 G', '', 11450, 11450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 4, 17, 10, 0, '100103'),
(88, 'QUISY MIX TEPUNG PREMIX KUE CUBIT TARO 225 G', '', 20000, 20000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 10, 18, 10, 0, '100104'),
(89, 'QUISY MIX TEPUNG PREMIX KUE CUBIT RED VELVET 225 G', '', 20000, 20000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 4, 19, 10, 0, '100105'),
(90, 'QUISY MIX TEPUNG PREMIX KUE CUBIT GREEN TEA 225 G', '', 20000, 20000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 5, 20, 10, 0, '100106'),
(91, 'QUISY MIX TEPUNG PREMIX KUE CUBIT ORIGINAL 225 G', '', 20000, 20000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 5, 21, 10, 0, '100107'),
(92, 'HANSATAPE DOUBLE TAPE 1 INCH', '', 3450, 3450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 12, 22, 10, 0, '100108'),
(93, 'BAKPIA LOKAL ISI 3 PCS', '', 3000, 3000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 12, 23, 10, 0, '100109'),
(94, 'LADZIS PANCAKE DURIAN', '', 14000, 14000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 6, 24, 10, 0, '100111'),
(95, 'SUMPIA EBI SWEET SNACK & COOKIES', '', 19550, 19550, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 13, 25, 10, 0, '100113'),
(96, 'SUMPIA AYAM SWEET SNACK & COOKIES', '', 40250, 40250, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 13, 26, 10, 0, '100114'),
(97, 'RAGI CURAH 1 ONS', '', 6450, 6450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 6, 27, 10, 0, '100117'),
(98, 'RIVANOL SEINO 285 ML', '', 6950, 6950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 14, 28, 10, 0, '100118'),
(99, 'VILLAMART GURILEM 80 G', '', 4950, 4950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 14, 29, 10, 0, '100119'),
(100, 'VILLAMART KERIPIK KENTANG 75 G', '', 4950, 4950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 7, 30, 10, 0, '100120'),
(101, 'VILLAMART KACANG THAILAND 250 G', '', 16500, 16500, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 7, 31, 10, 0, '100121'),
(102, 'ONEMED POVIDONE IODINE 60 ML', '', 10950, 10950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 8, 32, 10, 0, '100122'),
(103, 'SIKAT GIGI ORAL B CLASSIC ULTRACLEAN', '', 6650, 6650, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 8, 33, 10, 0, '3014260821883'),
(104, 'FERMIPAN INSTANT YEAST 11 G ', '', 6450, 6450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 1, 10, 0, '3516663740001'),
(105, 'FABER CASTELL PENCIL 2B', '', 3450, 3450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 2, 10, 0, '4005401171027'),
(106, 'INAFOOD MARIE SUSU BISCUIT 95 G', '', 5000, 5000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 3, 10, 0, '4711036010265'),
(107, 'MAGIC SYNTHETIC CLOTH', '', 6950, 6950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 4, 10, 0, '4719990020214'),
(108, 'NESTLE KOKO KRUNCH 170 G', '', 19950, 19950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 5, 10, 0, '4800361002851'),
(109, 'AXE DEODORANT BODYSPRAY DARK TEMPTATION 150 ML', '', 34450, 34450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 6, 10, 0, '4800888141125'),
(110, 'PANTENE SHAMPOO HAIR FALL CONTROL 170 ML', '', 19950, 19950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 2, 7, 10, 0, '4902430400886'),
(111, 'PANTENE SHAMPOO ANTI DANDRUFF BOTOL 170 ML', '', 20950, 20950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 8, 10, 0, '4902430400947'),
(112, 'PANTENE LEAVE ON PROTECT & CARE SACHET 4 ML', '', 450, 450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 9, 10, 0, '4902430416047'),
(113, 'REJOICE SHAMPO ANTI DANDRUFF 3 IN 1 170 ML', '', 16950, 16950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 1, 10, 10, 0, '4902430429375'),
(114, 'REJOICE SOFT & SMOOTH SHAMPOO BOTOL 170 ML', '', 14250, 14250, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 11, 11, 10, 0, '4902430432269'),
(115, 'DOWNY SEKALI BILAS FRESHNESS BUBBLES 400 ML', '', 13950, 13950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 9, 12, 10, 0, '4902430463324'),
(116, 'HEAD & SHOULDERS ITCHY SCALP CARE SHAMPO BOTOL 170 ML', '', 20950, 20950, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 3, 13, 10, 0, '4902430482783'),
(117, 'HEAD & SHOULDERS MEN COOL BLAST 165 ML', '', 21450, 21450, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 9, 14, 10, 0, '4902430506960'),
(118, 'PANTENE SHAMPO ANTI KETOMBE SACHET 10 ML', '', 1000, 1000, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 3, 15, 10, 0, '4902430563864'),
(119, 'PAMPERS BABY DRY PANTS UKURAN L ISI 8 PCS', '', 21350, 21350, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 10, 16, 10, 0, '4902430600835'),
(120, 'DOWNY SUNRISE FRESH FRESHNESS BUBBLES 400 ML', '', 12550, 12550, '2015-09-29 14:48:06', '2015-09-29 14:48:06', '', 4, 17, 10, 0, '4902430972260'),
(121, 'BOTAN MACKEREL IN TOMATO SAUCE 425 G', '', 22950, 22950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '4902931110178'),
(122, 'VICKS FORMULA ANAK STRAWBERRY 27 ML', '', 6000, 6000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '4987176004611'),
(123, 'KUSUKA RASA AYAM LADA HITAM 200 G', '', 10950, 10950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '653314502151'),
(124, 'SNICKERS BAR 51 G', '', 5500, 5500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '6914973600362'),
(125, 'XDR XANDER BLUE II PLUS', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '6950769355665'),
(126, 'ABC KECAP MANIS MANTAAP BOTOL 275 ML', '', 12150, 12150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '711844110083'),
(127, 'KECAP MANIS ABC ISI 18 SACHET', '', 5750, 5750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '711844110311'),
(128, 'ABC BLACK GOLD KECAP MANIS POUCH 500 ML', '', 19950, 19950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '711844110373'),
(129, 'ABC KECAP EXTRA PEDAS BOTOL POUCH 400 G', '', 12450, 12450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '711844110748'),
(130, 'ABC SAMBAL ASLI BOTOL 335 ML', '', 11450, 11450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '711844120013'),
(131, 'ABC SAMBAL ASLI BOTOL 135 ML ', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '711844120037'),
(132, 'ABC SAMBAL ASLI SACHET 22 X 9 G', '', 6250, 6250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '711844120310'),
(133, 'ABC SAMBAL ASLI POUCH 80 G', '', 2250, 2250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '711844120549'),
(134, 'ABC SAMBAL EXTRA PEDAS POUCH 80 G ', '', 2250, 2250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '711844120556'),
(135, 'ABC SQUASH DELIGHT JERUK FLORIDA 580 ML', '', 12450, 12450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '711844150003'),
(136, 'ABC SQUASH DELIGHT LECI 580 ML', '', 12450, 12450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '711844150034'),
(137, 'ABC SIRUP SPECIAL GRANDE RASA MELON 585 ML', '', 19650, 19650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '711844154568'),
(138, 'HILO GOLD VANILLA 500 G', '', 80450, 80450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '749921000392'),
(139, 'HILO ACTIVE VANILLA 500 G', '', 79950, 79950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '749921000415'),
(140, 'NUTRISARI AMERICAN SWEET ORANGE SACHET 14 G ', '', 1150, 1150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '749921005946'),
(141, 'NUTRISARI FLORIDA ORANGE SACHET 14 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '749921006110'),
(142, 'HEAVENLY BLUSH YOGURT DRINK STRAWBERRY 200 ML', '', 8750, 8750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '7499221123132'),
(143, 'HEAVENLY BLUSH YOGURT DRINK BLACKCURRANT 200 ML', '', 8150, 8150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '7499221133155'),
(144, 'HEAVENLY BLUSH YOGURT DRINK PEACH 200 ML', '', 8750, 8750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '7499221133162'),
(145, 'KRAFT CHEDDAR MINI 35 G', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '7622300065201'),
(146, 'OREO SOFT CAKE VANILA 16 G ', '', 1550, 1550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '7622300335809'),
(147, 'KRAFT KEJUCAKE 16 G', '', 1550, 1550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '7622300405588'),
(148, 'OREO BLUEBERRY ICE CREAM 29.4 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '7622300442477'),
(149, 'OREO BLUEBERRY ICE CREAM 137 G', '', 7150, 7150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '7622300442507'),
(150, 'GOLDEN OREO 29.4 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '7622300484095'),
(151, 'PHILIPS ENERGI SAVING 5 W', '', 25950, 25950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8718291791836'),
(152, 'PHILIPS ENERGY SAVING 8 W', '', 28750, 28750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8718291791874'),
(153, 'PHILIPS ENERGY SAVING 11 W', '', 32250, 32250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8718291791911'),
(154, 'MAMASUKA RUMPUT LAUT 4.5 G', '', 4500, 4500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8801052774411'),
(155, 'SOK PORTABLE ELECTRICITY CONNECTOR 8 METER', '', 17450, 17450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8809267555467'),
(156, 'SOK PORTABLE ELECTRICTY CONNECTOR 6 METER', '', 15450, 15450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8809267555476'),
(157, 'CAREFREE SUPER DRY 20 S', '', 7950, 7950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8850007331360'),
(158, 'CAREX NATURAL ANTIBACTERIAL HANDWASH FRESH DISPENSER 200 ML', '', 20450, 20450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8850169812011'),
(159, 'MOGU MOGU RASA MANGGA 320 ML', '', 7250, 7250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8850389108048'),
(160, 'MOGU MOGU RASA LECI 320 ML', '', 7250, 7250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8850389108062'),
(161, 'MOGU MOGU RASA KELAPA 320 ML', '', 7250, 7250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8850389108277'),
(162, 'MOGU MOGU RASA ANGGUR 320 ML', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8850389108314'),
(163, 'PADDLE POP TORNADO GRAPE 50 G', '', 4000, 4000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8851932310352'),
(164, 'PADDLE POP BANANA BOAT 42 ML', '', 4000, 4000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8851932343831'),
(165, 'TAO KAE NOI BIG ROLL CLASSIC FLAVOUR 3.6 G', '', 4550, 4550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8858702410816'),
(166, 'ROKARI WAFER BOLA VALUE PACK ISI 50 PCS', '', 22950, 22950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8883241267010'),
(167, 'TOLL ASSORTED MILK CANDY 120 G', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8883241269298'),
(168, 'ASTOR CHOCOLATE WAFER STICK ORIGINAL 150 G', '', 11450, 11450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8886001026025'),
(169, 'BENG-BENG CHOCOLATE 22 G', '', 1550, 1550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8886001038011'),
(170, 'KIS MINT BARLEY 125 G', '', 5500, 5500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8886001100855'),
(171, 'AQUA GELAS 240 ML', '', 750, 750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8886008101046'),
(172, 'AQUA BOTOL 600 ML', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8886008101053'),
(173, 'AQUA BOTOL 1500 ML', '', 4200, 4200, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8886008101091'),
(174, 'FRENCH FRIES 2000 TOMATO CHILI SAUCE 38 G', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8886013281481'),
(175, 'SPIX SOBA MIE STICK ALA JEPANG SAMBAL BALADO 24 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8886013304609'),
(176, 'SPIX SOBA MIE STICK 24 G', '', 1200, 1200, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8886013338604'),
(177, 'ARNOTTS NYAM NYAM FANTASY STICK 18 G', '', 2800, 2800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8886015401030'),
(178, 'ARNOTTS TIMTAM WAFER LONG VANILLA 18 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8886015427238'),
(179, 'ARNOTTS TIMTAM CHOCOLATE 120 G', '', 8547, 8547, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8886015428136'),
(180, 'SWALLOW NAPHTHALENE DISK BALL 150 G', '', 8850, 8850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8886020001126'),
(181, 'ABC DRY CELL D 1.5 V 2 PCS', '', 8650, 8650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8886022910266'),
(182, 'ABC DRY CELL AA 1.5 V 4 PCS', '', 7000, 7000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8886022930240'),
(183, 'ULTRAFLU 4 KAPLET', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8886029101049'),
(184, 'YURI HAND SOAP ORANGE POUCH 375 ML', '', 14250, 14250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8886030229954'),
(185, 'CLUB AIR MINERAL CUP 240 ML', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8886031111326'),
(186, 'TAMARIND GULAS PERMEN GULA ASEM ISI 5 BUTIR', '', 1450, 1450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8886047030116'),
(187, 'KRATINGDAENG ENERGY DRINK 150 ML', '', 4450, 4450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8886057883665'),
(188, 'KHONG GUAN SALTCHEESE 200 G', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8888166321106'),
(189, 'NISSIN CRISPY CRACKERS 225 G', '', 7400, 7400, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8888166336568'),
(190, 'KHONG GUAN CREAM CRACKERS 115 G', '', 4662, 4662, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8888166603356'),
(191, 'KHONG GUAN ASSORTED BISCUITS KALENG 650 G', '', 42450, 42450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8888166603615'),
(192, 'KHONG GUAN MALKIST RASA ABON SAPI 135 G', '', 5650, 5650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8888166606227'),
(193, 'KHONG GUAN POTATOZ KREKERS 150 G', '', 6660, 6660, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8888166607415'),
(194, 'MONDE GENJI ORIGINAL PIE 70 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8888166842557'),
(195, 'MONDE GENJI PIE SAND VANILI 110 G', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8888166842564'),
(196, 'MONDE GENJI RAISINS PIE 85 G', '', 6850, 6850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8888166842625'),
(197, 'DANISH MONDE BUTTER COOKIES 908 G', '', 98950, 98950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8888166989566'),
(198, 'SERENA SNACK 50 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8888166989634'),
(199, 'MONDE GENJI SOFT PIE RASA STRAWBERRY 85 G', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8888166989832'),
(200, 'MONDE EGG DROPS BISCUIT 110 G', '', 5150, 5150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8888166989948'),
(201, 'SERENA EGG ROLLS KALENG 300 G', '', 49850, 49850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8888166990050'),
(202, 'MONDE FRIED COOKIES 200 G', '', 11250, 11250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8888166990104'),
(203, 'NISSIN WALENS CHOCO SOES 100 G', '', 7750, 7750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8888166993150'),
(204, 'MONDE GENJI PIE SAND COKELAT 110 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8888166994683'),
(205, 'SIKA GIGI ORAL B ULTRA CLEAN CLASSIC MEDIUM', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8888826016564'),
(206, 'GILLETTE BLUE II PLUS', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8888826019589'),
(207, 'LA FONTE SPAGHETTI 225 G', '', 7650, 7650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8888900415009'),
(208, 'LA FONTE FUSILLI 202 450 G', '', 13450, 13450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8888900502020'),
(209, 'LA FONTE SPAGHETTI BOLOGNESE SAUCE 117 G ', '', 5800, 5800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8888900600115'),
(210, 'LA FONTE SPAGHETTI WITH MUSHROOM SAUCE 117 G', '', 5800, 5800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8888900620113'),
(211, 'BANGO KECAP MANIS BOTOL 135 ML ', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8990121011073'),
(212, 'LOTTE PERMEN KARET HOT WHEELS RASA ANGGUR SACHET 9.6 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8990333131095'),
(213, 'MENTOS MINT CHEWY DRAGEES 135 G', '', 6750, 6750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8990800004563'),
(214, 'MENTOS MINI RAINBOW 14 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8990800012636'),
(215, 'MARBELS ANEKA RASA 40 G', '', 3250, 3250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8990800013060'),
(216, 'HAPPYDENT WHITE PILLOWPACK 84 G', '', 9000, 9000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8990800013978'),
(217, 'ALPENLIEBE ECLAIRS CHOCOLATE CARAMEL 144 G', '', 10450, 10450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8990800014227'),
(218, 'MENTOS RASA MINT 135 G', '', 6000, 6000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8990800020013'),
(219, 'FRUITTELLA FRUIT FANTASY 105 G (ISI 42)', '', 5750, 5750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8990800919706'),
(220, 'SILVER QUEEN ROCK R CASHEW 22 G', '', 4500, 4500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8991001111937'),
(221, 'DELFI TOP CHOCOLATE 16 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8991001242013'),
(222, 'DELFI TOP CHOCOLATE BAR 9 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8991001242075'),
(223, 'DELFI TOP BLACK IN WHITE 16 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8991001242983'),
(224, 'CERES CLASSIC 225 G', '', 22750, 22750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8991001301031'),
(225, 'CERES MILK 90 G ', '', 9450, 9450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8991001302267'),
(226, 'RITZ COKLAT KOIN BOTOL 360 G', '', 39950, 39950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8991001662361'),
(227, 'DELFI TWISTER BLACK KALENG 300 G', '', 28650, 28650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8991001780539'),
(228, 'SILVER QUEEN ROCK R DARK 22 G', '', 4600, 4600, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8991001802088'),
(229, 'ABC KOPI SUSU SACHET 31 G', '', 1100, 1100, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8991002101630'),
(230, 'ABC MOCCA SACHET 27 G', '', 1500, 1500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8991002101746'),
(231, 'GOOD DAY COFFEE FREEZE MOCAFRIO SACHET 30 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8991002103733'),
(232, 'GOOD DAY CAPPUCCINO SACHET 25 G', '', 1500, 1500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8991002103764'),
(233, 'GOOD DAY CHOCOCINNO SACHET 20 G', '', 1150, 1150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8991002103832'),
(234, 'KOPI KAPAL API 380 G', '', 18900, 18900, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8991002105409'),
(235, 'KAPAL API SPECIAL KOPI BUBUK SACHET 165 G', '', 11250, 11250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8991002105423'),
(236, 'KAPAL API SPECIAL KOPI BUBUK SACHET 65 G', '', 4850, 4850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8991002105430'),
(237, 'KAPAL API SPECIAL MIX SACHET 25 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8991002105485'),
(238, 'GOOD DAY WHITE CAPPUCCINO SACHET 25 G', '', 1500, 1500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8991002113312'),
(239, 'GINGERBON PERMEN JAHE 125 G', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8991002202719'),
(240, 'RELAXA BARLEY MINT 135 G', '', 5500, 5500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8991002304017'),
(241, 'RELAXA ORANGE MINT 135 G', '', 5000, 5000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8991002304178'),
(242, 'MAYASI KORO KORO RASA PEDAS 70 G', '', 7500, 7500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8991002502161'),
(243, 'MAYASI KACANG JEPANG RASA BAWANG 70 G', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8991002502338'),
(244, 'MAYASI CRISPY CREPES STRAWBERRY MILK 100 G', '', 6438, 6438, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8991002502628'),
(245, 'MAYASI KACANG JEPANG SPICY FLAVOR 70 G', '', 7500, 7500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8991002502727'),
(246, 'MAYASI CRISPY CREPES CHOCO BANANA 100 G', '', 6438, 6438, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8991002502864'),
(247, 'SELECTION FACIAL COTTON 50 G', '', 7650, 7650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8991038110514'),
(248, 'FORMULA TOOTHBRUSH SILVER PROTECTOR SOFT TRENDY 1 PCS', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8991102020459'),
(249, 'SIKAT GIGI FORMULA SILVER PROTECTOR TRENDY MEDIUM', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8991102020473'),
(250, 'SIKAT GIGI FORMULA PLATINUM PROTECTOR SOFT', '', 7950, 7950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8991102023078'),
(251, 'FORMULA TOOTHPASTE ACTION PROTECTOR 190 G', '', 9750, 9750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8991102100823'),
(252, 'TEH GELAS ORIGINAL 180 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8991102222006'),
(253, 'TANGO WAFER SUSU VANILA 76 G', '', 5500, 5500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8991102302708'),
(254, 'TANGO CRUNCHCAKE CHOCOLATE 4 SLICES 80 G', '', 7950, 7950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8991102383417'),
(255, 'TANGO WAFFLE CRUNCHOX 8.5 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8991102385053'),
(256, 'TANGO WAFER CHOCOLATE 8.5 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8991102385084'),
(257, 'TANGO WAFFLE CRUNCH BLUEBERRY 8 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8991102385350'),
(258, 'TANGO CRUNCHCAKE FRENCH VANILLA 4 SLICES 80 G', '', 8950, 8950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8991102386401'),
(259, 'KIRANTI ORIGINAL 150 ML', '', 4850, 4850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8991102800020'),
(260, 'JOHNSONS BABY SHAMPOO 100 ML', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8991111102603'),
(261, 'PERMEN KARET BIG BABOL RASA KRIM STROBERI 114 G (ISI 30)', '', 7550, 7550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8991115012021'),
(262, 'HAPPYDENT WHITE BLISTER @10 BUTIR', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8991115040093'),
(263, 'SASA BUMBU PENYEDAP RASA 50 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8991188943017'),
(264, 'SASA BUMBU PENYEDAP RASA 250 G', '', 8750, 8750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8991188943062'),
(265, 'SASA BUMBU PENYEDAP RASA 500 G', '', 17250, 17250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8991188943079'),
(266, 'LOREAL MEN EXPERT WHITE ACTIVE BRIGHTENING FOAM 100 ML', '', 24500, 24500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8991380232131'),
(267, 'GARNIER LIGHT COMPLETE SCRUB 50 ML', '', 15250, 15250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8991380700593'),
(268, 'VITACIMIN TABLET HISAP VITAMIN C 500 G 1 PCS', '', 750, 750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8991771200329'),
(269, 'DECOLGEN ISI 4 TABLET', '', 1650, 1650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8992112025021'),
(270, 'GATSBY URBAN COLOGNE SPRAY INFINITY 125 ML', '', 16850, 16850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8992222053464'),
(271, 'GARNIER MEN GREASE CONTROL BRIGHTENING COOLING FOAM 50 ML', '', 14950, 14950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8992304009143'),
(272, 'LOREAL COLOR VIVE PROTECTING SHAMPOO 170 ML', '', 20150, 20150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8992304010101'),
(273, 'LOREAL RE NUTRITION ANTI DRYNESS 170 ML', '', 25450, 25450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8992304010187'),
(274, 'GARNIER MEN TURBOLIGHT OIL CONTROL CHARCOAL BLACK FOAM 100 ML', '', 25000, 25000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992304024740'),
(275, 'NU GREEN TEA ORIGINAL 500 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8992388101016'),
(276, 'NU GREEN TEA MADU 500 ML', '', 5650, 5650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8992388101023'),
(277, 'NU GREEN TEA LESS SUGAR 500 ML', '', 5800, 5800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8992388101054'),
(278, 'MI CUP ABC SELERA PEDAS RASA AYAM PEDAS LIMAU 80 G', '', 4150, 4150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8992388112678'),
(279, 'MI CUP ABC SELERA PEDAS RASA GULAI AYAM PEDAS 60 G', '', 4150, 4150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8992388121267'),
(280, 'NU MILK TEA BOTOL 330 ML', '', 5550, 5550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8992388133017'),
(281, 'BIMOLI MINYAK GORENG POUCH 2 L', '', 25450, 25450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8992628020152'),
(282, 'BIMOLI MINYAK GORENG BOTOL 1 L', '', 15650, 15650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8992628022149'),
(283, 'BIMOLI MINYAK GORENG POUCH 1 L', '', 13550, 13550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8992628022156'),
(284, 'BIMOLI MINYAK GORENG BOTOL 620 ML', '', 11050, 11050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8992628024143'),
(285, 'BIMOLI MINYAK GORENG BOTOL 250 ML', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8992628026147'),
(286, 'LARISST MARGARIN SACHET 200 G', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8992628312158'),
(287, 'PANADOL PARACETAMOL 1 BLISTER @10 KAPLET', '', 6900, 6900, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8992695100207'),
(288, 'PANADOL EXTRA 1 BLISTER @10 KAPLET', '', 7250, 7250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8992695110206'),
(289, 'PANADOL COLD FLU 1 BLISTER @10 KAPLET', '', 9800, 9800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8992695120205'),
(290, 'PANADOL ANAK RASA CHERRY 1 BLISTER @10 TABLET', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8992695160201'),
(291, 'NESTLE BEAR BRAND MILK 189 ML', '', 8500, 8500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8992696404441'),
(292, 'NESTLE DANCOW FORTIGRO COKLAT 800 G', '', 70950, 70950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8992696405257'),
(293, 'NESTLE DANCOW FORTIGRO PUTIH 400 G', '', 42950, 42950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8992696405431'),
(294, 'NESTLE DANCOW FORTIGRO PUTIH 800 G', '', 81450, 81450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8992696405448'),
(295, 'NESTLE DANCOW FORTIGRO FULL CREAM 800 G', '', 80000, 80000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8992696405493'),
(296, 'NESTLE CERELAC KACANG HIJAU SACHET 20 G', '', 1450, 1450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8992696408722'),
(297, 'NESCAFE COFFEE CREAM KOTAK 200 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8992696419742'),
(298, 'FOXS CRYSTAL CLEAR FRUITS 38 G', '', 3500, 3500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8992696419988'),
(299, 'NESCAFE FRENCH VANILLA KOTAK 200 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8992696422650'),
(300, 'NESCAFE MOCHACCINO KOTAK 200 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8992696423701'),
(301, 'NESCAFE BLACK KOTAK 200 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8992696425316'),
(302, 'NESTLE CRUNCH CHIPS CHOCOLATE ORIGINALE 60 G', '', 5750, 5750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8992696426214'),
(303, 'NESTLE CRUNCH CHIPS CHOCOLATE STRAWBERRY 30 G', '', 3150, 3150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8992696426320'),
(304, 'NESTLE CRUNCH CHIPS CHOCOLATE ORANGE 30 G', '', 3150, 3150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8992696426382'),
(305, 'NESTLE KOKO KRUNCH INSTANT 32 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8992696428263'),
(306, 'NESTLE CRUNCH CHIPS CHOCOLATE PEANUT 60 G', '', 5750, 5750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8992696428515'),
(307, 'NESCAFE WHITE COFFEE KOTAK 200 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992696428812'),
(308, 'NESTLE KOKO KRUNCH 30 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8992696429291'),
(309, 'NESTLE CARNATION 375 G ', '', 8950, 8950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8992696429970'),
(310, 'INDOMILK SUSU KENTAL MANIS PUTIH KALENG 375 G', '', 10450, 10450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8992702000018'),
(311, 'INDOMILK SUSU KENTAL MANIS COKLAT KALENG 375 G', '', 9250, 9250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8992702000063'),
(312, 'BISKUAT BOLU EKSTRA COKLAT 16 G', '', 1500, 1500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8992716109172'),
(313, 'SUN KARA SANTAN KELAPA 200 ML', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8992717102509'),
(314, 'KARA NATA DE COCO 1000 G', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8992717781001'),
(315, 'SUN KARA SANTAN KELAPA 65 ML', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8992717781025'),
(316, 'KARA NATA DE COCO RASA LECI 220 ML', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8992717822209'),
(317, 'MYLANTA OBAT MAAG 10 TABLET', '', 5500, 5500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8992725051219'),
(318, 'INACO JELLY WITH JUICE 225 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8992726938199'),
(319, 'INACO JELLY ANEKA RASA ISI 5 CUPS', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8992726938298'),
(320, 'LAURIER ACTIVE DAY SUPER MAXI ISI 8 BUAH', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8992727000048'),
(321, 'SASA TEPUNG BUMBU SERBA GUNA ORIGINAL 80 G', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8992736980133'),
(322, 'YUPI JUNGLE FUN 10 G', '', 800, 800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8992741902250'),
(323, 'YUPI STRAWBERRY KISS 120 G', '', 8650, 8650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8992741941327'),
(324, 'YUPI SPECIAL IDUL FITRI 200 G', '', 23450, 23450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8992741943536'),
(325, 'YUPI STRAWBERRY KISS 45 G', '', 3750, 3750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8992741950220'),
(326, 'YUPI STRAWBERRY KISS TOPLES 300 G (120 PCS)', '', 16450, 16450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8992741952262'),
(327, 'POLYTEX SABUT SPON', '', 5150, 5150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8992742360264'),
(328, 'POLYTEX SABUT STAINLESS REGULAR', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8992742360844'),
(329, 'HIT-MAT ANTI NYAMUK CLASSIC ISI 18', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8992745120636'),
(330, 'HIT NATURAL FRAGRANCE LEMON SPRAY 600 ML', '', 30450, 30450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8992745120964'),
(331, 'HIT MAGIC WANGI FRESH 3 SHEETS', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8992745130277'),
(332, 'BIOSOL BIANG KARBOL WANGI SACHET 25 ML', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8992745540892'),
(333, 'MITU TISU BAYI BABY PINK 50 SHEETS', '', 10950, 10950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8992745550518'),
(334, 'HIT NATURAL FRAGANCE POMEGRANATE SPRAY 600 ML', '', 35450, 35450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8992745705307'),
(335, 'VIXAL PEMBERSIH PORCELAIN LEBIH WANGI 800 ML', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8992747180201'),
(336, 'VIXAL PORCELAIN KUAT HARUM 800 ML', '', 14450, 14450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8992747180225'),
(337, 'KLINPAK CLING WRAP 6 M X 30 CM', '', 15450, 15450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8992749770301'),
(338, 'FRISIAN FLAG FULL CREAM GOLD POUCH 220 G', '', 10250, 10250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8992753005970'),
(339, 'FRISIAN FLAG COKELAT POUCH 220 G', '', 8050, 8050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8992753005994'),
(340, 'FRISIAN FLAG BENDERA KENTAL MANIS PUTIH SACHET 40 G', '', 1450, 1450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992753031894'),
(341, 'FRISIAN FLAG COKELAT KALENG 375 G', '', 9750, 9750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8992753102204'),
(342, 'LARISST TISU TRAVEL 50 SHEETS', '', 2250, 2250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8992759130041'),
(343, 'NICE TISSUE TAREPANDA 2 PLY 60 SHEETS', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8992759134117'),
(344, 'NICE FACIAL TISSUE 250 SHEET 2PLY', '', 10950, 10950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8992759313529'),
(345, 'LARISST TISU MAKAN 100 SHEET', '', 2750, 2750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8992759320268'),
(346, 'TOPLY FAMILY NAPKINS 50 PLY', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8992759324105'),
(347, 'NICE BATHROOM TISSUE', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8992759535457'),
(348, 'OREO BISKUIT DOUBLE DELIGHT KACANG & COKLAT 137 G', '', 7150, 7150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8992760121090'),
(349, 'RITZ CRACKERS 100 G', '', 5700, 5700, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8992760211029'),
(350, 'SPRITE KALENG 330 ML', '', 6550, 6550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8992761111038'),
(351, 'FRESTEA MELATI 500 ML', '', 5650, 5650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8992761122324'),
(352, 'FRESTEA APPLE 250 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8992761122331'),
(353, 'FRESTEA LEMON 500 ML', '', 5550, 5550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8992761122355'),
(354, 'FRESTEA LOW CALORIE 500 ML', '', 5650, 5650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8992761122515'),
(355, 'COCA COLA BOTOL 425 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8992761147013'),
(356, 'SPRITE BOTOL 425 ML', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8992761147020'),
(357, 'FANTA STRAWBERRY BOTOL 425 ML', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8992761147037'),
(358, 'MINUTE MAID NUTRIBOOST JERUK 300 ML', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8992761164539'),
(359, 'MINUTE MAID NUTRIBOOST STRAWBERRY 300 ML', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8992761164546'),
(360, 'MINUTE MAID PULPY ORANGE 350 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8992761166038'),
(361, 'MINUTE MAID PULPY TROPICAL 350 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8992761166113'),
(362, 'MINUTE MAID PULPY MANGO 350 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8992761166144'),
(363, 'MINUTE MAID PULPY ALOE VERA 350 ML', '', 6250, 6250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8992761166199'),
(364, 'GILLETTE GOAL II', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8992765301008');
INSERT INTO `items` (`id`, `item_name`, `item_thumbnail`, `item_price`, `discounted_item_price`, `created_at`, `updated_at`, `description`, `category_id`, `subcategory_id`, `stock`, `views`, `barcode`) VALUES
(365, 'AJINOMOTO BUMBU MASAK 500 G', '', 17750, 17750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8992770011107'),
(366, 'MASAKO RASA AYAM SACHET 11 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8992770033130'),
(367, 'MASAKO RASA SAPI SACHET 11 G ', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8992770033147'),
(368, 'MASAKO RASA AYAM POUCH 100 G', '', 4000, 4000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8992770034175'),
(369, 'KISPRAY AMORIS 300 ML', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8992772198011'),
(370, 'PILUS GARUDA RASA ORIGINAL 14 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8992775201404'),
(371, 'GARUDA KACANG ATOM 130 G', '', 6750, 6750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8992775204009'),
(372, 'GERY CHOCOLATOS 10 G', '', 550, 550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8992775311608'),
(373, 'OKKY JELLY DRINK JERUK 150 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992775406014'),
(374, 'OKKY JELLY DRINK JAMBU 150 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8992775406038'),
(375, 'OKKY JELLY DRINK APEL 150 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8992775406052'),
(376, 'LEO O CORN POPCORN INSTAN ORIGINAL 40 G', '', 3800, 3800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8992775810002'),
(377, 'LEO O CORN POPCORN INSTAN SWEET KETTLE FLAVOUR 80 G', '', 7950, 7950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8992775810026'),
(378, 'BEBEK KLOSET BOTOL 450 ML ', '', 11350, 11350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8992779228407'),
(379, 'TEH MELATI JASMINE TEA KEPALA JENGGOT 50 G', '', 9450, 9450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8992797450118'),
(380, 'FITBAR CHOCOLATE 25 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8992802063173'),
(381, 'MILNA BUBUR BAYI TUMIS HATI AYAM BROKOLI 120 G', '', 14750, 14750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8992802512121'),
(382, 'PRONAS KORNET SAPI KALENG 198 G ', '', 15950, 15950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8992804119816'),
(383, 'PRONAS KORNET SAPI KALENG 120 G ', '', 10250, 10250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8992804900254'),
(384, 'PRONAS SARDINES CHILI SAUCE 425 G', '', 12450, 12450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8992804900575'),
(385, 'SCOTCH BRITE S STAINLESS', '', 6000, 6000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8992806670056'),
(386, 'PRONAS IKAN SARDIN SAUS TOMAT 425 G', '', 12450, 12450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8992820114260'),
(387, 'PRONAS IKAN SARDIN SAUS TOMAT 150 G', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8992820115519'),
(388, 'MINYAK GORENG FILMA POUCH 2 L', '', 24950, 24950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8992826111089'),
(389, 'HYDRO COCO ORIGINAL 250 ML', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8992858527308'),
(390, 'PROMAG RASA MINT STRIP 12 TABLET', '', 5000, 5000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8992858664706'),
(391, 'XON-CE VITAMIN C TABLET HISAP STRIP 2 X 500 MG', '', 1500, 1500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8992858690200'),
(392, 'MADU SUPER NUSANTARA BOTOL 250 ML ', '', 46150, 46150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8992918265027'),
(393, 'SUGUS RASA BLACKCURRANT 30 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8992919856019'),
(394, 'SUGUS RASA JERUK 30 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8992919856026'),
(395, 'SUGUS STICK STRAWBERRY 30 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8992919856033'),
(396, 'TESSA FACIAL TISSUE 250 SHEETS', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8992931005013'),
(397, 'TESSA FACIAL TISSUE 260 SHEETS', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8992931005020'),
(398, 'TESSA TRAVEL TISSUE 50 SHEET', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8992931005099'),
(399, 'TESSA FACIAL TISSUE 50 SHEETS', '', 2750, 2750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8992931005112'),
(400, 'TESSA SOFT BATHROOM TISSUE ISI 6 ROLL', '', 24000, 24000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8992931009028'),
(401, 'NUTRIJELL JELLY POWDER LYCHEE SACHET 15 G ', '', 4750, 4750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8992933213119'),
(402, 'POP ICE CHOCOLATE SACHET 25 G ', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8992933231113'),
(403, 'POP ICE STRAWBERRY SACHET 25 G ', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8992933233117'),
(404, 'POP ICE MANGO SACHET 25 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8992933238112'),
(405, 'POP ICE VANILA BLUE SACHET 25 G ', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8992933254112'),
(406, 'POP ICE COKLAT SUSU SACHET 25 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992933258110'),
(407, 'POP ICE BLUEBERRY SACHET 25 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8992933285116'),
(408, 'NUTRIJELL RASA JERUK SACHET 10 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8992933322118'),
(409, 'LA PASTA SPAGHETTI INSTANT RASA CHEESE BOLOGNESE 59 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8992933651218'),
(410, 'NUTRIJELL JELLY SHAKE GUAVA 340 ML', '', 5150, 5150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8992933712117'),
(411, 'TONG TJI TEH MELATI 50 G', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8992936115021'),
(412, 'TONG TJI GREEN TEA JASMINE 30 G', '', 13650, 13650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8992936661610'),
(413, 'KINO SMILE FRUIT 38 G', '', 3500, 3500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8992942006344'),
(414, 'KINO CANDY MANGO 88 G', '', 4000, 4000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8992942220009'),
(415, 'KINO CANDY ICE CREAM 88 G', '', 4000, 4000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8992942830000'),
(416, 'TROPICAL MINYAK GORENG POUCH 1 L ', '', 13850, 13850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8992946121012'),
(417, 'TROPICAL MINYAK GORENG POUCH 2 L', '', 25550, 25550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8992946121029'),
(418, 'SHINZUI HERBA MATSU OIL FACIAL WASH 40 ML', '', 11450, 11450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8992946514302'),
(419, 'FORVITA MINYAK GORENG POUCH 1800 ML', '', 22950, 22950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8992946528118'),
(420, 'KECAP MANIS ECHO 475 ML', '', 8250, 8250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8992975100651'),
(421, 'ECHO KECAP MANIS 210 ML', '', 3850, 3850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8992975101016'),
(422, 'VIDA SOSIS GORENG (DAGING OLAHAN) ISI 6', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8992990520854'),
(423, 'SMAX RING CHEESE FLAVOUR 50 G', '', 4450, 4450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8993004785160'),
(424, 'INDOMIK SUSU KENTAL MANIS COKLAT SACHET 40 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8993007001359'),
(425, 'INDOMILK SUSU KENTAL MANIS PUTIH SACHET 40 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8993007001557'),
(426, 'HAPPYTOS TORTILLA CHIPS  MERAH 160 G', '', 9950, 9950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8993027111601'),
(427, 'HAPPYTOS TORTILLA CHIPS HIJAU 160 G', '', 9950, 9950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8993027121600'),
(428, 'REGAL MARIE BISCUITS 125 G', '', 8250, 8250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8993039111255'),
(429, 'REGAL BISKUIT MARIE 250 G', '', 18950, 18950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8993039112511'),
(430, 'MIE CAP AYAM 2 TELOR KUNING 200 G ', '', 3900, 3900, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8993047301112'),
(431, 'MIE CAP AYAM 2 TELOR MERAH 200 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8993047311111'),
(432, 'PASEO HYGIENIC SOFT AND NATURAL ELEGANT 250 S', '', 13250, 13250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8993053121018'),
(433, 'EXTRA JOSS ACTIVE PER 1 SACHET 4.6 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8993058000684'),
(434, 'TEPUNG BERAS PUTIH ROSE BRAND 500 G ', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8993093115008'),
(435, 'TEPUNG KETAN PUTIH ROSE BRAND 500 G ', '', 8650, 8650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8993093135006'),
(436, 'SO NICE SOSIS SIAP MAKAN RASA AYAM 20 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8993110071119'),
(437, 'SO NICE SOSIS SIAP MAKAN RASA SAPI 20 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8993110071126'),
(438, 'GARAM CAP KAPAL 250 G ', '', 1300, 1300, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8993111112507'),
(439, 'MIE KREMEZ RASA SAMBAL BALADO 15 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8993118622580'),
(440, 'MIE KREMEZ SHORR SAMBAL BALADO 12 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8993118622603'),
(441, 'MIE KREMEZ SHORR KEJU MANIS 12 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8993118622610'),
(442, 'MIE KREMEZ SHORR AYAM PANGGANG 12 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8993118622627'),
(443, 'MIE KREMEZZ SHAKE & SHORR AYAM PANGGANG 22 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8993118937066'),
(444, 'MIE KREMEZZ SHAKE & SHORR SAMBAL BALADO 22 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8993118937073'),
(445, 'MIE KREMEZZ SHAKE & SHORR KEJU MANIS 22 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8993118937097'),
(446, 'MIE KREMEZZ MIX SHAKE RASA AYAM PANGGANG 32 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 08:38:50', '', 3, 13, 10, 1, '8993118937103'),
(447, 'MIE KREMEZZ MIX SHAKE RASA SAMBAL BALADO 32 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8993118937110'),
(448, 'NABATI RICHOCO SIIP CHOCOCHIZ 6.5 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8993175531696'),
(449, 'RICHEESE NABATI SIIP CHEESE FLAVOUR 6.5 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8993175531702'),
(450, 'RICHEESE NABATI SIIP ROASTED CHEESE CORN FLAVOUR 6.5 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8993175531719'),
(451, 'RICHEESE AHH 6.5 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8993175532297'),
(452, 'MINYAK KAYU PUTIH CAP LANG BOTOL 60 ML', '', 16150, 16150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8993176110074'),
(453, 'MINYAK KAYU PUTIH CAP LANG 15 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8993176110098'),
(454, 'GPU LINIMENT OIL BOTOL 30 ML', '', 8000, 8000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8993176110111'),
(455, 'MINYAK KAYU PUTIH CAP LANG AROMATERAPI 15 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8993176111255'),
(456, 'MINYAK KAYU PUTIH CAP LANG AROMA THERAPY BOTOL 60 ML', '', 16950, 16950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8993176111279'),
(457, 'CHARM BODYFIT EXTRA MAXI 8 PADS', '', 3750, 3750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8993189270284'),
(458, 'CHARM BODYFIT EXTRA MAXI WING 5 PADS', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8993189270321'),
(459, 'MAMYPOKO PANTS STANDAR UKURAN M SACHET 1 PCS', '', 2650, 2650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8993189270796'),
(460, 'MAMYPOKO PANTS STANDAR UKURAN L SACHET 1 PCS', '', 2750, 2750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8993189270819'),
(461, 'MAMYPOKO PANTS STANDAR UKURAN S SACHET 1 PCS', '', 2150, 2150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8993189271748'),
(462, 'MAMYPOKO PANTS STANDAR UKURAN MINI S SACHET 1 PCS', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8993189272929'),
(463, 'MAMYPOKO PAINTS EXTRA DRY UKURAN-M  9 PCS', '', 22750, 22750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8993189272981'),
(464, 'CHARM BODYFIT ULTRA SLIM 5 PADS', '', 7900, 7900, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8993189320927'),
(465, 'CHARM BODYFIT FRAGRANCE 9 PADS ', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8993189321375'),
(466, 'FIESTA BRATWURST SAUSAGE 200 G', '', 28500, 28500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8993207100081'),
(467, 'FIESTA FRENCH FRIES SHOESTRING CURAH 500 G', '', 15450, 15450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8993207140346'),
(468, 'FIESTA FRENCH FRIES SHOESTRING 1 KG', '', 32000, 32000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8993207571720'),
(469, 'CHAMP SOSIS AYAM 150 G (ISI 6 POTONG)', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8993207730028'),
(470, 'CHAMP SOSIS AYAM 375 G (ISI 15 POTONG)', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8993207730035'),
(471, 'VISINE TETES MATA STERIL 6 ML', '', 12500, 12500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8993212100069'),
(472, 'GARAM REFINA 250 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8993226112508'),
(473, 'TEPUNG TERIGU SEGITIGA BIRU PREMIUM 1 KG', '', 10350, 10350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8993296201119'),
(474, 'TEPUNG TERIGU SEGITIGA BIRU TRANSPARAN 500 G', '', 4600, 4600, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8993296205001'),
(475, 'TEPUNG TERIGU SEGITIGA BIRU TRANSPARAN 1 KG', '', 8950, 8950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8993296210005'),
(476, 'DIXI CUKA BOTOL 150 ML', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8993366201216'),
(477, 'ELLIPS SMOOTH & SHINY 6 CAPSULES', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8993417200014'),
(478, 'ELLIPS HAIR VITAMIN HAIR TREATMENT  6 CAPSULES', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8993417200021'),
(479, 'ELLIPS HAIR VITAMIN  NUTRI COLOR  6 CAPSULES', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8993417200038'),
(480, 'SANIA MINYAK GORENG POUCH 2 L', '', 24950, 24950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8993496001076'),
(481, 'SANIA MINYAK GORENG POUCH 1 L', '', 13250, 13250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8993496001083'),
(482, 'MINYAK GORENG FORTUNE 1 L', '', 12150, 12150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8993496106504'),
(483, 'KINGS FISHER SARDEN GREEN CHILLI 425 G ', '', 17450, 17450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8993523101816'),
(484, 'RON 88 AIR MINERAL CUP 240 ML', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8993539102401'),
(485, 'HARPIC POWER PLUS 585 ML', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8993560033095'),
(486, 'VANISH CAIR PENGHILANG NODA BOTOL 500 ML', '', 17450, 17450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8993560033279'),
(487, 'MOMOGI JAGUNG BAKAR 8 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8994075230399'),
(488, 'MOMOGI KEJU SWISS 8 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8994075230412'),
(489, 'MOMOGI COKLAT 8 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8994075230436'),
(490, 'MOMOGI CAPPUCCINO 8 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8994075230863'),
(491, 'MOMOGI TUTTI FRUTTI 8 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8994075230870'),
(492, 'COTTON BUDS INDOMARET 100 PCS', '', 4450, 4450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8994096210592'),
(493, 'LARISST GULA PASIR PREMIUM 1 KG', '', 14250, 14250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8994096215467'),
(494, 'COUNTRY CHOICE MANGGA 250 ML', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8994116101183'),
(495, 'COUNTRY CHOICE GUAVA 250 ML', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8994116101190'),
(496, 'TIGA MAWAR SEJATI SOSIS SAPI 170 G', '', 7500, 7500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8994161101039'),
(497, 'TIGA MAWAR SEJATI SOSIS AYAM 170 G', '', 7500, 7500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8994161101107'),
(498, 'BASO SAPI ASLI MAWAR 320 G', '', 20650, 20650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8994161111168'),
(499, 'BASO SAPI ASLI MAWAR 220 G (ISI 25 BUTIR)', '', 14450, 14450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8994161111175'),
(500, 'LUWAK WHITE KOFFIE CARAMEL 20 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8994171101074'),
(501, 'LUWAK WHITE KOFFIE ORIGINAL 20 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8994171101289'),
(502, 'LUWAK WHITE KOFFIE FRENCH VANILLA 20 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8994171101760'),
(503, 'LUWAK WHITE KOFFIE MOCCA ROSE 20 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8994171101777'),
(504, 'MEIJI HELLO PANDA 15 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8994504102112'),
(505, 'MEIJI HELLO PANDA COKELAT 45 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8994504102310'),
(506, 'MEIJI LUCKY STICK 45 G', '', 7050, 7050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8994504121311'),
(507, 'ARNOTTS TIMTAM HAZELNUT & NUTS 100 G', '', 8547, 8547, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8994755010266'),
(508, 'NYAM NYAM BUBBLE PUFF BLUEBERRY 18 G', '', 2650, 2650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8994755020258'),
(509, 'ARNOTTS NYAM NYAM ICE DREAM BIRU 28 G', '', 3750, 3750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8994755020326'),
(510, 'ARNOTTS NYAM NYAM ICE DREAM PINK 28 G', '', 3750, 3750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8994755020340'),
(511, 'ARNOTTS SHAPES CHEEZY KEJU PIZZA 80 G', '', 7750, 7750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8994755090039'),
(512, 'BUMAS SANTAN KELAPA 90 ML', '', 2650, 2650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8994759211034'),
(513, 'TICTIC GARLIC FLAVOUR 70 G', '', 5550, 5550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8994834001192'),
(514, 'SUKI STIK ALA JEPANG RASA AYAM KECAP 125 G', '', 5150, 5150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8994834002243'),
(515, 'SUKRO DUA KELINCI ORIGINAL 140 G', '', 6650, 6650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8995077600166'),
(516, 'TIC TAC DUA KELINCI PEDAS 100 G', '', 4050, 4050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8995077600814'),
(517, 'TIC TAC DUA KELINCI SAPI PANGGANG 100 G', '', 4050, 4050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8995077601514'),
(518, 'DEKA CREPES CHOCO BANANA 100 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8995077603808'),
(519, 'DEKA CREPES CHOCONUT 14 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8995077603822'),
(520, 'DEKA CREPES CHOCO BANANA 14 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8995077603839'),
(521, 'KRIP KRIP SPICY BBQ 19 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8995077604409'),
(522, 'DUA KELINCI KRIP KRIP SWEET CHILI 19 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8995077604416'),
(523, 'SUKRO DUA KELINCI ORIGINAL 28 G', '', 1550, 1550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8995077604515'),
(524, 'MAMASUKA RAJAWALI AGAR AGAR BUBUK 7 G', '', 2250, 2250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8995102701318'),
(525, 'MAMASUKA RAJAWALI AGAR AGAR BUBUK MERAH 7 G', '', 2250, 2250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8995102701325'),
(526, 'MAMASUKA TEPUNG BAKWAN 100 G', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8995102702117'),
(527, 'MAMASUKA TEPUNG UDANG GORENG 90 G', '', 2150, 2150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8995102702315'),
(528, 'MAMASUKA TEPUNG BUMBU PEDAS 90 G', '', 2750, 2750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8995102702513'),
(529, 'MAMASUKA TEPUNG PISANG GORENG RASA KEJU 80 G', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8995102702636'),
(530, 'MAMASUKA SUP KRIM AYAM 55 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8995102702902'),
(531, 'MAMASUKA SUP KRIM JAGUNG 55 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8995102702919'),
(532, 'MAMASUKA SUP KRIM JAMUR 55 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8995102702926'),
(533, 'MAMASUKA BUMBU NASI GORENG 20 G', '', 1450, 1450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8995102703060'),
(534, 'MERBABU GULA HALUS 300 G', '', 7950, 7950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8995152001789'),
(535, 'MERBABU GULA MERAH 400 G', '', 10750, 10750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8995152001802'),
(536, 'MERBABU KETAN PUTIH 500 G', '', 11950, 11950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8995152002274'),
(537, 'MERBABU KETAN HITAM 500 G', '', 23950, 23950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8995152002298'),
(538, 'GULAKU HIJAU PREMIUM 1 KG ', '', 14950, 14950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8995177101112'),
(539, 'GULAKU KUNING 1 KG', '', 14950, 14950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8995177102058'),
(540, 'GULAKU HIJAU 1/2 KG', '', 7150, 7150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8995177109996'),
(541, 'MINUMAN CINCAU CAP PANDA 310 ML', '', 3150, 3150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8995227500582'),
(542, 'ROMA BISKUIT KELAPA 300 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8996001301142'),
(543, 'ROMA MALKIST ABON GURIH 27 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8996001302316'),
(544, 'ROMA SARI GANDUM SANDWICH PEANUT BUTTER 155 G', '', 6750, 6750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8996001305003'),
(545, 'KIS MINT CHERRY 125 G', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8996001326220'),
(546, 'KIS MINT GRAPE 125 G', '', 5400, 5400, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8996001326275'),
(547, 'KOPIKO COFFESHOT CLASSIC 40 G', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8996001340448'),
(548, 'CHOKI CHOKI CHOCOCASHEW 11 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8996001370063'),
(549, 'ENERGEN CEREAL VANILA SACHET 29 G ', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8996001440124'),
(550, 'SUPER BUBUR ABON SAPI 49 G', '', 2750, 2750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8996001524015'),
(551, 'TEH PUCUK HARUM MELATI BOTOL 350 ML', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8996001600146'),
(552, 'KOPIKO 78 C BOTOL 250 ML', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8996001600221'),
(553, 'TEH PUCUK HARUM LESS SUGAR 350 ML', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8996001600252'),
(554, 'SOSRO FRUIT TEA APEL KOTAK 200 ML', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8996006120014'),
(555, 'SOSRO TEH BOTOL KOTAK 250 ML', '', 3250, 3250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8996006142511'),
(556, 'SOSRO FRUIT TEA STROBERI 200 ML', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8996006742025'),
(557, 'TEBS TEA WITH SODA 330 ML', '', 4450, 4450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8996006853127'),
(558, 'SOSRO FRUIT TEA APEL BOTOL 500 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8996006853387'),
(559, 'SOSRO FRUIT TEA STROBERI 500 ML', '', 6750, 6750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8996006853394'),
(560, 'SOSRO FRUIT TEA BLACKCURRANT 500 ML', '', 6750, 6750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8996006853400'),
(561, 'SOSRO FRUIT TEA XTREME BOTOL 500 ML', '', 6750, 6750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8996006853431'),
(562, 'TEH BOTOL SOSRO MELATI 450 ML', '', 5500, 5500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8996006855145'),
(563, 'JOY TEA GREEN TEA JASMINE 500 ML', '', 5150, 5150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8996006856142'),
(564, 'TEH BOTOL SOSRO LESS SUGAR 250 ML', '', 2850, 2850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8996006856197'),
(565, 'TEH BOTOL SOSRO LESS SUGAR 450 ML', '', 5150, 5150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8996006856203'),
(566, 'S TEE MINUMAN TEH MELATI 330 ML', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8996006856876'),
(567, 'PIATTOS RASA RUMPUT LAUT 85 G', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8996196002954'),
(568, 'JACK N JILL CHIZ KING CHEESE FLAVOUR 90 G', '', 9450, 9450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8996196075958'),
(569, 'JACK N JILL DYNAMITE PERMEN MINT ISI COKLAT 150 G', '', 5000, 5000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8996196102555'),
(570, 'NARAYA OAT CHOCO 100 G', '', 9950, 9950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8997002052675'),
(571, 'OISHI PILLOWS COKLAT ISI COKLAT 130 G', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8997004301108'),
(572, 'OISHI PILLOWS RASA UBI 130 G ', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8997004301245'),
(573, 'OISHI SPONGE CRUNCH RASA COKLAT 120 G', '', 9450, 9450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8997004301429'),
(574, 'OISHI SPONGE CRUNCH STROBERI 120 G', '', 9450, 9450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8997004301436'),
(575, 'OISHI SPONGE CRUNCH MOCHACCINO 120 G', '', 9450, 9450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8997004301474'),
(576, 'OISHI SUKY SUKY SEAWEED FLAVOR 14 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8997004306448'),
(577, 'OISHI SUKY SUKY SWEET & SPICY SHRIMP FLAVOR 14 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8997004306455'),
(578, 'PRIMA DJAJA RAYA GULA PASIR TEBU 1 KG', '', 13450, 13450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8997006530025'),
(579, 'YOU C1000 ORANGE 140 ML', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8997009510055'),
(580, 'YOU C1000 LEMON WATER BOTOL 500 ML', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8997009510116'),
(581, 'YOU C1000 ORANGE WATER 500 ML', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8997009510123'),
(582, 'PROCHIZ CHEDDAR SLICE 85 G  ', '', 6550, 6550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8997014450070'),
(583, 'PROCHIZ SPREADABLE 180 G ', '', 12950, 12950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8997014450155'),
(584, 'PROCHIZ GOLD 180 G ', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8997014450216'),
(585, 'TASTYMAX BRATWURST ORIGINAL 500 G', '', 36750, 36750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8997017560585'),
(586, 'FUTAMI 17 GREEN TEA RASA LECI 485 ML', '', 5650, 5650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8997020760156'),
(587, 'PRONAS JAGUNG PIPILAN KALENG 425 G', '', 13250, 13250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8997021220017'),
(588, 'BIG COLA STRAWBERRY 535 ML', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8997025060039'),
(589, 'SAMBEL BAWANG CAP SWAN TERBANG 500 ML', '', 1500, 1500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8997029640046'),
(590, 'TINS BABY WIPE GREEN TEA 10 PCS', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8997030760016'),
(591, 'TARO NET POTATO BBQ 70 G', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8997032680299'),
(592, 'TARONET 3DPOTATO TORNADO CHEESE 40 G', '', 3996, 3996, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8997032680312'),
(593, 'BRAVO SPONGE CRUNCH MILK CHOCOLATE 40 G', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8997032680503'),
(594, 'POCARI SWEAT BOTOL 350 ML', '', 5750, 5750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8997035563544'),
(595, 'ULTRA MILK FULL CREAM 250 ML', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8998009010224'),
(596, 'ULTRA MILK COKLAT 250 ML', '', 4800, 4800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8998009010231'),
(597, 'ULTRA MILK STRAWBERRY 250 ML', '', 5000, 5000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8998009010248'),
(598, 'ULTRA MILK RASA COKLAT 200 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8998009010569'),
(599, 'ULTRA MILK RASA STRAWBERRY 200 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8998009010576'),
(600, 'ULTRA MILK COKLAT 125 ML', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8998009010590'),
(601, 'ULTRA MILK STRAWBERRY 125 ML', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8998009010606'),
(602, 'ULTRA MILK FULL CREAM 1 L', '', 15950, 15950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8998009010613'),
(603, 'ULTRA MILK COKLAT 1 L', '', 15950, 15950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8998009010620'),
(604, 'ULTRA MILK LOW FAT PLAIN 200 ML', '', 4500, 4500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8998009010682'),
(605, 'ULTRA MIMI RASA COKLAT 125 ML', '', 2550, 2550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8998009010910'),
(606, 'ULTRA MILK LOW FAT COKLAT 200 ML', '', 4500, 4500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8998009011344'),
(607, 'ULTRA MIMI FULL CREAM 125 ML', '', 2750, 2750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8998009011696'),
(608, 'BUAVITA ORANGE 250 ML', '', 6050, 6050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8998009020179'),
(609, 'BUAVITA APPLE 250 ML', '', 6250, 6250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8998009020223'),
(610, 'BUAVITA SELECTION MANGO JUICE 1 L', '', 25450, 25450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8998009020315'),
(611, 'TEH KOTAK TEH MELATI 200 ML', '', 3250, 3250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8998009040023'),
(612, 'ULTRA JAYA MINUMAN SARI KACANG IJO 200 ML', '', 3250, 3250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8998009050060'),
(613, 'CUSSON BABY SOFT  & SMOOTH SOAP 75 G', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8998103000534'),
(614, 'CUSSONS BABY SABUN BATANG SOFT & SMOOTH 75 G', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8998103000534'),
(615, 'CUSSON BABY ANTIBACTERIAL SOAP 75 G', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8998103008233'),
(616, 'CUSSONS BABY SABUN BATANG PROTECT CARE 75 G', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8998103008233'),
(617, 'CAREX NATURAL ANTIBACTERIAL HANDWASH FRESH POUCH 200 ML', '', 13450, 13450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8998103012339'),
(618, 'MINYAK GORENG FORTUNE POUCH 2 L', '', 20950, 20950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8998225800043'),
(619, 'PARAMEX SAKIT KEPALA STRIP 4 TABLET', '', 2300, 2300, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8998667100206'),
(620, 'TERMOREX SIRUP TURUN PANAS RASA JERUK BOTOL 30 ML', '', 9950, 9950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8998667300231'),
(621, 'SILADEX SIRUP BATUK DAN BERDAHAK 60 ML', '', 10950, 10950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8998667300491'),
(622, 'KONICARE MINYAK TELON 60 ML', '', 18750, 18750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8998667400283'),
(623, 'NANO NANO NOUGHAT ORIGINAL 20 G', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8998685028001'),
(624, 'NANO NANO NOUGHAT RASA STROBERI 20 G', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8998685171004'),
(625, 'NANO NANO MILKY VANILA 12 G', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8998685177013'),
(626, 'NANO NANO SPESIAL IDUL FITRI 130 G', '', 11450, 11450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8998685178218'),
(627, 'NANO NANO MILKY STROBERI 12 G', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8998685179017'),
(628, 'NANO NANO MILKY COKLAT 12 G', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8998685221013'),
(629, 'SOBISCO TINI WINI BITI RASA COKLAT 20 G', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8998694110889'),
(630, 'SOBISCO TINI WINI BITI RASA KEJU 20 G', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8998694110896'),
(631, 'SOBISCO TINI WINI BITI RASA STROBERI 20 G', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8998694110902'),
(632, 'CHOCO MANIA CHOCOLATE CHIP COOKIES 270 G', '', 19950, 19950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8998694120260'),
(633, 'SOBISCO CHOCO MANIA RICH CHOCOLATE 90 G', '', 6750, 6750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8998694120345'),
(634, 'SOBISCO CHOCO MANIA CHEESALICIOUS 90 G', '', 6500, 6500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8998694120987'),
(635, 'KODOMO TOOTHPASTE WITH XYLITOL 45 G', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8998866100229'),
(636, 'EMERON NUTRITIVE SHAMPO BLACK & SHINE 80 ML', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8998866100557'),
(637, 'PASTA GIGI CIPTADENT SPEARMINT 75 G', '', 3150, 3150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8998866105057'),
(638, 'KODOMO PRO KIDS 1', '', 9150, 9150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8998866105354'),
(639, 'MAMA LIME TOTAL CLEAN GREEN TEA POUCH 800 ML', '', 12800, 12800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8998866106160'),
(640, 'PASTA GIGI CIPTADENT FRESH SPRING MINT 75 G', '', 3150, 3150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8998866181068'),
(641, 'MI SEDAP GORENG 91 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8998866200301'),
(642, 'MI SEDAP RASA AYAM BAWANG 70 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8998866200318'),
(643, 'MIE SEDAP CUP MI GORENG 83 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8998866200813'),
(644, 'MIE SEDAP CUP SOTO 77 G', '', 3650, 3650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8998866200837'),
(645, 'MIE SEDAP CUP KARI SPESIAL 83 G', '', 3650, 3650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8998866200844'),
(646, 'MIE SEDAP CUP BAKSO SPESIAL 72 G', '', 3650, 3650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8998866200851'),
(647, 'ALE ALE RASA JERUK 200 ML', '', 1050, 1050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8998866500319'),
(648, 'FLORIDINA FLORIDA ORANGE 360 ML', '', 3000, 3000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8998866500708'),
(649, 'ALE ALE RASA ANGGUR 200 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8998866500715'),
(650, 'NUVO FAMILY CARING SABUN BATANG 80 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8998866602549'),
(651, 'NUVO FAMILY CLASSIC SABUN BATANG 80 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8998866602556'),
(652, 'NUVO FAMILY ENERGIZING 80 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8998866602570'),
(653, 'SO KLIN PEWANGI ROMANTIC PINK POUCH 900 ML', '', 9500, 9500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8998866602730'),
(654, 'SO KLIN LANTAI AROMATIC SPA LAVENDER 800 ML', '', 8950, 8950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8998866603539'),
(655, 'RAPIKA BIANG 4 IN 1 SWEET PINK SPRAY 250 ML', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8998866604703'),
(656, 'GIV WHITE SABUN BATANG BENGKOANG 80 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8998866608305'),
(657, 'SO KLIN PEWANGI ROYAL BLUE SACHET 15 ML', '', 350, 350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8998866608480'),
(658, 'DAIA + SOFTENER 900 G', '', 14750, 14750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8998866608725'),
(659, 'SO KLIN RAPIKA BIANG 4 IN 1 SWEET PINK POUCH 250 ML', '', 2750, 2750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8998866608862'),
(660, 'SO KLIN RAPIKA BIANG 4 IN 1 LOVELY LAVENDER POUCH 250 ML', '', 2750, 2750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8998866608879'),
(661, 'DAIA + SOFTENER 700 G', '', 9750, 9750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8998866609043'),
(662, 'SO KLIN LANTAI CITRUS LEMON POUCH 800 ML', '', 9000, 9000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8998866679664'),
(663, 'MARJAN BOUDOIN LECI 550 ML', '', 18950, 18950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8998888110817'),
(664, 'MAESTRO MAYONNAISE SACHET 100 G', '', 4500, 4500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8998888461117'),
(665, 'DEL MONTE EXTRA HOT CHILLI SAUCE BOTOL 140 ML', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8998888710567'),
(666, 'DEL MONTE EXTRA HOT 1 KG', '', 16250, 16250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8998888710574'),
(667, 'TOLAK ANGIN SIDOMUNCUL SACHET 15 ML', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8998898101409'),
(668, 'TOLAK ANGIN FLU SACHET 15 ML', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8998898280401'),
(669, 'MINUMAN SERBUK JAHE WANGI SIDOMUNCUL 5 X 25 G', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8998898804119'),
(670, 'BAYCLIN REGULAR BOTOL 200 ML', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8998899013046'),
(671, 'BAYGON NATURAL ORANGE 600 ML', '', 34950, 34950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8998899400334'),
(672, 'CLARIS RC FOODSAVER 1.35 L', '', 11500, 11500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8999118273807'),
(673, 'THE TASTY GROOVE ROOT BEER BOTOL 250 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8999510800113'),
(674, 'NIVEA BODY LOTION INTENSIVE MOISTURE 100 ML', '', 13450, 13450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8999777000004'),
(675, 'NIVEA BODY LOTION UV WHITENING 100 ML', '', 14450, 14450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8999777000042'),
(676, 'TOTAL CARE COOL MINT 250 ML ', '', 14350, 14350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8999908205704'),
(677, 'MARINA NATURAL RICH MOISTURIZING 100 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8999908214706'),
(678, 'ESPE LASEGAR RASA LECI KALENG 320 ML', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8999988778860'),
(679, 'ESPE LARUTAN PENYEGAR RASA JERUK KALENG 320 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8999988888811'),
(680, 'ESPE LARUTAN PENYEGAR RASA JAMBU KALENG 320 ML', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8999988888842'),
(681, 'ESPE LARUTAN PENYEGAR RASA LECI KALENG 320 ML', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8999988888866'),
(682, 'LARUTAN PENYEGAR CAP BADAK BOTOL 200 ML ', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8999988888989'),
(683, 'TARO NET POTATO BBQ 10 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8999999000059'),
(684, 'TARO NET SEAWEED 40 G', '', 4050, 4050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8999999000189'),
(685, 'LIFEBUOY MILDCARE HAND WASH POUCH 180 ML', '', 12950, 12950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8999999001056'),
(686, 'LIFEBUOY BODY WASH TOTAL 10 POUCH 450 ML ', '', 21450, 21450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8999999001193'),
(687, 'LIFEBUOY MILDCARE BOTOL 100 ML ', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8999999001247'),
(688, 'VASELINE MENBODY BODY LOTION 100 ML', '', 17950, 17950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8999999001933'),
(689, 'BANGO KECAP MANIS BOTOL 275 ML', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8999999002503'),
(690, 'PONDS PURE WHITE DEEP CLEANING FACIAL FOAM 50 G', '', 15550, 15550, '2015-09-29 14:48:07', '2015-10-12 06:04:33', '', 13, 26, 8, 4, '8999999003395'),
(691, 'CITRA LASTING WHITE UV BENGKOANG SUSU 250 ML', '', 19950, 19950, '2015-09-29 14:48:07', '2015-09-29 08:17:43', '', 6, 27, 10, 2, '8999999003753'),
(692, 'CITRA BODY SCRUB BENGKOANG 200 ML ', '', 16950, 16950, '2015-09-29 14:48:07', '2015-10-10 03:43:38', '', 14, 28, 10, 3, '8999999004248'),
(693, 'LIFEBUOY VITA PROTECT POUCH 250 ML', '', 12350, 12350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8999999005443'),
(694, 'DOVE WHITENING ORIGINAL DEO ROLL-ON 40 ML', '', 18950, 18950, '2015-09-29 14:48:07', '2015-10-12 05:54:36', '', 7, 30, 8, 1, '8999999005580'),
(695, 'MAGNUM ALMOND 90 ML', '', 12000, 12000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8999999005610'),
(696, 'SIKAT GIGI PEPSODENT DOUBLE CARE SENSITIVE SOFT ISI 2', '', 9150, 9150, '2015-09-29 14:48:07', '2015-10-12 06:02:11', '', 8, 32, 9, 1, '8999999005801'),
(697, 'PADDLE POP CHOCO MAGMA 60 ML', '', 3000, 3000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8999999008208'),
(698, 'PADDLE POP RAINBOW POWER 60 ML', '', 3000, 3000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8999999008215'),
(699, 'RINSO ANTI NODA 700 G', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8999999008376'),
(700, 'REXONA MEN ADVENTURE DEO 40 ML ', '', 13450, 13450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8999999008543'),
(701, 'BANGO KECAP MANIS POUCH 220 ML', '', 8750, 8750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8999999012625'),
(702, 'RINSO MOLTO ULTRA CAIR POUCH 400 ML ', '', 9650, 9650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8999999016111'),
(703, 'LIFEBUOY MILDCARE SABUN BATANG 80 G', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8999999021979'),
(704, 'RINSO MOLTO ULTRA BUBUK 50 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8999999023805'),
(705, 'BANGO KECAP MANIS PEDAS GURIH POUCH 220 ML ', '', 9250, 9250, '2015-09-29 14:48:07', '2015-10-05 04:18:33', '', 1, 8, 10, 1, '8999999026257'),
(706, 'LIFEBUOY SHAMPO ANTI KETOMBE 10 ML', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8999999027056'),
(707, 'SUNLIGHT JERUK ANTI BAU POUCH 800 ML', '', 15950, 15950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8999999027469'),
(708, 'CLEAR COMPLETE SOFT CARE SHAMPO SACHET 10 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8999999027605'),
(709, 'PADDLE POP COLOR POPPER 58 ML', '', 3000, 3000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8999999028336'),
(710, 'PEPSODENT FRESH COOL MINT 75 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8999999028695'),
(711, 'PASTA GIGI PEPSODENT FRESH COOL MINT 190 G', '', 8950, 8950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8999999028701'),
(712, 'SUNSILK NOURISHING SOFT & SMOOTH SHAMPO BOTOL 80 ML', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8999999029333'),
(713, 'SUNSILK BLACK SHINE SHAMPO BOTOL 80 ML', '', 8250, 8250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8999999029364'),
(714, 'CLEAR COMPLETE SOFT CARE BOTOL 170 ML', '', 22050, 22050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8999999029685'),
(715, 'LIFEBUOY TOTAL 10 SABUN BATANG 115 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8999999030353'),
(716, 'LIFEBUOY MILDCARE SABUN BATANG 115 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8999999030360'),
(717, 'LUX BODY WASH FRESH SPLASH POUCH 450 ML', '', 21450, 21450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8999999030926'),
(718, 'PONDS WHITE BEAUTY SUN DULLNESS REMOVAL 50 G', '', 15550, 15550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8999999030988'),
(719, 'BUAVITA STRAWBERRY SMOOTHIEZ 75 ML', '', 6000, 6000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8999999031947'),
(720, 'BLUEBAND + OMEGA 3 SACHET 200 G', '', 6250, 6250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8999999034153'),
(721, 'BLUEBAND + OMEGA 3 CUP 250 G', '', 10450, 10450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8999999034160');
INSERT INTO `items` (`id`, `item_name`, `item_thumbnail`, `item_price`, `discounted_item_price`, `created_at`, `updated_at`, `description`, `category_id`, `subcategory_id`, `stock`, `views`, `barcode`) VALUES
(722, 'BLUEBAND SERBAGUNA KALENG 1 KG', '', 49750, 49750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8999999034177'),
(723, 'CORNETTO BLACK & WHITE 110 ML', '', 6800, 6800, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8999999034566'),
(724, 'CORNETTO MINI CHOCOLATE & VANILLA 28 ML', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8999999034641'),
(725, 'CORNETTO MINI COOKIES & BLACK FOREST 28 ML', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8999999034658'),
(726, 'CORNETTO MINI TIRAMISU & DARK CHOCOLATE 28 ML', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8999999034672'),
(727, 'BANGO KECAP MANIS SACHET 30 ML ', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8999999034825'),
(728, 'SARIWANGI SARIMURNI POUCH 38 G', '', 4000, 4000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8999999035396'),
(729, 'MAGNUM MINI ALMOND 36 G', '', 6500, 6500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8999999035631'),
(730, 'ROYCO AYAM SACHET 7 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8999999036041'),
(731, 'ROYCO RASA SAPI SACHET 7 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8999999036058'),
(732, 'MOLTO ULTRA PURE SOFTENER KONSENTRAT POUCH 900 ML', '', 24950, 24950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8999999036430'),
(733, 'LUX BODY WASH SOFT TOUCH BATANG 85 G', '', 3000, 3000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8999999036607'),
(734, 'LUX BODY WASH VELVET TOUCH BATANG 85 G', '', 3000, 3000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8999999036638'),
(735, 'LUX BODY WASH WHITE GLAMOUR BATANG 85 G', '', 3000, 3000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8999999036706'),
(736, 'LUX SOFT TOUCH BODY WASH POUCH 450 ML', '', 21450, 21450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8999999036829'),
(737, 'LUX AQUA SPARKLE BODY WASH POUCH 450 ML', '', 21450, 21450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8999999036843'),
(738, 'LUX BODY WASH VELVET TOUCH POUCH 250 ML', '', 12450, 12450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8999999036904'),
(739, 'SUNLIGHT JERUK NIPIS POUCH 85 ML', '', 1850, 1850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8999999037352'),
(740, 'BUAVITA KELAPA 1000 ML', '', 20750, 20750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8999999037468'),
(741, 'DUNG DUNG KACANG HIJAU 38 ML', '', 3500, 3500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8999999037635'),
(742, 'ROYCO BUMBU KOMPLIT IKAN GORENG SACHET 22.5 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8999999039240'),
(743, 'ROYCO BUMBU KOMPLIT NASI GORENG PEDAS SACHET 17 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8999999039288'),
(744, 'ROYCO BUMBU KOMPLIT SAYUR ASEM SACHET 22 G ', '', 1450, 1450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8999999039820'),
(745, 'ROYCO BUMBU KOMPLIT TAHU & TEMPE GORENG SACHET 17 G', '', 1200, 1200, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8999999039837'),
(746, 'ROYCO BUMBU KOMPLIT NASI GORENG 17 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8999999039868'),
(747, 'CIF SPRAY PEMBERSIH SERBAGUNA 500 ML', '', 14850, 14850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8999999039974'),
(748, 'TRESEMME ANTI HAIR FALL CONDITIONER BOTOL 170 ML', '', 22250, 22250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8999999041113'),
(749, 'TRESEMME ANTI HAIR FALL SHAMPOO BOTOL 170 ML', '', 21950, 21950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8999999041120'),
(750, 'TRESEMME SMOOTH & SHINE CONDITIONER BOTOL 170 ML', '', 22150, 22150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8999999041137'),
(751, 'TRESEMME SMOOTH & SHINE SHAMPOO BOTOL 170 ML', '', 21950, 21950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8999999041144'),
(752, 'CORNETTO MINI GREEN TEA & CHOCOLATE 28 ML', '', 2500, 2500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8999999042172'),
(753, 'SUNSILK LIVELY STRAIGHT SHAMPO SACHET 10 ML', '', 850, 850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8999999042899'),
(754, 'SIKAT GIGI PEPSODENT DEEP CLEAN ISI 3 MEDIUM', '', 14950, 14950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8999999043261'),
(755, 'MAGNUM MINI CLASSIC 36 G', '', 6500, 6500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8999999043339'),
(756, 'MOLTO TRIKA FLORAL BLISS 400 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8999999043360'),
(757, 'CORNETTO TAYLOR SWIFT CHOCOBERRY 110 ML', '', 7200, 7200, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8999999044404'),
(758, 'PADDLE POP OCEAN FREEZE 56 ML', '', 3000, 3000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8999999044886'),
(759, 'MOLTO PERFUME ESSENCE SEKALI BILAS SACHET 22 ML', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8999999045654'),
(760, 'MOLTO WHITE MUSK POUCH 900 ML', '', 25750, 25750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8999999046156'),
(761, 'BANGO KECAP MANIS POUCH 600 ML', '', 20950, 20950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8999999100506'),
(762, 'ROYCO AYAM POUCH 100 G', '', 4350, 4350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8999999192150'),
(763, 'SARIWANGI TEH ASLI 46.25 G', '', 4850, 4850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8999999195649'),
(764, 'BLUEBAND SERBAGUNA KALENG 1 KG', '', 49250, 49250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8999999198893'),
(765, 'PADDLE POP TRICO 60 ML', '', 2300, 2300, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8999999280253'),
(766, 'POPULAIRE COKLAT 90 ML', '', 4100, 4100, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8999999282196'),
(767, 'POPULAIRE STRAWBERRY VANILA 90 ML', '', 4000, 4000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8999999282202'),
(768, 'FEAST VANILA 65 ML', '', 5000, 5000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8999999294021'),
(769, 'FEAST CHOCOLATE 65 ML', '', 5000, 5000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8999999294038'),
(770, 'SUNLIGHT LEMON POUCH 400 ML ', '', 9650, 9650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8999999390167'),
(771, 'SUNLIGHT JERUK NIPIS POUCH 800 ML', '', 13750, 13750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8999999390198'),
(772, 'SUNLIGHT JERUK NIPIS POUCH 200 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8999999390419'),
(773, 'MOLTO PEWANGI FLORAL BLISS POUCH 900 ML', '', 10750, 10750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8999999400958'),
(774, 'RINSO ANTI NODA 900 G', '', 17950, 17950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8999999401238'),
(775, 'RINSO ANTI NODA 1400 G', '', 26750, 26750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8999999401245'),
(776, 'RINSO COLOUR & CARE DETERGEN 800 G', '', 17650, 17650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8999999401269'),
(777, 'MOLTO SOFTENER SPRING BLUE POUCH 900 ML', '', 12850, 12850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8999999403119'),
(778, 'SUPER PELL PINE ESSENCE POUCH 800 ML', '', 10250, 10250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8999999406974'),
(779, 'PASTA GIGI PEPSODENT WHITE 75 G', '', 3750, 3750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8999999706081'),
(780, 'PASTA GIGI PEPSODENT WHITE 25 G', '', 1650, 1650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8999999706111'),
(781, 'PASTA GIGI PEPSODENT GIGI SUSU ORANGE 50 G', '', 5500, 5500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8999999707743'),
(782, 'PASTA GIGI PEPSODENT WHITENING 75 G', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8999999707774'),
(783, 'PASTA GIGI PEPSODENT HERBAL 120 G', '', 9950, 9950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8999999710873'),
(784, 'PONDS OIL CONTROL SKIN MATTIFYING FACIAL FOAM 100 G', '', 24950, 24950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8999999716998'),
(785, 'PONDS CLEAR SOLUTIONS ANTI BACTERIAL FACIAL SCRUB 100 G', '', 24950, 24950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8999999717025'),
(786, 'PONDS WHITE BEAUTY LIGHTENING FACIAL FOAM  100 G', '', 24750, 24750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8999999717094'),
(787, 'SIKAT GIGI PEPSODENT DOUBLE CARE CLEAN', '', 8950, 8950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8999999717643'),
(788, 'LIFEBUOY MEN BODY WASH DEODORISING BOTOL 100 ML ', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8999999719852'),
(789, 'REXONA MEN ICE COOL DEO SPRAY 150 ML', '', 34450, 34450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '9300663458257'),
(790, 'DOVE WHITENING ORIGINAL DEO SPRAY 100 G', '', 35550, 35550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '9300830007394'),
(791, 'INDOCAFE COFFEMIX SACHET 20 G ', '', 1100, 1100, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '9311931024036'),
(792, 'MAX TEA TEH TARIK SACHET 25 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '9311931201208'),
(793, 'NESTLE KITKAT COKLAT 2F 17 G ', '', 3996, 3996, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '95502489'),
(794, 'MILO ACTIGEN PROTO MALT 300 G', '', 28650, 28650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '9556001008633'),
(795, 'NESCAFE LATTE KALENG 240 ML', '', 7850, 7850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '9556001025272'),
(796, 'NESTLE CERELAC APEL, JERUK & PISANG 120 G', '', 9700, 9700, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '9556001040398'),
(797, 'NESCAFE ORIGINAL KALENG 240 ML', '', 7650, 7650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '9556001047175'),
(798, 'NESTLE MILO ORIGINAL KALENG 240 ML', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '9556001051509'),
(799, 'NESCAFE MOCCA KALENG 240 ML', '', 7850, 7850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '9556001054005'),
(800, 'NESTLE CERELAC SUSU BERAS MERAH 120 G', '', 15000, 15000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '9556001131867'),
(801, 'NESTLE KITKAT COKLAT 4F 35 G ', '', 7950, 7950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '9556001670700'),
(802, 'NESTLE CERELAC BERAS MERAH 120 G', '', 9000, 9000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '9556001975904'),
(803, 'JOHNSONS BABY TOP TO TOE WASH 200 ML', '', 18250, 18250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '9556006012093'),
(804, 'QUAKER OATMEAL 800 G', '', 35550, 35550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '9556174802212'),
(805, 'QUAKER INSTAN OATMEAL MERAH 200 G', '', 9850, 9850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '9556174802236'),
(806, 'ISOLASI HITAM KECIL', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '100123'),
(807, 'ISOLASI BENING SEDANG ', '', 2250, 2250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '100124'),
(808, 'SIDU BUKU TULIS 58 LBR', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8991389220054'),
(809, 'SIDU BUKU TULIS 38 LBR', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8991389220016'),
(810, 'POP MIE RASA KARI KEJU 75 G', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '089686060560'),
(811, 'MILO SEREAL KOTAK 170 G', '', 21250, 21250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '4800361002974'),
(812, 'ARNOTTS SHAPES CHEEZY KEJU ABON 80 G', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8994755090053'),
(813, 'MOLTO ULTRA ANTI BAKTERI SACHET 14 ML', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8999999043964'),
(814, 'MOLTO ALL IN ONE PINK SACHET 14 ML', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8999999043957'),
(815, 'MOLTO ALL IN ONE BIRU SACHET 14 ML', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8999999043940'),
(816, 'CHAMP SOSIS SAPI SIAP SANTAP 20 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8993207120072'),
(817, 'ENERGEN RASA JAHE SACHET 29 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8996001440353'),
(818, 'ENERGEN RASA COKLAT SACHET 29 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8996001440049'),
(819, 'SARIAYU LULUR PUTIH LANGSAT 175 G', '', 15950, 15950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8990090400779'),
(820, 'LULUR MANDI PURBASARI BENGKOANG 250 G', '', 15950, 15950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8999225783152'),
(821, 'KUSUKA RASA BBQ 60 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '053314502210'),
(822, 'KUSUKA RASA KEJU BAKAR 60 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '053314502234'),
(823, 'OISHI PILLOWS DURIAN 130 G', '', 8450, 8450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8997004301283'),
(824, 'SHINZUI BODY LOTION KEIKO 210 ML', '', 21950, 21950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8992946527623'),
(825, 'PEPSODENT GIGI SUSU STRAWBERRY BUBBLE 50 G', '', 5750, 5750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8999999705985'),
(826, 'PEPSODENT ACTION ANTI BACTERIAL 75 G', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8999999037819'),
(827, 'HEAD & SHOULDERS CLEAN BALANCED SACHET 10 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '4902430566988'),
(828, 'HEAD & SHOULDERS SMOOTH SILKY SACHET 10 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '4902430566834'),
(829, 'DOVE HAIRTHERAPY DAMAGE SOLUTION SACHET 10 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8999999039141'),
(830, 'DOVE HAIRTHERAPY DANDRUFF CARE SACHET 10 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8999999041205'),
(831, 'CLEAR SHAMPO ICE COOL MENTHOL SACHET 10 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8999999027599'),
(832, 'CHOKI CHOKI CHOCOBERRY & MILK 11 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8996001370513'),
(833, 'YUPI EXOTIC MANGO 10 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8992741904988'),
(834, 'YUPI MILLY MOOS 9 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992741955140'),
(835, 'YUPI BURGER 9 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8992741941006'),
(836, 'ALE ALE STROBERI 200 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-10-09 00:56:09', '', 1, 8, 10, 0, '8998866500326'),
(837, 'OKKY JELLY DRINK BLACKCURRANT 150 ML', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8992775406069'),
(838, 'MINTZ PEPPERMINT 115 G', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8991102281416'),
(839, 'FRUIT TEA XTREME 200 ML', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8996006853462'),
(840, 'ULTRA MILK LOW FAT COKLAT 1 L', '', 17950, 17950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8998009011207'),
(841, 'YOU C1000 LEMON 140 ML', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8997009510017'),
(842, 'PEPSODENT HERBAL 75 G', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8999999710880'),
(843, 'NYAM NYAM FANTASY STICK STRAWBERRY 25 G', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8886015403034'),
(844, 'NYAM NYAM ICE DREAM MILKY VANILA 28 G', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8994755020333'),
(845, 'NYAM NYAM BUBBLE PUFF CHOCO 18 G', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8886015414061'),
(846, 'ENERGEN RASA KACANG HIJAU 30 G', '', 1250, 1250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8996001440087'),
(847, 'PEPSODENT EXPERT PROTECTION GENTLE WHITE 65 G', '', 9850, 9850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8999999032647'),
(848, 'RINSO ANTI NODA SACHET 50 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8999999037260'),
(849, 'RINSO MATIC POUCH 800 ML', '', 20950, 20950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8999999041748'),
(850, 'RICHEESE SIIP ROASTED CHEESE CORN FLAVOUR 30 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8993175538886'),
(851, 'RICHEESE SIIP CHEESE FLAVOUR 35 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8993175536912'),
(852, 'BISVIT SELIMUT 50 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8993175538527'),
(853, 'RICHEESE AHH KEMASAN EKONOMIS 32.5 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8993175538282'),
(854, 'RICHEESE BISVIT SELIMUT  11 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8993175533799'),
(855, 'RICHOHO BISVIT SELIMUT 11 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8993175537919'),
(856, 'RICHEESE DELISH BAKED POTATO CRACKERS & CHEESE 11 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8993175533645'),
(857, 'RICHEESE PASTA KEJU ', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8993175537988'),
(858, 'NEXTAR NASTAR COOKIES 42 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8993175538541'),
(859, 'RICHEESE AHH TRIPLE CHEESE 17.5 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8993175536813'),
(860, 'ANAKONIDIN OBH 30 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8998667300545'),
(861, 'ANAKONIDIN 30 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8998667300255'),
(862, 'SUPER PELL LAVENDER & GREEN TEA 800 ML', '', 9450, 9450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8999999406998'),
(863, 'BIORE BODY FOAM WHITENING SCRUB POUCH 250 ML', '', 12750, 12750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8992727002066'),
(864, 'BIORE BODY FOAM FRESHY BLOSSOM POUCH 250 ML', '', 12950, 12950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8992727004114'),
(865, 'PEPSI BLUE 410 ML', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '089686816068'),
(866, 'LAURIER ACTIVE DAY SUPER MAXI WING 10 BUAH', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8992727002998'),
(867, 'LAURIER ACTIVE DAY DOUBLE COMFORT 25 CM', '', 11950, 11950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992727004541'),
(868, 'LAURIER RELAX NIGHT 35 CM', '', 8250, 8250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8992727003087'),
(869, 'MULTI FACIAL TISSUE 200 SHEET', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8992931025035'),
(870, 'TESSA TISU SAKU 10 SHEET', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8992931006072'),
(871, 'YUPI GUMMY CANDIES BURGER 108 G', '', 9450, 9450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8992741941303'),
(872, 'OREO COCONUT DELIGHT 137 G', '', 7150, 7150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '7622210360472'),
(873, 'OREO VANILA CREAM 137 G', '', 7150, 7150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8992760221028'),
(874, 'ESPE LARUTAN PENYEGAR  RASA ANGGUR 320 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8999988888859'),
(875, 'ESPE LARUTAN PENYEGAR RASA MELON 320 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8999988888835'),
(876, 'VIT LEVITE JERUK 350 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8992752121497'),
(877, 'VIT LEVITE JAMBU BIJI 350 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8992752121598'),
(878, 'ORIGINAL LOVE JUICE POMEGRANATE 300 ML', '', 5950, 5950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8997009780397'),
(879, 'MOGU MOGU RASA STRAWBERRY 320 ML', '', 7450, 7450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8850389108055'),
(880, 'NESCAFE SMOOVLATTE 190 ML', '', 7750, 7750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8850127061642'),
(881, 'GOOD DAY TIRAMISU BLISS COFFEE 250 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8991002121003'),
(882, 'GOOD DAY FUNASTIC MOCHACINNO COFFEE 250 ML', '', 5750, 5750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8991002121010'),
(883, 'ULTRA SARI KACANG IJO 250 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8998009050053'),
(884, 'YEO''S GRASS JELLY DRINK 300 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '9556156003385'),
(885, 'MERBABU KERUPUK BAWANG 200 G', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8995152001543'),
(886, 'MERBABU KERUPUK RAKYAT 250 G', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8995152003035'),
(887, 'PRONAS KORNET DAGING SAPI 340 G', '', 23450, 23450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8992804113401'),
(888, 'INDOFOOD SAMBAL PEDAS BOTOL140 ML', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '089686400427'),
(889, 'LAY''S FIESTA BBQ FLAVOUR 35 G', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '089686596540'),
(890, 'LAY''S FIESTA BBQ FLAVOUR 68 G', '', 9750, 9750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '089686596564'),
(891, 'LAY''S NORI SEAWEED FLAVOUR 35 G', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '089686596441'),
(892, 'LAY''S GRILLED CHICKEN PAPRIKA FLAVOUR 35 G', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '089686596878'),
(893, 'ATTACK PLUS SOFTENER 800 G', '', 19850, 19850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8992727002479'),
(894, 'NARAYA OAT CHOCO CHOCOLATE FLAVOUR 90 G', '', 10450, 10450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8997002053870'),
(895, 'TARO NET 3D VEGGIE 40 G', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8997032680206'),
(896, 'MALKIST SEAWEED 135 G', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8888166607927'),
(897, 'MALKIST CRACKERS 115 G', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8888166989344'),
(898, 'LASERIN 30 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8992828831527'),
(899, 'SOFFEL  BUNGA GERANIUM 80 G', '', 8750, 8750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8992772215015'),
(900, 'SOFFEL KULIT JERUK 80 G', '', 9450, 9450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992772315128'),
(901, 'SIKAT BAGUS PP BRISTLE ', '', 7950, 7950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8886020209300'),
(902, 'DETTOL SKIN CARE HAND SOAP POUCH 200 ML', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8993560027230'),
(903, 'DETTOL SENSITIVE HAND SOAP POUCH 200 ML', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8993560027216'),
(904, 'DETTOL ORIGINAL HAND SOAP POUCH 200 ML', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8993560027001'),
(905, 'NEXCARE DAILY SHOWER PUFF', '', 18950, 18950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8992806902966'),
(906, 'HOLISTI CARE SUPER ESTER C 4 TAB', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8992863661035'),
(907, 'TEMPRA PARACETAMOL ANAK SIRUP RASA ANGGUR 30 ML', '', 19450, 19450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8995201800974'),
(908, 'TARO NET 3D POTATO JUNGLE CHICKEN', '', 4150, 4150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8997032680329'),
(909, 'ADEM SARI 7 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8992772123068'),
(910, 'NETSLE HONEY STAR 32 G', '', 7250, 7250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8992696428300'),
(911, 'IZZI BODY MIST SWEET LOVE 100 ML', '', 14950, 14950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8992856890824'),
(912, 'IZZI BODY MIST DAZZLING LOVE 100 ML', '', 14950, 14950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8992856890817'),
(913, 'IZZI BODY MIST TRUE LOVE 80 ML', '', 14950, 14950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8992856891654'),
(914, 'FISHERMAN''S FRIEND  SUGAR FREE LEMON 25 G', '', 11950, 11950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '50604159'),
(915, 'FISHERMAN''S FRIEND  SUGAR FREE BLACKCURRANT 25 G', '', 11950, 11950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '96068809'),
(916, 'ANTIMO DIMENHYDRINATE 10 TAB', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8993498210230'),
(917, 'FISHERMAN''S FRIEND  SUGAR FREE MINT 25 G', '', 11950, 11950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '50357154'),
(918, 'FRESH CARE STRONG  ROLL ON 10 ML', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8997021870151'),
(919, 'FRESH CARE GREEN TEA  ROLL ON 10 ML', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8997021870052'),
(920, 'FRESH CARE CITRUS  ROLL ON 10 ML', '', 13950, 13950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8997021870014'),
(921, 'ANTIS ANTISEPTIC PEMBERSIH TANGAN JERUK NIPIS 60 ML', '', 9850, 9850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8992772265027'),
(922, 'ANTIS ANTISEPTIC PEMBERSIH TANGAN FRESH CLEAN 60 ML', '', 9850, 9850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8992772265010'),
(923, 'JOHNSON''S BABY POWDER BLOSSOM 100 G', '', 6250, 6250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8991111101514'),
(924, 'CERES CLASSIC 50 G', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8991001301055'),
(925, 'CUSSON BABY OIL SOFT & SMOOTH 100 ML', '', 17250, 17250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8998103004495'),
(926, 'DETTOL INSTAN HAND SANITIZER 50 ML', '', 12750, 12750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8993560027247'),
(927, 'VASELINE TOTAL MOISTURE COCA GLOW 200 ML', '', 32450, 32450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8999999020293'),
(928, 'MOLTO SOFTENER BLOSSOM PINK POUCH 900 ML', '', 12750, 12750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8999999403188'),
(929, 'SWALLOW NAPHTHALENE DISK BALL 35 G', '', 2950, 2950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8886020001119'),
(930, 'EMERON HAIR FALL CONTROL 80 ML', '', 6150, 6150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8998866100595'),
(931, 'SWALLOW NAPHTHALENE DISK BALL 300 G G', '', 14950, 14950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8886020001133'),
(932, 'DOVE NOURISHING BLACK SHAMPOO 70 ML', '', 9950, 9950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8999999041236'),
(933, 'NESTLE DANCOW INSTAN FORTIGO VANILA 27 G', '', 3150, 3150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992696405417'),
(934, 'INDOFOOD AYAM BAKAR BUMBU RUJAK 50 G', '', 4450, 4450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '089686441253'),
(935, 'PROCHIZ SLICE 10  LEMBAR 170 G', '', 12650, 12650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8997014450094'),
(936, 'SILADEX BATUK BERDAHAK 30 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8998667300484'),
(937, 'SILADEX  BATUK & PILEK 30 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8998667300507'),
(938, 'MAMASUKA RAJAWALI AGAR AGAR BUBUK HIJAU 7 G', '', 2000, 2000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8995102701332'),
(939, 'BLUE BAND CAKE AND COOKIE 1 KG', '', 47350, 47350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8999999031091'),
(940, 'TESSA TRAVEL TISSUE ANGRY BIRDS 120 SHEET', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8992931005174'),
(941, 'FANTA ORANGE 425 ML', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8992761147051'),
(942, 'ULTRA MILK LOW FAT 250 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8998009010262'),
(943, 'ULTRA MIMI RASA STROBERI 125 ML', '', 2550, 2550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8998009010927'),
(944, 'INDOMILK UHT RASA STROBERI 190 ML', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8993007000109'),
(945, 'INDOMILK UHT RASA COKLAT 115 ML', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8993007000239'),
(946, 'INDOMILK UHT RASA COKLAT 190 ML', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8993007000062'),
(947, 'BUAVITA GRAPE 250 ML', '', 6250, 6250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8999999023171'),
(948, 'FRISIAN FLAG FULL CREAM 250 ML', '', 4750, 4750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8992753033744'),
(949, 'FRISIAN FLAG KENTAL MANIS 370 G', '', 10250, 10250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8992753101207'),
(950, 'TEH PUCUK HARUM LESS SUGAR 480 ML', '', 4950, 4950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8996001600191'),
(951, 'MIE SEDAP GORENG AYAM KRISPI 88 G', '', 2050, 2050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8998866200936'),
(952, 'POLYTEX SABUT SPON REGULAR ', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8992742370683'),
(953, 'MINYAK KAYU PUTIH CAP LANG 120 ML', '', 32450, 32450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8993176110067'),
(954, 'MINYAK KAYU PUTIH CAP LANG 30 ML', '', 9850, 9850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8993176110081'),
(955, 'HARPIC POWER PLUS ROSE 585 ML', '', 15450, 15450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8993560033071'),
(956, 'VANISH CAIR 1 LITER', '', 32850, 32850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8993560081027'),
(957, 'ESPE LARUTAN PENYEGAR RASA STRAWBERRY 320 ML', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8999988888828'),
(958, 'MINYAK GORENG FILMA POUCH 1 LITER', '', 14050, 14050, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8992826111072'),
(959, 'DESAKU KETUMBAR BUBUK 5 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8997011930223'),
(960, 'JASJUS RASA JAMBU 8 G', '', 350, 350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8998866200134'),
(961, 'JASJUS RASA STRAWBERRY 8 G', '', 350, 350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8998866200110'),
(962, 'JASJUS RASA MANGGA 8 G', '', 350, 350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8998866200158'),
(963, 'JASJUS RASA MELON 8 G', '', 350, 350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8998866200127'),
(964, 'JASJUS RASA JERUK 8 G', '', 350, 350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8998866200097'),
(965, 'JASJUS RASA SIRSAK 8 G ', '', 350, 350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8998866200592'),
(966, 'YUPI ICE CREAM CONE 9 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992741904728'),
(967, 'MIE TELOR CAP 3 AYAM MERAH 200 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '089686000016'),
(968, 'DETTOL BODY WASH ORIGINAL 125 ML', '', 12750, 12750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8993560026400'),
(969, 'DETTOL COOL BODY WASH 125 ML', '', 12750, 12750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8993560026417'),
(970, 'NATUR GINSENG EXTRACT SHAMPOO 140 ML', '', 20750, 20750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8992928121405'),
(971, 'NATUR ALOEVERA EXTRACT SHAMPOO 140 ML', '', 20750, 20750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8992928060186'),
(972, 'MAYASI KACANG JEPANG RASA JAGUNG BAKAR 70 G', '', 8150, 8150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8991002502826'),
(973, 'RINSO ANTI NODA ADVANCE FOAM POUCH 800 ML', '', 15950, 15950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8999999006006'),
(974, 'SURF + SOFTENER DETERGEN BUBUK 900 ML', '', 15850, 15850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8999999043902'),
(975, 'MIZONE ISOTONIK ORANGE LIME 500 ML', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8992752118107'),
(976, 'POPMIE RASA AYAM BAWANG 75 G', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '089686060065'),
(977, 'KARA SANTAN KELAPA 200 ML', '', 6950, 6950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8992717880186'),
(978, 'INDOMIE TOA BULGOGI ALA KOREA 90 G', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '089686043532'),
(979, 'MOLTO ULTRA ANTI BAKTERI 900 ML', '', 22950, 22950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8999999031336'),
(980, 'MAYASI KACANG JEPANG HOT SENZA  70 G', '', 8150, 8150, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8991002502390'),
(981, 'TANGO WAFER CHOCOLATE 171 G', '', 10950, 10950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8991102300421'),
(982, 'OREO BISKUIT VANILLA 29.4 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8992760121014'),
(983, 'OREO DOUBLE DELIGHT 29.4 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '8992760121083'),
(984, 'ARNOTTS GOOD TIME DOUBLE CHOCOCHIPS COOKIES 80 G', '', 7750, 7750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '8886015203115'),
(985, 'ARNOTTS GOOD TIME COFFEE CHOCO CHIPS COOKIES 80 G', '', 7750, 7750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8994755030240'),
(986, 'ARNOTTS GOOD TIME BROWNIES CHOCOCHIPS COOKIES 76 G', '', 7750, 7750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8994755030264'),
(987, 'NUTRISARI AMERICAN SWEET ORANGE 140 G', '', 21950, 21950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '749921005953'),
(988, 'NUTRISARI FLORIDA ORANGE 140 G', '', 21950, 21950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '749921006127'),
(989, 'KERTAS KADO KIKY ', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '100125'),
(990, 'SAMPUL BUKU COKLAT SPONGEBOB', '', 250, 250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '100126'),
(991, 'PLASTIK SAMPUL BUKU RAJAWALI', '', 350, 350, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '100127'),
(992, 'PLASTIK SAMPUL BUKU RAJAWALI BESAR', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '100128'),
(993, 'M2000 KOREK API BARA ', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '100130'),
(994, 'MARY QUEEN CHOCOLATE CASHEWNUT 20 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8992952925390'),
(995, 'KENTANG GORENG IDAHO CURAH 2.5 KG', '', 80000, 80000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '100131'),
(996, 'LEM EROKOL ', '', 1450, 1450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '100132'),
(997, 'SERUTAN I-GLOO MOPED ', '', 2450, 2450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '3154145347548'),
(998, 'SNOWMAN BOARDMARKER RED ', '', 7750, 7750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '4970129727521'),
(999, 'SNOWMAN BOARDMARKER BLUE ', '', 7750, 7750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '4970129727538'),
(1000, 'JOYKO ERASER BLACK ', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8993988090069'),
(1001, 'TAO KAE NOI CRISPY SEAWEED BIG SHEET 4 G', '', 3750, 3750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8857107232016'),
(1002, 'PADDLE POP SHAKYZ SHAKE 150 ML', '', 7700, 7700, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8999999275440'),
(1003, 'FRISIAN FLAG FULL CREAM GOLD SACHET 40 G', '', 1950, 1950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 10, 10, 0, '8992753100002'),
(1004, 'FRISIAN FLAG KENTAL MANIS COKELAT SACHET 40 G', '', 1450, 1450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 11, 11, 10, 0, '8992753102006'),
(1005, 'LA PASTA ROYALE CHEESE BOLOGNESE SAUCE 102 G', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 12, 10, 0, '8992933653212'),
(1006, 'LA PASTA SPICY BARBEQUE 57 G', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 13, 10, 0, '8992933652215'),
(1007, 'SUNSILK SHAMPO ANTI DANDRUFF 80 ML', '', 8550, 8550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 9, 14, 10, 0, '8999999029241'),
(1008, 'SUNSILK SHAMPO HAIR FALL SOLUTION 80 ML', '', 8550, 8550, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 3, 15, 10, 0, '8999999029302'),
(1009, 'TAO KAE NOI  CRISPY SEAWEED BIG SHEET SPICY FLAVOUR 4 G', '', 3750, 3750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 16, 10, 0, '8857107232023'),
(1010, 'MIZONE LYCHEE LEMON 500 ML ', '', 3450, 3450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 17, 10, 0, '8992752119104'),
(1011, 'TARO NET POTATO BBQ 40 G', '', 4250, 4250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 10, 18, 10, 0, '8999999000066'),
(1012, 'DELFI TAKE-IT 4 FINGERS 37 G', '', 6650, 6650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 4, 19, 10, 0, '8991001111944'),
(1013, 'DELFI TOP BLACK IN WHITE 9 G', '', 500, 500, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 20, 10, 0, '8991001242976'),
(1014, 'BENG-BENG PEANUT BUTTER 25 G', '', 1850, 1850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 5, 21, 10, 0, '8996001355176'),
(1015, 'TARO NET COWBOY STEAK 10 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 22, 10, 0, '8999999002473'),
(1016, 'ABC HOMESTYLE SAMBAL TERASI SACHET 23 G', '', 1000, 1000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 12, 23, 10, 0, '711844120402'),
(1017, 'ABC TOMATO KETCHUP BOTOL 335 ML', '', 9850, 9850, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 24, 10, 0, '711844130012'),
(1018, 'TARO NET SEAWEED 70 G', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 25, 10, 0, '8997032680275'),
(1019, 'YOU C1000 LEMON 140 ML', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 13, 26, 10, 0, '8997009510017'),
(1020, 'YOU C1000 ORANGE 140 ML', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 6, 27, 10, 0, '8997009510055'),
(1021, 'FANTA STROBERI BOTOL 1 L', '', 8650, 8650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 28, 10, 0, '8992761136185'),
(1022, 'POCARI SWEAT KALENG 330 ML', '', 5450, 5450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 14, 29, 10, 0, '8997035111110'),
(1023, 'FANTA  STROBERI KALENG 330 ML', '', 6450, 6450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 30, 10, 0, '8992761111045'),
(1024, 'SPRITE SLIM KALENG 250 ML', '', 5750, 5750, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 7, 31, 10, 0, '8992761111533'),
(1025, 'ULTRA MILK MOKA 200 ML', '', 5250, 5250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 32, 10, 0, '8998009010583'),
(1026, 'ULTRA MILK FULL CREAM 200 ML', '', 3950, 3950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 8, 33, 10, 0, '8998009010552'),
(1027, 'BIORE LIVELY REFRESH POUCH 250 ML', '', 12450, 12450, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 1, 10, 0, '8992727003872'),
(1028, 'BIORE BODY FOAM REALAXING AROMATIC 250 ML', '', 12950, 12950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 2, 10, 0, '8992727002578'),
(1029, 'BIORE BODY FOAM SWEET PEACH 250 ML', '', 12950, 12950, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 3, 10, 0, '8992727004848'),
(1030, 'HANSA PLAST ELASTIS ISI 10 LEMBAR', '', 3250, 3250, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 4, 10, 0, '8999777026134'),
(1031, 'SPRITE BOTOL 1 L', '', 8650, 8650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 5, 10, 0, '8992761136178'),
(1032, 'COCA COLA BOTOL 1 L', '', 8650, 8650, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 6, 10, 0, '8992761136161'),
(1033, 'PADDLE POP BANANA BOAT 42 ML', '', 4000, 4000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 2, 7, 10, 0, '8851932343831'),
(1034, 'PADDLE POP TORNADO GRAPE 50 G', '', 4000, 4000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 8, 10, 0, '8851932310352'),
(1035, 'FEAST VANILA 65 ML', '', 5000, 5000, '2015-09-29 14:48:07', '2015-09-29 14:48:07', '', 1, 9, 10, 0, '8999999294021');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_05_07_023543_create_items_table', 1),
('2015_05_08_083126_create_customer_table', 1),
('2015_05_30_054436_create_gifts_table', 1),
('2015_05_30_103656_create_coupons_table', 2),
('2015_05_30_105416_add_column_expired', 3),
('2015_05_30_135003_change_schema_coupon_gift', 4),
('2015_05_30_141032_remove_amount', 5),
('2015_05_30_141317_fix_gift', 6),
('2015_05_30_141632_point_of_customer', 7),
('2015_05_30_150508_gifts_claimed', 8),
('2015_05_30_150921_coupons_claimed', 9),
('2015_05_30_165712_coupon_add_thumbnail', 10),
('2015_06_05_141314_create_table_todos', 11),
('2015_06_06_022555_add_remember_token', 12),
('2015_06_07_142455_add_verify_token', 13),
('2015_06_22_135516_create_carts_table', 14),
('2015_06_28_033616_add_table_description_change_categosy', 15),
('2015_06_28_034121_create_categories_table', 16),
('2015_06_28_035227_create_subcategories_table', 17),
('2015_06_28_040053_change_item_category', 18),
('2015_06_28_054627_create_price_filters_table', 19),
('2015_07_24_102726_add_stok_to_item', 20),
('2015_07_24_171037_create_favorites_table', 21),
('2015_07_26_052028_create_recently_vieweds_table', 22),
('2015_07_26_055435_add_views_column', 23),
('2015_08_14_215130_create_admins_table', 24),
('2015_08_25_103351_create_transactions_table', 24),
('2015_08_27_063806_create_cart_transactions_table', 25),
('2015_09_22_121415_create_static_contents_table', 26),
('2015_09_24_013526_create_admins_table', 27),
('2015_09_25_082320_create_claimed_coupons_table', 28),
('2015_09_29_132050_add_barcode', 29),
('2015_10_02_022310_create_claimed_gifts_table', 30),
('2015_10_03_130213_create_settings_table', 31),
('2015_10_04_135512_create_image_sliders_table', 32),
('2015_10_04_144947_add_column_type', 33),
('2015_10_05_103314_add_mobile_image', 34);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `price_filters`
--

CREATE TABLE IF NOT EXISTS `price_filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filter_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bottom_range` int(11) NOT NULL,
  `top_range` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `price_filters`
--

INSERT INTO `price_filters` (`id`, `filter_name`, `bottom_range`, `top_range`, `created_at`, `updated_at`) VALUES
(1, '< 50.000', 0, 50000, '2015-06-28 05:58:17', '2015-06-28 05:58:17'),
(2, '50.000 - 150.000', 50000, 150000, '2015-06-28 05:58:53', '2015-06-28 05:58:53'),
(3, '150.000 - 300.000', 150000, 300000, '2015-06-28 05:59:12', '2015-06-28 05:59:12'),
(4, '> 300.000', 300000, 0, '2015-06-28 06:00:12', '2015-06-28 06:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `recently_vieweds`
--

CREATE TABLE IF NOT EXISTS `recently_vieweds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `last_seen` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `recently_vieweds_customer_id_foreign` (`customer_id`),
  KEY `recently_vieweds_item_id_foreign` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `recently_vieweds`
--

INSERT INTO `recently_vieweds` (`id`, `customer_id`, `item_id`, `last_seen`, `created_at`, `updated_at`) VALUES
(1, 7, 4, '2015-10-12 12:57:50', '2015-07-26 01:17:56', '2015-10-12 05:57:50'),
(2, 7, 3, '2015-10-12 13:04:26', '2015-07-26 02:21:14', '2015-10-12 06:04:26'),
(3, 7, 692, '2015-10-10 10:43:38', '2015-10-10 03:41:27', '2015-10-10 03:43:38'),
(4, 7, 694, '2015-10-12 12:54:33', '2015-10-12 05:54:33', '2015-10-12 05:54:33'),
(5, 7, 690, '2015-10-12 13:04:31', '2015-10-12 05:57:55', '2015-10-12 06:04:31'),
(6, 7, 696, '2015-10-12 13:02:09', '2015-10-12 06:02:09', '2015-10-12 06:02:09');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parameter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value_int` int(11) DEFAULT NULL,
  `value_string` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `parameter`, `value_int`, `value_string`, `created_at`, `updated_at`) VALUES
(1, 'poin_given', 30, NULL, '2015-10-03 13:08:21', '2015-10-04 01:21:51'),
(2, 'purchase_for_poin', 10000, NULL, '2015-10-03 13:24:10', '2015-10-03 13:24:10'),
(3, 'phone', NULL, '085715516893', '2015-10-12 02:57:51', '2015-10-12 02:57:51'),
(4, 'email', NULL, 'destra.bintang.perkasa@gmail.com', '2015-10-12 02:59:00', '2015-10-12 02:59:00'),
(5, 'facebook', NULL, 'https://www.facebook.com/destra.bintangperkasa', '2015-10-12 02:59:42', '2015-10-12 02:59:42'),
(6, 'twitter', NULL, 'https://twitter.com/destraaaa', '2015-10-12 03:03:02', '2015-10-11 20:53:48'),
(7, 'google', NULL, 'https://plus.google.com/+DestraBintangPerkasa', '2015-10-12 03:06:50', '2015-10-12 03:06:50'),
(8, 'instagram', NULL, 'https://instagram.com/destraaaa', '2015-10-12 03:08:27', '2015-10-12 03:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `static_contents`
--

CREATE TABLE IF NOT EXISTS `static_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `featured_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `static_contents`
--

INSERT INTO `static_contents` (`id`, `title`, `content`, `featured_image`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'aboutus_banner.jpg', '2015-09-23 04:49:00', '2015-09-22 23:29:21'),
(2, 'Frequently Asked Question (FAQ)', '<p><strong>Q : Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo?</strong></p>\r\n\r\n<p>A : Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'NULL', '2015-09-23 06:34:05', '2015-09-22 23:42:49'),
(3, 'Alisha M.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>\r\n', 'foto_1.png', '2015-09-23 06:40:30', '2015-09-23 00:22:25'),
(4, 'Hamid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo', 'foto_2.png', '2015-09-23 06:41:21', '2015-09-23 06:41:21');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subcategory_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_foreign` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `subcategory_name`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Telur', 2, '2015-06-28 04:53:38', '2015-06-28 04:53:38'),
(2, 'Roti & Selai', 2, '2015-06-28 04:53:49', '2015-06-28 04:53:49'),
(3, 'Sereal & Oats', 2, '2015-06-28 04:54:25', '2015-06-28 04:54:25'),
(4, 'Makanan Instan', 2, '2015-06-28 04:54:38', '2015-06-28 04:54:38'),
(5, 'Makanan Kaleng', 2, '2015-06-28 04:54:48', '2015-06-28 04:54:48'),
(6, 'Makanan Beku', 2, '2015-06-28 04:54:58', '2015-06-28 04:54:58'),
(7, 'Produk Segar', 2, '2015-06-28 04:55:11', '2015-06-28 04:55:11'),
(8, 'Soft Drink', 1, '2015-06-28 04:58:43', '2015-06-28 04:58:43'),
(9, 'Susu', 1, '2015-06-28 04:59:01', '2015-06-28 04:59:01'),
(10, 'Jus', 1, '2015-06-28 04:59:11', '2015-06-28 04:59:11'),
(11, 'Susu Bubuk', 11, '2015-06-28 05:02:20', '2015-06-28 05:02:20'),
(12, 'Bayi', 9, '2015-09-29 13:01:18', '2015-09-29 13:01:18'),
(13, 'Mie instan', 3, '2015-09-29 13:01:22', '2015-09-29 13:01:22'),
(14, 'Anak', 9, '2015-09-29 13:01:26', '2015-09-29 13:01:26'),
(15, 'Bihun', 3, '2015-09-29 13:01:33', '2015-09-29 13:01:33'),
(16, 'Pembersih Lantai', 10, '2015-09-29 13:01:52', '2015-09-29 13:01:52'),
(17, 'Snack Manis', 4, '2015-09-29 13:02:01', '2015-09-29 13:02:01'),
(18, 'Pembersih Piring', 10, '2015-09-29 13:02:03', '2015-09-29 13:02:03'),
(19, 'Snack Gurih', 4, '2015-09-29 13:02:06', '2015-09-29 13:02:06'),
(20, 'Peralatan Mandi', 5, '2015-09-29 13:02:23', '2015-09-29 13:02:23'),
(21, 'Kosmetik', 5, '2015-09-29 13:02:28', '2015-09-29 13:02:28'),
(22, 'Bahan', 12, '2015-09-29 13:02:30', '2015-09-29 13:02:30'),
(23, 'Bumbu', 12, '2015-09-29 13:02:37', '2015-09-29 13:02:37'),
(24, 'Peralatan Dapur', 6, '2015-09-29 13:02:52', '2015-09-29 13:02:52'),
(25, 'Eskrim 1', 13, '2015-09-29 13:03:03', '2015-09-29 13:03:03'),
(26, 'Eskrim 2', 13, '2015-09-29 13:03:14', '2015-09-29 13:03:14'),
(27, 'Kebun', 6, '2015-09-29 13:03:48', '2015-09-29 13:03:48'),
(28, 'Lain 1', 14, '2015-09-29 13:03:49', '2015-09-29 13:03:49'),
(29, 'Lain 2', 14, '2015-09-29 13:03:57', '2015-09-29 13:03:57'),
(30, 'Obat anak', 7, '2015-09-29 13:03:59', '2015-09-29 13:03:59'),
(31, 'Obat dewasa', 7, '2015-09-29 13:04:06', '2015-09-29 13:04:06'),
(32, 'Alat tulis', 8, '2015-09-29 13:04:22', '2015-09-29 13:04:22'),
(33, 'Buku', 8, '2015-09-29 13:04:27', '2015-09-29 13:04:27');

-- --------------------------------------------------------

--
-- Table structure for table `todos`
--

CREATE TABLE IF NOT EXISTS `todos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isDone` tinyint(1) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `todos_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_address` text COLLATE utf8_unicode_ci NOT NULL,
  `customer_request` text COLLATE utf8_unicode_ci NOT NULL,
  `customer_payment` int(11) NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `transactions_customer_id_foreign` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `customer_name`, `customer_phone`, `customer_email`, `customer_address`, `customer_request`, `customer_payment`, `customer_id`, `created_at`, `updated_at`) VALUES
(1, 'Destra Bintang P', '081234567890', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', 'as;dja', 100000, 7, '2015-09-28 22:40:58', '2015-09-28 22:40:58'),
(2, 'Destra Bintang P', '081234567890', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-09-29 01:07:32', '2015-09-29 01:07:32'),
(3, 'Destra Bintang P', '081234567890', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-01 21:41:55', '2015-10-01 21:41:55'),
(4, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-01 21:48:56', '2015-10-01 21:48:56'),
(5, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-01 21:50:46', '2015-10-01 21:50:46'),
(6, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:06:32', '2015-10-02 00:06:32'),
(7, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:07:36', '2015-10-02 00:07:36'),
(8, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:07:48', '2015-10-02 00:07:48'),
(9, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:10:51', '2015-10-02 00:10:51'),
(10, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:16:49', '2015-10-02 00:16:49'),
(11, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:18:26', '2015-10-02 00:18:26'),
(12, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:20:59', '2015-10-02 00:20:59'),
(13, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:21:16', '2015-10-02 00:21:16'),
(14, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:22:37', '2015-10-02 00:22:37'),
(15, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:43:24', '2015-10-02 00:43:24'),
(16, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-02 00:43:54', '2015-10-02 00:43:54'),
(17, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 200000, 7, '2015-10-02 01:50:20', '2015-10-02 01:50:20'),
(18, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 200000, 7, '2015-10-02 04:31:01', '2015-10-02 04:31:01'),
(19, 'Chrestella Stephanie', '085712345678', 'chrestellastephanie@gmail.com', 'Jalan lagi.', '', 100000, 7, '2015-10-02 04:32:57', '2015-10-02 04:32:57'),
(20, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 5678, 7, '2015-10-02 22:25:37', '2015-10-02 22:25:37'),
(21, 'Destra Bintang P', '085715516893', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-10 03:22:34', '2015-10-10 03:22:34'),
(22, 'Destra Bintang P', '085715516893', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-10 03:23:57', '2015-10-10 03:23:57'),
(23, 'Destra Bintang P', '085715516893', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-10 03:28:54', '2015-10-10 03:28:54'),
(24, 'Destra Bintang P', '085715516893', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-10 03:31:29', '2015-10-10 03:31:29'),
(25, 'Destra Bintang P', '085715516893', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-10 03:50:35', '2015-10-10 03:50:35'),
(26, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-12 05:55:34', '2015-10-12 05:55:34'),
(27, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 10000, 7, '2015-10-12 05:58:29', '2015-10-12 05:58:29'),
(28, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 10000, 7, '2015-10-12 06:03:01', '2015-10-12 06:03:01'),
(29, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 10000, 7, '2015-10-12 06:03:18', '2015-10-12 06:03:18'),
(30, 'Destra Bintang P', '085712345678', '13511057@std.stei.itb.ac.id', 'Jalan lagi.', '', 100000, 7, '2015-10-12 06:06:59', '2015-10-12 06:06:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Destra', 'destra.bintang.perkasa@gmail.com', '$2y$10$UomKRJcmZ1ZTwaHFmhcvIOHjPO5apd5maQoUNfRof6J/4HtWOA4Yq', '2015-06-06 01:52:34', '2015-06-05 20:40:06', 'uPWjmPsEW0TnJOfKt4HwFMgZfLijNNXO2whylDE2ORY71sk0RcoF8S17wMDX');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `carts_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `claimed_coupons`
--
ALTER TABLE `claimed_coupons`
  ADD CONSTRAINT `claimed_coupons_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `claimed_coupons_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `claimed_gifts`
--
ALTER TABLE `claimed_gifts`
  ADD CONSTRAINT `claimed_gifts_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `claimed_gifts_gift_id_foreign` FOREIGN KEY (`gift_id`) REFERENCES `gifts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favorites_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `items_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `recently_vieweds`
--
ALTER TABLE `recently_vieweds`
  ADD CONSTRAINT `recently_vieweds_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `recently_vieweds_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `todos`
--
ALTER TABLE `todos`
  ADD CONSTRAINT `todos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
