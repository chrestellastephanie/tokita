-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 29, 2015 at 03:43 PM
-- Server version: 5.5.42-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `k3559804_tokita`
--

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subcategory_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_foreign` (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `subcategory_name`, `category_id`, `created_at`, `updated_at`) VALUES
(12, 'Bayi', 9, '2015-09-29 13:01:18', '2015-09-29 13:01:18'),
(13, 'Mie instan', 3, '2015-09-29 13:01:22', '2015-09-29 13:01:22'),
(14, 'Anak', 9, '2015-09-29 13:01:26', '2015-09-29 13:01:26'),
(15, 'Bihun', 3, '2015-09-29 13:01:33', '2015-09-29 13:01:33'),
(16, 'Pembersih Lantai', 10, '2015-09-29 13:01:52', '2015-09-29 13:01:52'),
(17, 'Snack Manis', 4, '2015-09-29 13:02:01', '2015-09-29 13:02:01'),
(18, 'Pembersih Piring', 10, '2015-09-29 13:02:03', '2015-09-29 13:02:03'),
(19, 'Snack Gurih', 4, '2015-09-29 13:02:06', '2015-09-29 13:02:06'),
(20, 'Peralatan Mandi', 5, '2015-09-29 13:02:23', '2015-09-29 13:02:23'),
(21, 'Kosmetik', 5, '2015-09-29 13:02:28', '2015-09-29 13:02:28'),
(22, 'Bahan', 12, '2015-09-29 13:02:30', '2015-09-29 13:02:30'),
(23, 'Bumbu', 12, '2015-09-29 13:02:37', '2015-09-29 13:02:37'),
(24, 'Peralatan Dapur', 6, '2015-09-29 13:02:52', '2015-09-29 13:02:52'),
(25, 'Eskrim 1', 13, '2015-09-29 13:03:03', '2015-09-29 13:03:03'),
(26, 'Eskrim 2', 13, '2015-09-29 13:03:14', '2015-09-29 13:03:14'),
(27, 'Kebun', 6, '2015-09-29 13:03:48', '2015-09-29 13:03:48'),
(28, 'Lain 1', 14, '2015-09-29 13:03:49', '2015-09-29 13:03:49'),
(29, 'Lain 2', 14, '2015-09-29 13:03:57', '2015-09-29 13:03:57'),
(30, 'Obat anak', 7, '2015-09-29 13:03:59', '2015-09-29 13:03:59'),
(31, 'Obat dewasa', 7, '2015-09-29 13:04:06', '2015-09-29 13:04:06'),
(32, 'Alat tulis', 8, '2015-09-29 13:04:22', '2015-09-29 13:04:22'),
(33, 'Buku', 8, '2015-09-29 13:04:27', '2015-09-29 13:04:27');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
