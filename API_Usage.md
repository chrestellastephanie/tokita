API Documentation for Tokita Mobile App

Develpment environment
-------------------------------
endpoint: tokita.ap01.aws.af.cm/public/api/mobile
method: post

Production environment
-------------------------------
endpoint: [to be filled later]
method: post

Status Code
-------------------------------
200: success
400: bad request
500: internal server error

Request Body & Response
-------------------------------
Request body consists of array. It specifies the parameters for function in the controller.
Basically, post the following request to the endpoint.
The response is in json format.
	Login (logging in a user)
		Request:
		---------------------
		['request' => 'login',
		'email'=> 'email@example.com',
		'password' => 'password_example']

		Response:
		{
			'status_code': '200',
			'message': 'the message',
			'user':{
				'id' 				: 'user_id',
				'customer_name' 	: 'user_name',
				'customer_email' 	: 'user_email',
				'customer_address' 	: 'user_address',
				'point' 			: '20',
			}
		}
		Basically, you will want to save those information locally if login successful.

	Register (register new user)
		Request:
		---------------------
		['request' => 'register',
		'name' => 'example',
		'email'=> 'email@example.com',
		'password' => 'password_example',
		'address' => 'example street no 12']

		Response:
		{
			'status_code': '201',
			'message': 'Success. Visit email for confirmation.'
		}
		Basically, you will want to save those information.

	Update (updating an account)
		Request:
		---------------------
		['request' => 'update',
		'name' => 'example',
		'email'=> 'email@example.com',
		'address' => 'example street no 12']

		Response:
		{
			'status_code': '200',
			'message': 'the message',
			'user':{
				'id' 				: 'user_id',
				'customer_name' 	: 'user_name',
				'customer_email' 	: 'user_email',
				'customer_address' 	: 'user_address',
				'point' 			: '0',
			}
		}
		Basically, you will want to save those information.
		Note: If email changed, user will be logged out.

	Change Password (updating a password)
		Request:
		---------------------
		['request' => 'change password',
		'user_id' => '1'
		'old_password' => 'password_old',
		'password'=> 'password_new']

		Response:
		{
			'status_code': '200',
			'message': 'the message'
		}
	
	Reset Password (sent a confirmation email to reset password)
		Request:
		---------------------
		['request' => 'reset password',
		'email' => 'example@example.com']

		Response:
		{
			'status_code': '201',
			'message': 'the message'
		}		

	Product (retrieve all product)
		Request:
		---------------------
		['request' => 'product',
		'filter' => 'all']

		Response:
		{
			'status_code': '200',
			'message': 'the message',
			'products':{
				'id' 				: 'user_id',
				'name'			 	: 'user_name',
				'price'			 	: 'user_email',
				'discounted_price' 	: 'user_address',
			}
		}
