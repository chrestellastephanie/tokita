<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

	protected $table = 'transactions';

	protected $fillable = [
		'customer_id',
		'total_amount',
		'customer_name',
		'customer_phone',
		'customer_email',
		'customer_address',
		'customer_request',
		'customer_payment'
	];

}
