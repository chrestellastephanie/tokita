<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimedCoupon extends Model {

	protected $table = 'claimed_coupons';

	protected $fillable = [
		'customer_id',
		'coupon_id',
		'identifier',
		'status'
	];

	public function coupon_details(){
		return Coupon::whereRaw("id = ?",[$this->coupon_id])->first();
	}

	public function customer_details(){
		return Customer::whereRaw("id = ?",[$this->customer_id])->first();
	}
}
