<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model {

	protected $table = 'favorites';

	protected $fillable = [
		'customer_id',
		'item_id'
	];

	public function item_detail(){
		return Item::whereRaw("id = ?",[$this->item_id])->first();
	}

	public function customer_detail(){
		return Customer::whereRaw("id = ?", [$this->customer_id])->first();
	}

}
