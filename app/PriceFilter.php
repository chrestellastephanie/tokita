<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceFilter extends Model {

	protected $fillable = [
		'filter_name',
		'bottom_range',
		'top_range'
	];

}
