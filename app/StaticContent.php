<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticContent extends Model {

	protected $table = 'static_contents';

	protected $fillable = [
		'title',
		'content',
		'featured_image'
	];

}
