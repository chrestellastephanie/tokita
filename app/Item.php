<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

	//
	protected $fillable = [
		'item_name',
		'item_thumbnail',
		'item_price',
		'discounted_item_price',
		'description',
		'category_id',
		'subcategory_id',
		'barcode'
	];

	public function subcategory(){
		return Subcategory::whereRaw("id = ?",[$this->subcategory_id])->first();
	}

	public function category(){
		return Category::whereRaw("id = ?",[$this->category_id])->first();
	}

}
