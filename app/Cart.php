<?php namespace App;

use Illuminate\Database\Eloquent\Model;
class Cart extends Model {

	protected $table = 'carts';

	protected $fillable = [
		'customer_id',
		'item_id',
		'quantity',
		'status'
	];

	public function item_detail(){
		return Item::whereRaw("id = ?",[$this->item_id])->first();
	}

	public function customer_detail(){
		return Customer::whereRaw("id = ?", [$this->customer_id])->first();
	}
}
