<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'WelcomeController@index');

// Route::get('home', 'HomeController@index');

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);

Route::get('/', 'SiteController@home');

// ACCOUNT
Route::get('/register','SiteController@showRegister');
Route::get('/edit-akun','SiteController@showEditAccount');
Route::get('/resetPassword','SiteController@showResetPassword');
Route::post('/changePassword','SiteController@changePassword');
Route::get('/logout','SiteController@logout');
Route::post('/registerAccount','SiteController@register');
Route::post('/login', 'SiteController@login');
Route::post('/checkEmail', 'SiteController@checkEmail');
Route::post('/updateAccount', 'SiteController@updateCustomer');
Route::post('/addToCart', 'SiteController@addToCart');
Route::post('/addToFav', 'SiteController@addToFav');
Route::post('/removeFav', 'SiteController@removeFav');
Route::get('/verify', 'SiteController@verifyToken');
Route::get('/reset', 'SiteController@reset');
Route::post('/sendNewPassword', 'SiteController@sendNewPassword');
Route::post('/api/mobile', 'SiteController@processApi');
Route::get('/api/token', 'SiteController@supplyCsrfToken');
Route::get('api/todo', ['uses' => 'TodoController@index','middleware'=>'simpleauth']);
Route::post('api/todo', ['uses' => 'TodoController@store','middleware'=>'simpleauth']);
Route::post('tk-admin/product/edit/{idProduct}', 'ProductController@update');
Route::get('tk-admin/product/delete/{idProduct}', 'ProductController@destroy');
Route::get('tk-admin/product/{idCategory}', 'AdminController@product');
Route::post('tk-admin/product/save', 'ProductController@store');
Route::get('/user/favorit','SiteController@showFavorit');
Route::get('/coupon/get/{couponId}','SiteController@getCoupon');
Route::get('/gift/get/{giftId}','SiteController@getGift');


// PRODUCTS
Route::get('/products/view/{idCategory?}/{idSubcategory?}','SiteController@showProducts');
Route::post('/search/{searchQuery}', 'SiteController@search');
Route::get('/search/{searchQuery}', 'SiteController@search');
Route::get('/product/addView/{idItem}', 'SiteController@addView');
Route::post('/product/addView/{idItem}', 'SiteController@addView');
Route::get('/product/{event}', 'SiteController@show');

// CART
Route::get('/cart','SiteController@showCart');
Route::get('/cart/delete/{idCart}','SiteController@deleteCart');
Route::get('/cart/deleteAll','SiteController@deleteAllCart');
Route::post('/cart/editCart','SiteController@editCart');
Route::get('/confirmation','SiteController@showConfirmation');
Route::post('/setSessionTransaction','SiteController@setSessionTransaction');
Route::get('/summary','SiteController@showSummary');
Route::get('/order-sent','SiteController@showOrderSent');
Route::get('/invoice','SiteController@invoice');

// PAGES
Route::get('/about','SiteController@showAbout');
Route::get('/faq','SiteController@showFaq');
Route::get('tk-admin/staticcontent/{idStatic}', 'AdminController@staticContent');
Route::post('tk-admin/staticcontent/edit/{idStatic}', 'AdminController@editStaticContent');

// ADMIN
Route::get('/tk-admin', 'AdminController@auth');
Route::get('tk-admin/login', 'AdminController@showLogin');
Route::get('tk-admin/logout', 'AdminController@logout');
Route::post('tk-admin/authorize', 'AdminController@authorize');
Route::get('tk-admin/category', 'AdminController@category');
Route::post('tk-admin/category/edit/{idCategory}', 'AdminController@updateCategory');
Route::get('tk-admin/subcategory/{idCategory}', 'AdminController@subcategory');
Route::post('tk-admin/subcategory/save', 'AdminController@addSubcategory');
Route::post('tk-admin/subcategory/edit/{idSubcategory}', 'AdminController@editSubcategory');
Route::get('tk-admin/subcategory/delete/{idSubcategory}', 'AdminController@deleteSubcategory');
Route::get('tk-admin/customer', 'AdminController@customer');
Route::get('tk-admin/transaction', 'AdminController@transaction');
Route::get('tk-admin/invoice/{idTransaction}', 'AdminController@invoice');
Route::get('tk-admin/gift', 'AdminController@gift');
Route::post('tk-admin/changepoin', 'AdminController@changePoin');
Route::post('tk-admin/changepassword', 'AdminController@changePassword');
Route::post('tk-admin/gift/save', 'AdminController@addGift');
Route::post('tk-admin/gift/edit/{idGift}', 'AdminController@editGift');
Route::get('tk-admin/gift/delete/{idGift}', 'AdminController@deleteGift');
Route::post('tk-admin/slider/save', 'AdminController@addSlider');
Route::post('tk-admin/slider/edit/{idSlider}', 'AdminController@editSlider');
Route::get('tk-admin/slider/delete/{idSlider}', 'AdminController@deleteSlider');
Route::get('tk-admin/coupon', 'AdminController@coupon');
Route::get('tk-admin/slider', 'AdminController@slider');
Route::get('tk-admin/settings', 'AdminController@settings');
Route::post('tk-admin/savecontacts', 'AdminController@saveContacts');
Route::get('tk-admin/claimedcoupon', 'AdminController@claimedCoupon');
Route::get('tk-admin/claimedgift', 'AdminController@claimedgift');
Route::get('tk-admin/claimedgift/changeStatus/{idGift}', 'AdminController@changeStatus');
Route::post('tk-admin/coupon/save', 'AdminController@addCoupon');
Route::post('tk-admin/coupon/edit/{idCoupon}', 'AdminController@editCoupon');
Route::get('tk-admin/coupon/delete/{idCoupon}', 'AdminController@deleteCoupon');
Route::get('tk-admin/cart/{idTransaction}', 'AdminController@cart');