<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Item;
use App\Category;
use App\Subcategory;
use App\Customer;
use App\CartTransaction;
use App\Transaction;
use App\Gift;
use App\Coupon;
use App\StaticContent;
use App\Admin;
use App\Settings;
use App\ClaimedCoupon;
use App\ClaimedGift;
use App\ImageSlider;

use DB;
use Hash;
use Input;
use Redirect;
use URL;
use Session;


class AdminController extends Controller {

	public function auth(){
		if(Session::has('loggedadmin')){
			return Redirect::intended(URL::to('/tk-admin/product/1'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function showLogin(){
		return view('admin.login');
	}

	public function authorize(){
		$email = Input::get('email');
		$password = Input::get('password');
		$users = $this->processLogin($email, $password);
		if($users){
			Session::put('loggedadmin', $users);
			Session::put('success_message', "Anda berhasil masuk.");
			return Redirect::intended('/tk-admin/product/1');
		}
		else{
			Session::put('error_message', "Email atau password salah.");
			return Redirect::intended('/tk-admin/login');
		}
	}

	private function processLogin($email, $password){
		$users = Admin::whereRaw('email = ?', [$email])->first();
		if($users){
			if (Hash::check($password, $users->password))
			{
			    return $users;
			}
		}
	}

	public function logout(){
		Session::forget('loggedadmin');
		Session::put('success_message', "Anda telah signout");
		return Redirect::intended('/tk-admin');
	}

	public function dashboard(){
		$items = Item::all();
		return view("admin.dashboard", compact('items'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function product($idCategory){
		if(Session::has('loggedadmin')){
			$items = Item::whereRaw('category_id = ?', [$idCategory])->get();
			$categories = Category::all();
			$product = "true";
			foreach ($categories as $cat) {
				$subcat = Subcategory::whereRaw('category_id = ?', [$cat->id])->get();
				$cat->subcat = $subcat;
			}
			$current_category = Category::find($idCategory);
			$subcategories = Subcategory::whereRaw('category_id = ?', [$idCategory])->get();
			return view("admin.dashboard", compact('product','items', 'categories', 'current_category', 'subcategories'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function category(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			foreach ($categories as $cat) {
				$cat->count_sub = Subcategory::whereRaw('category_id = ?', [$cat->id])->count();
			}
			return view("admin.category", compact('categories'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function updateCategory($id){
		if(Session::has('loggedadmin')){
			$category = Category::find($id);
			$category->category_name = Input::get('name');
			$category->save();
			return Redirect::intended(URL::to('/tk-admin/category/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function subcategory($idCategory){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$subs = Subcategory::whereRaw('category_id = ?', [$idCategory])->get();
			$current_category = Category::find($idCategory);
			return view("admin.subcategory", compact('categories', 'subs', 'current_category'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function addSubcategory(){
		if(Session::has('loggedadmin')){
			$category_id = Input::get('category_id');
			Subcategory::create([
								'subcategory_name' => Input::get('name'),
								'category_id' => $category_id]);
			return Redirect::intended(URL::to('/tk-admin/subcategory/'.$category_id));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function editSubcategory($id){
		if(Session::has('loggedadmin')){
			$category_id = Input::get('category_id');
			$subcategory = Subcategory::find($id);
			$subcategory->subcategory_name = Input::get('name');
			$subcategory->save();
			return Redirect::intended(URL::to('/tk-admin/subcategory/'.$category_id));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function deleteSubcategory($id){
		if(Session::has('loggedadmin')){
			$current = Subcategory::find($id);
			$category_id = $current->category_id;
			Subcategory::destroy($id);
			return Redirect::intended(URL::to('/tk-admin/subcategory/'.$category_id));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function customer(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$customers = Customer::all();
			return view("admin.customer",compact('categories', 'customers'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function transaction(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$transactions = CartTransaction::groupBy('transaction_id')->get()->sortByDesc('created_at');
			return view("admin.transaction",compact('categories', 'transactions'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function claimedCoupon(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$claimedCoupons = ClaimedCoupon::all();
			return view("admin.claimed_coupon",compact('categories', 'claimedCoupons'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function changePoin(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$poin_given = Settings::whereRaw('parameter = ?', ['poin_given'])->first();
			$purchase_for_poin = Settings::whereRaw('parameter = ?', ['purchase_for_poin'])->first();
			$poin_given->value_int = Input::get('poin_given');
			$poin_given->save();
			$purchase_for_poin->value_int = Input::get('purchase_for_poin');
			$purchase_for_poin->save();
			return Redirect::intended(URL::to('/tk-admin/settings'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function saveContacts(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$inputs = Input::all();
			foreach ($inputs as $key => $value) {
				if($key != '_token'){
					$input = Settings::whereRaw('parameter = ?', [$key])->first();
					$input->value_string = $value;
					$input->save();
				}
			}
			return Redirect::intended(URL::to('/tk-admin/settings'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function changePassword(){
		if(Session::has('loggedadmin')){
			$admin = Session::get('loggedadmin');
			$password_old = Input::get('password_old');
			$change = Admin::find($admin->id);
			if(Hash::check($password_old, $change->password)){
				if(Input::get('password_new') == Input::get('confirm_password')){
					$change->password = Hash::make(Input::get('password_new'));
					$change->save();
					Session::put('success_message', 'password berhasil diganti');
				}
				else{
					Session::put('error_message', 'password baru tidak cocok');
				}
			}
			else{
				Session::put('error_message', 'password salah');
			}
			return Redirect::intended(URL::to('tk-admin/settings'));
		} else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function settings(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$prefs = DB::table('settings')
                     ->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))
                     ->get();
			foreach ($prefs as $pref) {
				$settings[$pref->parameter] = $pref->value;
			}
			if(Session::has('success_message')){
				$success_message = Session::get('success_message');
				Session::forget('success_message');
			}
			if(Session::has('error_message')){
				$error_message = Session::get('error_message');
				Session::forget('error_message');
			}
			return view("admin.settings",compact('categories', 'settings', 'success_message', 'error_message'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function slider(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$sliders = ImageSlider::all();
			return view("admin.slider", compact('categories', 'sliders'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function invoice($id){
		if(Session::has('loggedadmin')){
			$transactions = CartTransaction::whereRaw('transaction_id = ?',[$id])->get();
			$coupon = ClaimedCoupon::whereRaw('status = ?',[$id])->first();
			$total = 0;
			foreach ($transactions as $transaction) {
				$total = $total + ($transaction->cart_detail()->quantity * $transaction->cart_detail()->item_detail()->discounted_item_price);
			}
			if($coupon){
				$total = $total - $coupon->coupon_details()->nominal;
			}
			$prefs = DB::table('settings')
                     ->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))
                     ->get();
			foreach ($prefs as $pref) {
				$settings[$pref->parameter] = $pref->value;
			}
			$tr = Transaction::find($id);
			$name = $tr->customer_name;
			$address = $tr->customer_address;
			return view("invoice_for_admin",compact('name','address','settings','transactions', 'coupon', 'total'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function claimedGift(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$claimedGifts = ClaimedGift::all();
			return view("admin.claimed_gift",compact('categories', 'claimedGifts'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function changeStatus($id){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$gift = ClaimedGift::find($id);
			if($gift->status == 0){
				$gift->status = 1;
			}else{
				$gift->status = 0;
			}
			$gift->save();
			return Redirect::intended(URL::to('tk-admin/claimedgift'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	public function gift(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$gifts = Gift::all();
			return view("admin.gift",compact('categories', 'gifts'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function addGift(){
		if(Session::has('loggedadmin')){
			$image = $_FILES["image"];
			$this->uploadFile($image, 'gifts');
			Gift::create([		'thumbnail' => $image['name'],
								'name' => Input::get('name'),
								'point' => Input::get('point')]);
			return Redirect::intended(URL::to('/tk-admin/gift/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function addSlider(){
		if(Session::has('loggedadmin')){
			$image = $_FILES["image"];
			$image2 = $_FILES["image2"];
			$this->uploadFile($image, 'sliders');
			$this->uploadFile($image, 'mobile_sliders');
			ImageSlider::create(['image' => $image['name'],
								'mobile_image' => $image2['name'],
							'name' => Input::get('name'),
							'type' => Input::get('type'),
							'url' => Input::get('url')]);
			return Redirect::intended(URL::to('/tk-admin/slider/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function editGift($idGift){
		if(Session::has('loggedadmin')){
			$gift = Gift::find($idGift);
			$gift->name = Input::get('name');
			$gift->point = Input::get('point');
			if($_FILES['image']['name']!=""){
				$target_dir = "images/gifts/";
				$target_file = $target_dir . basename($gift->thumbnail);
				if(file_exists($target_file)){
					unlink($target_file);
				}
				$image = $_FILES["image"];
				$this->uploadFile($image, 'gifts');
				$gift->thumbnail = $image['name'];
			}
			$gift->save();
			return Redirect::intended(URL::to('/tk-admin/gift/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function editSlider($idSlider){
		if(Session::has('loggedadmin')){
			$slider = ImageSlider::find($idSlider);
			$slider->name = Input::get('name');
			$slider->type = Input::get('type');
			$slider->url = Input::get('url');
			if($_FILES['image']['name']!=""){
				$target_dir = "images/sliders/";
				$target_file = $target_dir . basename($slider->image);
				if(file_exists($target_file)){
					unlink($target_file);
				}
				$image = $_FILES["image"];
				$this->uploadFile($image, 'sliders');
				$slider->image = $image['name'];
			}
			if($_FILES['image2']['name']!=""){
				$target_dir = "images/mobile_sliders/";
				$target_file = $target_dir . basename($slider->mobile_image);
				if(file_exists($target_file)){
					unlink($target_file);
				}
				$image2 = $_FILES["image2"];
				$this->uploadFile($image2, 'mobile_sliders');
				$slider->mobile_image = $image2['name'];
			}
			$slider->save();
			return Redirect::intended(URL::to('/tk-admin/slider/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function deleteGift($idGift){
		if(Session::has('loggedadmin')){
			Gift::destroy($idGift);
			return Redirect::intended(URL::to('/tk-admin/gift/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function deleteSlider($idSlider){
		if(Session::has('loggedadmin')){
			ImageSlider::destroy($idSlider);
			return Redirect::intended(URL::to('/tk-admin/slider/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function coupon(){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$coupons = Coupon::all();
			return view("admin.coupon",compact('categories', 'coupons'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function addCoupon(){
		if(Session::has('loggedadmin')){
			$image = $_FILES["image"];
			$this->uploadFile($image, 'coupons');
			Coupon::create(['thumbnail' => $image['name'],
							'nominal' => Input::get('nominal'),
							'point' => Input::get('point')]);
			return Redirect::intended(URL::to('/tk-admin/coupon/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function editCoupon($idCoupon){
		if(Session::has('loggedadmin')){
			$coupon = Coupon::find($idCoupon);
			$coupon->nominal = Input::get('nominal');
			$coupon->point = Input::get('point');
			if($_FILES['image']['name']!=""){
				$target_dir = "images/coupons/";
				$target_file = $target_dir . basename($coupon->thumbnail);
				if(file_exists($target_file)){
					unlink($target_file);
				}
				$image = $_FILES["image"];
				$this->uploadFile($image, 'coupons');
				$coupon->thumbnail = $image['name'];
			}
			$coupon->save();
			return Redirect::intended(URL::to('/tk-admin/coupon/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function deleteCoupon($idCoupon){
		if(Session::has('loggedadmin')){
			Coupon::destroy($idCoupon);
			return Redirect::intended(URL::to('/tk-admin/coupon/'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function cart($idTransaction){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$carts = CartTransaction::whereRaw('transaction_id = ?',[$idTransaction])->get();
			$transaction = $idTransaction;
			return view("admin.cart",compact('categories', 'carts', 'transaction'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function staticContent($idStatic){
		if(Session::has('loggedadmin')){
			$categories = Category::all();
			$static = StaticContent::find($idStatic);
			return view("admin.staticcontent",compact('categories', 'static'));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
	public function editStaticContent($idStatic){
		if(Session::has('loggedadmin')){
			$static = StaticContent::find($idStatic);
			$static->title = Input::get('title');
			$static->content = Input::get('content');
			if($idStatic != 2){ //Bukan FAQ
				if($_FILES['image']['name']!=""){
					$target_dir = "images/contents/";
					$target_file = $target_dir . basename($static->featured_image);
					if(file_exists($target_file)){
						unlink($target_file);
					}
					$image = $_FILES["image"];
					$this->uploadFile($image, 'contents');
					$static->featured_image = $image['name'];
				}
			}
			$static->save();
			return Redirect::intended(URL::to('tk-admin/staticcontent/'.$idStatic));
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}

	private function uploadFile($image, $dir){
		if(Session::has('loggedadmin')){
			$target_dir = "images/".$dir."/";
			$target_file = $target_dir . basename($image["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			// Check if image file is a actual image or fake image
			if(isset($_POST["submit"])) {
			    $check = getimagesize($image["tmp_name"]);
			    if($check !== false) {
			        echo "File is an image - " . $check["mime"] . ".";
			        $uploadOk = 1;
			    } else {
			        echo "File is not an image.";
			        $uploadOk = 0;
			    }
			}
			// Check if file already exists
			if (file_exists($target_file)) {
			    echo "Sorry, file already exists.";
			    $uploadOk = 0;
			}
			// Check file size
			if ($image["size"] > 500000) {
			    echo "Sorry, your file is too large.";
			    $uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
			&& $imageFileType != "gif" ) {
			    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			    $uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
			    echo "Sorry, your file was not uploaded.";
			// if everything is ok, try to upload file
			} else {
			    if (Input::file('image')->move($target_dir, $image['name'])) {
			        echo "The file ". basename( $image["name"]). " has been uploaded.";
			    } else {
			        echo "Sorry, there was an error uploading your file.";
			    }
			}
		}
		else{
			return Redirect::intended(URL::to('/tk-admin/login'));
		}
	}
}
