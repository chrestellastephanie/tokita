<?php 
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Item;
use App\Gift;
use App\Customer;
use App\Coupon;
use App\Cart;
use App\Category;
use App\Subcategory;
use App\PriceFilter;
use App\RecentlyViewed;
use App\Favorite;
use App\Transaction;
use App\CartTransaction;
use App\StaticContent;
use App\ClaimedCoupon;
use App\ClaimedGift;
use App\ImageSlider;
use View;
use Auth;
use Input;
use App\User;
use DB;
use Mail;
use Session;
use Hash;
use URL;
use Uuid;
use DateTime;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SiteController extends Controller {

	//

	public function home(){
		// this week results
		$itemsDiscount = Item::whereRaw('discounted_item_price < item_price')->get()->sortByDesc('discounted_item_price')->take(8);
		foreach ($itemsDiscount as $key => $item) {
			$item->type = "discount";
		}
		$itemsPopular = Item::all()->sortByDesc('views')->take(8);
		foreach ($itemsPopular as $key => $item) {
			$item->type = "popular";
		}
		$itemsNew = Item::all()->sortByDesc('created_at')->take(8);
		foreach ($itemsNew as $key => $item) {
			$item->type = "new";
		}
		$items = Item::all();
		$gifts = Gift::all();
		$coupons = Coupon::all();
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
		$sliders = ImageSlider::whereRaw('type = ?',['slider'])->get();
		$maxN = ImageSlider::whereRaw('type = ?',['banner'])->count();
 $n1 = rand(0,$maxN-1);
 $n2 = rand(1,$maxN-1);
 $banners = array();
 $banns = ImageSlider::whereRaw('type = ?',['banner'])->get();
 $banners[] = $banns[$n1];
 $banners[] = $banns[$n2];
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
			foreach ($itemsDiscount as $key => $item) {
				$item->isFav = $this->itemExistInFav($loggedin->id, $item->id);
			}
			foreach ($itemsPopular as $key => $item) {
				$item->isFav = $this->itemExistInFav($loggedin->id, $item->id);
			}
			foreach ($itemsNew as $key => $item) {
				$item->isFav = $this->itemExistInFav($loggedin->id, $item->id);
			}
			$recent = RecentlyViewed::whereRaw('customer_id = ?', [$loggedin->id])->get()->sortByDesc('last_seen')->take(4);
			$itemsRecent = array();
			foreach($recent as $rec){
				$itemsRecent[] = $rec->item_detail();
			}
			foreach ($itemsRecent as $key => $item) {
				$item->isFav = $this->itemExistInFav($loggedin->id, $item->id);
				$item->type = "recent";
			}
		}
		if(Session::has('success_message')){
			$success_message = Session::get('success_message');
			Session::forget('success_message');
		}
		if(Session::has('error_message')){
			$error_message = Session::get('error_message');
			Session::forget('error_message');
		}
		return view('homePage', compact('count_cart_items','settings','sliders','banners','itemsDiscount','itemsPopular','itemsNew','itemsRecent','items', 'loggedin', 'gifts', 'coupons', 'success_message', 'error_message', 'categories'));
	}

	public function addView($itemId){
		$id = $itemId;
		if(Session::has('loggedin')){
			$customer = Session::get('loggedin');
			$exist = $this->isItemAlreadyViewedByCustomer($id, $customer->id);
			$created = date("Y-m-d H:i:s");
			$updated = $created;
			if(!$exist){
				RecentlyViewed::create(['customer_id' => $customer->id,
								  'item_id' => $id,
								  'last_seen' => $created,
								  'created_at' => $created,
								  'updated_at' => $updated]);
			}
			else{
				$last_seen = date("Y-m-d H:i:s");
				$exist->last_seen = $last_seen;
				$exist->save();
			}
		}
		$item = Item::find($id);
		$item->views = $item->views+1;
		$item->save();
	}

	private function isItemAlreadyViewedByCustomer($idItem, $idCustomer){
		$items = RecentlyViewed::whereRaw('item_id = ? and customer_id = ?',[$idItem, $idCustomer]);
		if($items->count() >0){
			return $items->first();
		}
		else{
			return false;
		}
	}

	public function login(){
		$email = Input::get("email");
		$password = Input::get("password");
		$users = $this->processLogin($email, $password);
		if($users){
			if(Hash::check($email, $users->verify_token)){
				Session::put('loggedin', $users);
				Session::put('success_message', "Anda berhasil masuk.");
			}
			else{
				Session::put('error_message', "Verifikasi diperlukan. Kunjungi email Anda untuk melakukan verifikasi.");
			}
			return Redirect::intended('/');
		}
		else{
			Session::put('error_message', "Email atau password salah.");
			return Redirect::intended('/');
		}
	}


	public function updateCustomer(){
		$id = Input::get("id_customer");
		$name = Input::get("name");
		$email = Input::get("email");
		$phone = Input::get("phone");
		$address = Input::get("address");
		$user = $this->processUpdateCustomer($id, $name, $email, $phone, $address);
		return Redirect::intended('/');
	}

	public function processUpdateCustomer($id, $name, $email, $phone, $address){
		$user = Customer::find($id);
		Session::forget('loggedin');
		$user->customer_name = $name;
		$user->customer_phone = $phone;
		$user->customer_address = $address;
		if($email == $user->customer_email){
			Session::put('loggedin', $user);
			Session::put('success_message', "Detil akun berhasil diganti.");
		}
		if($email != $user->customer_email){
			$user->customer_email = $email;
			$user->verify_token = '';
			$this->sendMail($name, $email);
			Session::put('error_message', "Anda telah signout. Silakan verifikasi email baru terlebih dahulu.");
		}
		$user->save();
		return $user;
	}

	public function processLogin($email, $password){
		$users = Customer::whereRaw('customer_email = ?', [$email])->first();
		if($users){
			if (Hash::check($password, $users->customer_password))
			{
			    return $users;
			}
		}
	}

	public function processRegister($name, $email, $password, $phone, $address){
		$created = date("Y-m-d H:i:s");
		$updated = $created;
		$this->sendMail($name, $email);
		Session::put('success_message', "Pendaftaran Berhasil. Silakan verifikasi email.");
		return Customer::create(['customer_name' => $name,
						  'customer_email' => $email,
						  'customer_password' => Hash::make($password),
						  'customer_phone' => $phone,
						  'customer_address' => $address,
						  'created_at' => $created,
						  'updated_at' => $updated]);
	}

	private function sendMail($name, $email){
		$token = Hash::make($email);
		Session::put('email', $email);
		$prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
		Mail::send('email_confirmation', ['name'=>$name, 'token' => $token, 'email' => $email, 'settings' => $settings], function($message)
		{
		    $message->to(Session::get('email'), 'Tokita')->subject('Konfirmasi E-mail');
		});
	}

	private function sendResetPassword($name, $email){
		$token = Hash::make($name);
		Session::put('email', $email);
		$prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
		Mail::send('forgot_password', ['name'=>$name, 'token' => $token, 'email' => $email, 'settings' => $settings], function($message)
		{
		    $message->to(Session::get('email'), 'Tokita')->subject('Reset Password');
		});
	}

	public function verifyToken(){
		$email = Input::get("user");
		$token = Input::get("verify");
		$user = Customer::whereRaw('customer_email = ?', [$email])->first();
		$user->verify_token = $token;
		$user->save();
		Session::put('success_message', "Email telah berhasil diverifikasi. Silakan login.");
		return Redirect::intended('/');
	}

	public function reset(){
		$email = Input::get("user");
		$token = Input::get("verify");
		$user = Customer::whereRaw('customer_email = ?', [$email])->first();
		if(Hash::check($user->customer_name, $token)){
			$user->customer_password = '';
			$user->save();
			Session::put('user_id', $user->id);
			return Redirect::intended('resetPassword');
		}
		else{
			Session::put('error_message', "Invalid token verifikasi");
			return Redirect::intended('/');
		}
	}

	public function sentInvoice($carts, $transaction, $coupon, $total, $settings){
		Session::put('email', $transaction['email']);
		Mail::send('invoice', ['address' => $transaction['address'], 'name' => $transaction['name'],'settings'=>$settings, 'transactions' => $carts, 'coupon' => $coupon, 'total' => $total], function($message)
		{
		    $message->to(Session::get('email'), 'Tokita')->subject('Nota Pembelian');
		    Session::forget('email');
		});
		$prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
		Session::put('email_admin', $settings['email']);
		Mail::send('invoice_for_admin', ['address' => $transaction['address'], 'name' => $transaction['name'],'settings'=>$settings, 'transactions' => $carts, 'coupon' => $coupon, 'total' => $total], function($message)
		{
		    $message->to(Session::get('email_admin'), 'Tokita')->subject('Transaksi Baru');
		    Session::forget('email_admin');
		});
	}

	public function logout(){
		Session::forget('loggedin');
		Session::put('success_message', "Anda telah signout");
		return Redirect::intended('/');
	}

	public function register(){
		$name = Input::get("name");
		$email = Input::get("email");
		$password = Input::get("password");
		$phone = Input::get("phone");
		$address = Input::get("address");
		$user = $this->processRegister($name, $email, $password, $phone, $address);
		return Redirect::intended('/');
	}

	public function checkEmail(){
		$email = Input::get("email");
		if(Input::get("id_customer") != null){
			$id = Input::get("id_customer");
			$users = Customer::whereRaw('customer_email = ? and id <> ?', [$email, $id])->count();
		}
		else{
			$users = Customer::whereRaw('customer_email = ?', [$email])->count();
		}
		if($users > 0){
			return 0;
		}
		else{
			return 1;
		}
	}

	public function showRegister(){
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		return view('account.register', compact('count_cart_items','settings','banners','categories'));
	}
	
	public function showEditAccount(){
		$gifts = Gift::all();
		$coupons = Coupon::all();
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
		}
		return view('account.edit-account', compact('count_cart_items','settings','banners','loggedin', 'coupons', 'gifts','categories'));
	}

	public function processApi(){
		$request = Input::get("request");
		if($request == 'ping'){
			$response['status_code'] = '200';
			$response['message'] = 'pong';
		}

		else if($request == 'login'){
			$email = Input::get("email");
			$password = Input::get("password");
			$users = $this->processLogin($email, $password);
			if($users){
				if(Hash::check($email, $users->verify_token)){
					$response['status_code'] = '200';
					$response['message'] = 'success, user logged in';
					$response['user'] =  $this->userToArray($users);
				}
				else{
					$response['status_code'] = '401';
					$response['message'] = 'Login failed. Please verify your email address';
				}
			}else{
				$response['status_code'] = '400';
				$response['message'] = 'email and password mismatch';
			}
		}

		else if($request == 'register'){
			$name = Input::get("name");
			$email = Input::get("email");
			$password = Input::get("password");
			$address = Input::get("address");
			$users = $this->processRegister($name, $email, $password, $phone, $address);
			if($users){
				$response['status_code'] = '201';
				$response['message'] = 'Success. Visit email for confirmation';
			}
		}

		else if($request == 'update'){
			$name = Input::get("id");
			$name = Input::get("name");
			$email = Input::get("email");
			$address = Input::get("address");
			$users = $this->processUpdateCustomer($id, $name, $email, $phone, $address);
			if($users){
				$response['status_code'] = '200';
				$response['message'] = 'success, user logged in';
				$response['user'] =  $this->userToArray($users);
			}
		}

		else if($request == 'change password'){
			$id = Input::get("user_id");
			$old = Input::get("old_password");
			$new = Input::get("password");
			$success = $this->processChangePassword($id, $old, $new);
			if($success){
				$response['status_code'] = '200';
				$response['message'] = 'success, password changes';
			}
			else{
				$response['status_code'] = '400';
				$response['message'] = 'Wrong password';	
			}
		}

		else if($request == 'reset password'){
			$email = Input::get("email");
			$users = Customer::whereRaw('customer_email = ?', [$email]);
			if($users->count() > 0){
				$user = $users->first();
				$this->sendResetPassword($user->customer_name, $user->customer_email);
				$response['status_code'] = '201';
				$response['message'] = 'success, email confirmation has been sent.';
			}
			else{
				$response['status_code'] = '400';
				$response['message'] = 'Wrong email address';
			}
		}

		else if($request == 'product'){
			$filter = Input::get("filter");
			if($filter == 'all'){
				$items = Item::all();
				$response['status_code'] = '200';
				$response['message'] = 'success.';
				$response['product'] = $this->itemsToArray($items);
			}
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function addToCart(){
		$referer = $_GET['ref'];
		$id_customer = Input::get("customer_id");
		$id_item = Input::get("item_id");
		$quantity = Input::get("quantity");
		$this->processAddToCart($id_customer, $id_item, $quantity);
		return Redirect::intended($referer);
	}

	public function deleteCart($idCart){
		$cart = Cart::find($idCart);
		$currentItem = Item::find($cart->item_id);
		$currentItem->stock = $currentItem->stock + $cart->quantity;
		$currentItem->save();
		Cart::destroy($idCart);
		return Redirect::intended(URL::to('/cart'));
	}

	public function deleteAllCart(){
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
			$carts = Cart::whereRaw('customer_id = ? and status = 0',[$loggedin->id])->get();
			foreach ($carts as $key => $cart) {
				$cart = Cart::find($cart->id);
				$currentItem = Item::find($cart->item_id);
				$currentItem->stock = $currentItem->stock + $cart->quantity;
				$currentItem->save();
				Cart::destroy($cart->id);
			}
			return Redirect::intended(URL::to('/cart'));
		}
	}

	public function editCart(){
		$customer = Input::get('customer_id');
		$carts = Cart::whereRaw('customer_id = ? and status = 0',[$customer])->get();
		foreach ($carts as $cart) {
			$inputCart = Input::get('qty'.$cart->id);
			$currentItem = Item::find($cart->item_id);
			$currentItem->stock = $currentItem->stock + $cart->quantity - $inputCart;
			$currentItem->save();
			$cart->quantity = $inputCart;
			$cart->save();
		}
		if(isset($_POST['edit'])){
			return Redirect::intended(URL::to('/cart'));
		}
		else if(isset($_POST['continue'])){
			return Redirect::intended(URL::to('/confirmation'));	
		}
	}

	public function processAddToCart($id_customer, $id_item, $quantity){
		$created = date("Y-m-d H:i:s");
		$updated = $created;
		$exist = $this->itemExistInCart($id_customer, $id_item);
		if(!$exist){
			Cart::create(['customer_id' => $id_customer,
							  'item_id' => $id_item,
							  'quantity' => $quantity,
							  'status' => 0,
							  'created_at' => $created,
							  'updated_at' => $updated]);
		}
		else{
			$exist->quantity = $exist->quantity + $quantity;
			$exist->save();
		}
		$currentItem = Item::find($id_item);
		$currentItem->stock = $currentItem->stock - $quantity;
		$currentItem->save();
		Session::put('success_message', "Produk berhasil ditambahkan ke keranjang.");
	}

	public function addToFav(){
		if(Session::has('loggedin')){
			$referer = $_GET['ref'];
			$loggedin = Session::get('loggedin');
			$id_customer = $loggedin->id;
			$id_item = Input::get("item_id");
			$this->processAddToFav($id_customer, $id_item);
		}
		return Redirect::intended($referer);
	}

	public function removeFav(){
		if(Session::has('loggedin')){
			$referer = $_GET['ref'];
			$loggedin = Session::get('loggedin');
			$id_customer = $loggedin->id;
			$id_item = Input::get("item_id");
			$this->processRemoveFav($id_customer, $id_item);
		}
		return Redirect::intended($referer);
	}

	public function processAddToFav($id_customer, $id_item){
		$created = date("Y-m-d H:i:s");
		$updated = $created;
		$exist = $this->itemExistInFav($id_customer, $id_item);
		if(!$exist){
			Favorite::create(['customer_id' => $id_customer,
							  'item_id' => $id_item,
							  'created_at' => $created,
							  'updated_at' => $updated]);
		}
		Session::put('success_message', "Produk berhasil ditambahkan ke favorit.");
	}

	public function processRemoveFav($id_customer, $id_item){
		$fav = Favorite::whereRaw('customer_id = ? and item_id = ?', [$id_customer, $id_item])->delete();
		Session::put('success_message', "Berhasil menghapus produk dari favorit.");
	}

	public function itemExistInCart($id_customer, $id_item){
		$item_cart = Cart::whereRaw('customer_id = ? and item_id = ? and status = 0', [$id_customer, $id_item]);
		if($item_cart->count() >0){
			return $item_cart->first();
		}
		else{
			return false;
		}
	}

	public function itemExistInFav($id_customer, $id_item){
		$item_fav = Favorite::whereRaw('customer_id = ? and item_id = ?', [$id_customer, $id_item]);
		if($item_fav->count() >0){
			return $item_fav->first();
		}
		else{
			return false;
		}
	}

	public function sendNewPassword(){
		$email = Input::get('email');
		$users = Customer::whereRaw('customer_email = ?', [$email]);
		if($users->count() > 0){
			$user = $users->first();
			$this->sendResetPassword($user->customer_name, $user->customer_email);
			Session::put('success_message', "Email berisi link untuk reset password telah dikirimkan.");
		}
		else{
			Session::put('error_message', "Email tidak terdaftar");
		}
		return Redirect::intended('/');
	}

	public function showProducts($idCategory = 0, $idSubcategory = 0){
		$highest = Item::all()->sortByDesc('discounted_item_price')->first()->discounted_item_price;
		$lowest = Item::all()->sortBy('discounted_item_price')->first()->discounted_item_price;
		$where = 'discounted_item_price > 0';
		if(isset($_GET['sort_by'])){
			$sort_by = $_GET['sort_by'];
		}else{
			$sort_by = "created_at";
		}
		if(isset($_GET['order'])){
			$order = $_GET['order'];
		}else{
			$order = "desc";
		}
		if($idCategory == 0){
			if($order == "desc"){
				$items = Item::all()->sortByDesc($sort_by);
			}
			else{
				$items = Item::all()->sortBy($sort_by);	
			}
		}
		else{
			$subcategories = Subcategory::whereRaw('category_id = ?', [$idCategory])->get();
			$current_cat = Category::find($idCategory);
			$where = $where.' and category_id = '. $idCategory;
			if($idSubcategory == 0){
				$idSubcategory = Subcategory::whereRaw('category_id = ?', [$idCategory])->first()->id;
			}
			$current_subcat = Subcategory::whereRaw('id = ?', [$idSubcategory])->first();
			$current_cat = Category::whereRaw('id = ?', [$current_subcat->category_id])->first();
			$subcategories = Subcategory::whereRaw('category_id = ?', [$current_cat->id])->get();
			$where = $where.' and subcategory_id = '. $idSubcategory;
			if(isset($_GET['bottom_range'])){
				$bottom_range = $_GET['bottom_range'];
				$where = $where.' and discounted_item_price >= '.$_GET['bottom_range'];
			}
			if(isset($_GET['top_range'])){
				$top_range = $_GET['top_range'];
				$where = $where.' and discounted_item_price <= '.$_GET['top_range'];
			}
			if($order == "desc"){
				$items = Item::whereRaw($where)->get()->sortByDesc($sort_by);
			}
			else{
				$items = Item::whereRaw($where)->get()->sortBy($sort_by);
			}
		}
		if(isset($loggedin)){
			foreach ($items as $key => $item) {
				$item->isFav = $this->itemExistInFav($loggedin->id, $item->id);
				$item->type = "product";
			}
		}
		$filters = PriceFilter::all();
		$maxN = ImageSlider::whereRaw('type = ?',['banner'])->count();
 $n1 = rand(0,$maxN-1);
 $n2 = rand(1,$maxN-1);
 $banners = array();
 $banns = ImageSlider::whereRaw('type = ?',['banner'])->get();
 $banners[] = $banns[$n1];
 $banners[] = $banns[$n2];
		$gifts = Gift::all();
		$coupons = Coupon::all();
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
		}
		if(Session::has('success_message')){
			$success_message = Session::get('success_message');
			Session::forget('success_message');
		}
		if(Session::has('error_message')){
			$error_message = Session::get('error_message');
			Session::forget('error_message');
		}
		return view('products.products', compact('error_message','success_message','count_cart_items','settings','banners', 'lowest','highest','items', 'loggedin', 'gifts', 'coupons', 'categories', 'subcategories', 'current_cat', 'filters','bottom_range','top_range', 'sort_by', 'order'));
	}

	public function show($event){
		$highest = Item::all()->sortByDesc('discounted_item_price')->first()->discounted_item_price;
		$lowest = Item::all()->sortBy('discounted_item_price')->first()->discounted_item_price;
		if($event == 'discount'){
			$where = 'discounted_item_price < item_price';
		}else if($event == 'new'){
			$itemsNew = Item::all()->sortByDesc('created_at')->first();
			$newest = new DateTime(date('Y-m-d', strtotime($itemsNew->created_at)));
	        date_modify($newest, '-1 days');
	        $last_week = date_format($newest, 'Y-m-d');
			$where = 'created_at >= '. $last_week;
		}
		else if($event == 'popular'){
			$itemsPopular = Item::all()->sortByDesc('views')->first();
			$popularst = $itemsPopular->views;
			$where = 'views >= '. ($popularst-10);
		}
		if(isset($_GET['sort_by'])){
			$sort_by = $_GET['sort_by'];
		}else{
			$sort_by = "created_at";
		}
		if(isset($_GET['order'])){
			$order = $_GET['order'];
		}else{
			$order = "desc";
		}
		if(isset($_GET['bottom_range'])){
			$bottom_range = $_GET['bottom_range'];
			$where = $where.' and discounted_item_price >= '.$_GET['bottom_range'];
		}
		if(isset($_GET['top_range'])){
			$top_range = $_GET['top_range'];
			$where = $where.' and discounted_item_price <= '.$_GET['top_range'];
		}
		if($order == "desc"){
			$items = Item::whereRaw($where)->get()->sortByDesc($sort_by);
		}
		else{
			$items = Item::whereRaw($where)->get()->sortBy($sort_by);
		}

		if(isset($loggedin)){
			foreach ($items as $key => $item) {
				$item->isFav = $this->itemExistInFav($loggedin->id, $item->id);
				$item->type = "product";
			}
		}
		$filters = PriceFilter::all();
		$gifts = Gift::all();
		$coupons = Coupon::all();
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
		}
		if(Session::has('success_message')){
			$success_message = Session::get('success_message');
			Session::forget('success_message');
		}
		if(Session::has('error_message')){
			$error_message = Session::get('error_message');
			Session::forget('error_message');
		}
		return view('products.products', compact('error_message','success_message','count_cart_items','settings','banners','lowest','highest','items', 'loggedin', 'gifts', 'coupons', 'categories', 'subcategories', 'current_cat', 'filters','bottom_range','top_range', 'sort_by', 'order'));
	}

	public function search($searchQuery){
		$highest = Item::all()->sortByDesc('discounted_item_price')->first()->discounted_item_price;
		$lowest = Item::all()->sortBy('discounted_item_price')->first()->discounted_item_price;
		$where = 'item_name like "%'. $searchQuery.'%"';
		if(isset($_GET['bottom_range'])){
			$where = $where.' and discounted_item_price >= '.$_GET['bottom_range'];
		}
		if(isset($_GET['top_range'])){
			$where = $where.' and discounted_item_price <= '.$_GET['top_range'];
		}
		$items = Item::whereRaw($where)->get();
		$filters = PriceFilter::all();
		$gifts = Gift::all();
		$coupons = Coupon::all();
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
		}
		if(isset($loggedin)){
			foreach ($items as $key => $item) {
				$item->isFav = $this->itemExistInFav($loggedin->id, $item->id);
				$item->type = "product";
			}
		}
		$query = $searchQuery;
		$params = "";
		if(Session::has('success_message')){
			$success_message = Session::get('success_message');
			Session::forget('success_message');
		}
		if(Session::has('error_message')){
			$error_message = Session::get('error_message');
			Session::forget('error_message');
		}
		return view('products.products', compact('error_message','success_message','count_cart_items','settings','banners','highest', 'lowest', 'query','params', 'items', 'loggedin', 'gifts', 'coupons', 'categories', 'subcategories', 'current_cat', 'filters'));
	}

	public function showResetPassword(){
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		Session::forget('loggedin');
		$user_id = Session::get('user_id');
		Session::forget('user_id');
		return view('account.reset-password', compact('count_cart_items','categories','settings','banners','user_id'));
	}

	public function processChangePassword(){
		if(Hash::check($old_password, $user->customer_password)){
			$user->customer_password = Hash::make($password);
			$user->save();
			return true;
		}
		else{
			return false;
		}
	}

	public function changePassword(){
		$id = Input::get('user_id');
		$password = Input::get('password');
		$user = Customer::find($id);
		if(Input::get('old_password') != null){
			$old_password = Input::get('old_password');
			if(Hash::check($old_password, $user->customer_password)){
				$user->customer_password = Hash::make($password);
				$user->save();	
				Session::put('success_message', "Password berhasil diganti.");
			}
			else{
				Session::put('error_message', "Password lama salah.");
			}
		}
		else{
			Session::forget('loggedin');
			$user->customer_password = Hash::make($password);
			$user->save();
			Session::put('success_message', "Password berhasil diganti.");
		}
		return Redirect::intended('/');
	}

	public function userToArray($user) {
	    return array(
	        "id" => $user->id,
	        "customer_name" => $user->customer_name,
	        "customer_email" => $user->customer_email,
	        "customer_address" => $user->customer_address
	    );
	}

	public function itemsToArray($items){
		$result = array();
		foreach ($items as $item) {
			$i = array(
				"id" => $item->id,
				"item_name" => $item->item_name,
				"item_price" => $item->item_price,
				"item_thumbnail" => "http://tokita.ap01.aws.af.cm/public/images/products/".$item->item_thumbnail,
				"discounted_item_price" => $item->discounted_item_price,
				"item_category" => $item->item_category
				);
			$result[] = $i;
		}
		return $result;
	}

	public function showCart(){
		$items = Item::all();
		$gifts = Gift::all();
		$coupons = Coupon::all();
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		$total = 0;
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
			$cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->get();
			foreach ($cart_items as $cart) {
				$total += $cart->quantity*$cart->item_detail()->discounted_item_price;
			}
		}
		return view('transaction.cart', compact('count_cart_items','settings','banners','loggedin', 'gifts', 'items', 'coupons', 'cart_items', 'total', 'categories'));
	}
	public function showConfirmation(){
		$items = Item::all();
		$gifts = Gift::all();
		$coupons = Coupon::all();
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		$total = 0;
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
			$claimedCoupons = ClaimedCoupon::whereRaw('customer_id = ? and status = 0',[$loggedin->id])->get();
			$cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->get();
			foreach ($cart_items as $cart) {
				$total += $cart->quantity*$cart->item_detail()->discounted_item_price;
			}
		}
		return view('transaction.confirmation', compact('count_cart_items','settings','banners','claimedCoupons','loggedin', 'gifts', 'items', 'coupons', 'cart_items', 'total', 'categories'));
	}
	public function showSummary(){
		$items = Item::all();
		$gifts = Gift::all();
		$coupons = Coupon::all();
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		$total = 0;
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
			$transaction = Session::get('transaction');
			$coupon = ClaimedCoupon::find($transaction['coupon']);
			$cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->get();
			foreach ($cart_items as $cart) {
				$total += $cart->quantity*$cart->item_detail()->discounted_item_price;
			}
			if($coupon){
				$total = $total - $coupon->coupon_details()->nominal;
			}
		}
		return view('transaction.summary', compact('count_cart_items','settings','banners','coupon','transaction','loggedin', 'gifts', 'items', 'coupons', 'cart_items', 'total', 'categories'));
	}
	public function showOrderSent(){
		if(Session::has('loggedin') && Session::has('transaction')){
			$items = Item::all();
			$gifts = Gift::all();
			$coupons = Coupon::all();
			$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
			$total = 0;
			if(Session::has('loggedin')){
				$loggedin = Session::get('loggedin');
				$transaction = Session::get('transaction');
				$trans = Transaction::create(['customer_id' => $loggedin->id,
											  'customer_name' => $transaction['name'],
											  'customer_phone' => $transaction['phone'],
											  'customer_email' => $transaction['email'],
											  'customer_address' => $transaction['address'],
											  'customer_request' => $transaction['request'],
											  'customer_payment' => $transaction['payment']]);
				foreach ($transaction['carts'] as $c) {
					$result = CartTransaction::create(['cart_id' => $c,
												   	   'transaction_id' => $trans->id]);
					$current_cart = Cart::find($c);
					$total = $total + ($current_cart->quantity * $current_cart->item_detail()->discounted_item_price);
					$current_cart->status = 1;
					$current_cart->save();
				}
				$carts = CartTransaction::whereRaw('transaction_id = ?',[$trans->id])->get();
				$coupon = ClaimedCoupon::find($transaction['coupon']);
				if($coupon){
					$coupon->status = $trans->id;
					$coupon->save();
					$total = $total - $coupon->coupon_details()->nominal;
				}
				$prefs = DB::table('settings')
	                     ->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))
	                     ->get();
				foreach ($prefs as $pref) {
					$settings[$pref->parameter] = $pref->value;
				}
				$customer = Customer::find($loggedin->id);
				$customer->point = $customer->point + (($total * $settings['poin_given'])/$settings['purchase_for_poin']);
				$customer->save();
				$this->sentInvoice($carts, $transaction, $coupon, $total, $settings);
				Session::forget('transaction');
			}
			return view('transaction.order-sent',compact('count_cart_items','settings','banners','categories', 'loggedin'));
		}
		else{
			return Redirect::intended('/');
		}
	}
	public function setSessionTransaction(){
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
			$cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->get();
			$transaction = Input::all();
			Session::put('transaction', $transaction);
			return Redirect::intended(URL::to('/summary'));
		}
		else{
			return Redirect::intended('/');
		}
	}
	public function showFavorit(){
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
			$favs = Favorite::whereRaw('customer_id = ?', [$loggedin->id])->get();
			$items = array();
			foreach ($favs as $fav) {
				$items[] = $fav->item_detail();
			}
			foreach ($items as $item) {
				$item->isFav = $this->itemExistInFav($loggedin->id, $item->id);
			}
		}
		return view('transaction.favorit',compact('count_cart_items','settings','banners','categories', 'favs', 'loggedin', 'items'));
	}
	public function showAbout(){
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		$about = StaticContent::find(1);
		$ceo = StaticContent::find(3);
		$cfo = StaticContent::find(4);
		return view('pages.about',compact('count_cart_items','settings','banners','categories', 'about', 'ceo', 'cfo'));
	}

	public function showFaq(){
		$categories = Category::all();
if(Session::has('loggedin')){
$loggedin = Session::get('loggedin');
$count_cart_items = Cart::whereRaw("customer_id = ? and status = 0",[$loggedin->id])->count();
}
 $prefs = DB::table('settings')->select(DB::raw('parameter, coalesce(value_int, value_string) as value'))->get();
 foreach ($prefs as $pref) {$settings[$pref->parameter] = $pref->value;}
$banners = ImageSlider::whereRaw('type = ?', ['banner'])->get();
		$faq = StaticContent::find(2);
		return view('pages.faq',compact('count_cart_items','settings','banners','categories', 'faq'));
	}

	public function getCoupon($couponId){
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
			$coupon = Coupon::find($couponId);
			if($loggedin->point > $coupon->point){
				ClaimedCoupon::create(['customer_id' => $loggedin->id,
								  'coupon_id' => $coupon->id,
								  'status' => 0,
								  'identifier' => Uuid::generate()]);
				$customer = Customer::find($loggedin->id);
				$customer->point = $customer->point - $coupon->point;
				$customer->save();
			}
			else{
				Session::put('error_message', 'Poin tidak mencukupi');
			}
		}
		return Redirect::intended('/');
	}

	public function getGift($giftId){
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
			$gift = Gift::find($giftId);
			if($loggedin->point > $gift->point){
				ClaimedGift::create(['customer_id' => $loggedin->id,
								  'gift_id' => $gift->id,
								  'status' => 0,
								  'identifier' => Uuid::generate()]);
				$customer = Customer::find($loggedin->id);
				$customer->point = $customer->point - $gift->point;
				$customer->save();
			}
			else{
				Session::put('error_message', 'Poin tidak mencukupi');
			}
		}
		return Redirect::intended('/');
	}

	public function invoice(){
		return view('invoice');
	}
}
