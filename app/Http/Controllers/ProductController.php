<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Input;
use URL;

use App\Item;

use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$name = Input::get("name");
		$barcode = Input::get("barcode");
		$price = Input::get("price");
		$discounted = Input::get("discounted_price");
		$description = Input::get("description");
		$subcategory = Input::get("subcategory");
		$category = Input::get("category");
		$stok = Input::get("stock");
		$created = date("Y-m-d H:i:s");
		$updated = $created;
		$image = $_FILES["image"];
		$this->uploadFile($image);
		Item::create(['item_name' => $name,
					  'barcode' => $barcode,
					  'item_price' => $price,
					  'discounted_item_price' => $discounted,
					  'item_thumbnail' => $image['name'],
					  'description' => $description,
					  'category_id' => $category,
					  'subcategory_id' => $subcategory,
					  'stock' => $stok,
					  'views' => 0,
					  'created_at' => $created,
					  'updated_at' => $updated]);
		return Redirect::intended(URL::to('/tk-admin/product/'.$category));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	private function uploadFile($image){
		$target_dir = "images/products/";
		$target_file = $target_dir . basename($image["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($image["tmp_name"]);
		    if($check !== false) {
		        echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $uploadOk = 0;
		    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    echo "Sorry, file already exists.";
		    $uploadOk = 0;
		}
		// Check file size
		if ($image["size"] > 500000) {
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
		    if (Input::file('image')->move($target_dir, $image['name'])) {
		        echo "The file ". basename( $image["name"]). " has been uploaded.";
		    } else {
		        echo "Sorry, there was an error uploading your file.";
		    }
		}
	}

	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$product = Item::find($id);
		$product->item_name = Input::get('name');
		$product->barcode = Input::get('barcode');
		$product->item_price = Input::get('price');
		$product->discounted_item_price = Input::get('discounted_price');
		$product->stock = Input::get('stock');
		$product->category_id = Input::get('category');
		$product->subcategory_id = Input::get('subcategory');
		if($_FILES['image']['name']!=""){
			$target_dir = "images/products/";
			$target_file = $target_dir . basename($product->item_thumbnail);
			if(file_exists($target_file)){
				unlink($target_file);
			}
			$image = $_FILES["image"];
			$this->uploadFile($image);
			$product->item_thumbnail = $image['name'];
		}
		$product->save();
		return Redirect::intended(URL::to('/tk-admin/product/'.$product->category_id));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$product = Item::find($id);
		Item::destroy($id);
		return Redirect::intended(URL::to('/tk-admin/product/'.$product->category_id));
	}

}
