<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimedGift extends Model {

	protected $table = 'claimed_gifts';

	protected $fillable = [
		'customer_id',
		'gift_id',
		'identifier',
		'status'
	];

	public function gift_details(){
		return Gift::whereRaw("id = ?",[$this->gift_id])->first();
	}

	public function customer_details(){
		return Customer::whereRaw("id = ?",[$this->customer_id])->first();
	}

}
