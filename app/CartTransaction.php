<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CartTransaction extends Model {

	protected $table = 'cart_transactions';

	protected $fillable = [
		'transaction_id',
		'cart_id'
	];

	public function cart_detail(){
		return Cart::find($this->cart_id);
	}

	public function transaction_detail(){
		return Transaction::find($this->transaction_id);
	}

}
