<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RecentlyViewed extends Model {

	protected $table = 'recently_vieweds';

	protected $fillable = [
		'customer_id',
		'item_id',
		'last_seen'
	];

	public function item_detail(){
		return Item::whereRaw("id = ?",[$this->item_id])->first();
	}

	public function customer_detail(){
		return Customer::whereRaw("id = ?", [$this->customer_id])->first();
	}

}
