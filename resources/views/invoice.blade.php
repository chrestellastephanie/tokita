<!DOCTYPE html><html lang="en" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-family: 'Open Sans', sans-serif;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 62.5%;-webkit-tap-highlight-color: rgba(0,0,0,0);position: relative;min-height: 100%;">
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
<head style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<title style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">Tokita</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<link href="http://projects.dextrovert.com/tokita/public/css/bootstrap.min.css" rel="stylesheet" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<link href="http://projects.dextrovert.com/tokita/public/css/style.css" rel="stylesheet" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<script src="http://projects.dextrovert.com/tokita/public/js/jquery.min.js" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"></script>
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"></script>
	<script src="http://projects.dextrovert.com/tokita/public/js/bootstrap.min.js" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"></script> 
</head>

<body style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin: 0;font-family: &quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size: 14px;line-height: 1.428571429;color: #333;background-color: #fff;">
<div class="container" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
	<div class="invoice-header" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
		<div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
			<div class="col-sm-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;width: 100%;">
				<div class="pull-left" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;float: left!important;">
					<a style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background: transparent;color: #393939;text-decoration: none !important;"><img src="http://projects.dextrovert.com/tokita/public/images/logo/tagline_navbar.png" alt="" class="display-block " style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;display: block;width: 200px;max-width: 100%!important;"></a>
					<a style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background: transparent;color: #393939;text-decoration: none !important;"><img src="http://projects.dextrovert.com/tokita/public/images/logo/logo.png" alt="" class="display-block invoice-logo" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;display: block;margin-top: 5px;width: 200px;max-width: 100%!important;"></a>
				</div>
				<div class="pull-right" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;float: right!important;">
					<div class="invoice-menu" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-top: 30px;">
						<ul style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-top: 0;margin-bottom: 10px;list-style: none;">
							<li style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;list-style: none;display: inline;margin-left: 30px;color: #CC1412;text-decoration: underline;">Lanjutkan Belanja</li>
							<li style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;list-style: none;display: inline;margin-left: 30px;color: #CC1412;text-decoration: underline;">FAQ</li>
							<li style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;list-style: none;display: inline;margin-left: 30px;color: #CC1412;text-decoration: underline;">Tentang Kami</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="invoice-body" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-top: 45px;display:inline-block;">
		<div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: 0px;">
			<div class="col-sm-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;width: 100%;">	
				Dear Bapak/Ibu,
				<br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"><br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
				Salah satu sahabat Tokita sedang dalam perjalanan mengirimkan pesanan Anda, yaitu: <br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"><br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
			</div>
		</div>
		<div class="row margin-left" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: 30px;">
			<div class="col-sm-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;width: 100%;">
				<div class="invoice-list" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border-bottom: solid black 1px;padding-bottom: 5px;margin-bottom: 5px;overflow: hidden;">
					<div class="row invoice-table-heading" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: 0px;font-style: italic;color: #999999;margin-bottom: 5px;">
						<div class="col-sm-8" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 66.66666666666666%;">
							Nama produk
						</div>
						<div class="col-sm-1" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 8.333333333333332%;">
							Qty
						</div>
						<div class="col-sm-3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 25%;">
							Subtotal
						</div>
					</div>
					@foreach($transactions as $transaction)
					<div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: 0px;">
						<div class="col-sm-8" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 66.66666666666666%;">
							{{$transaction->cart_detail()->item_detail()->item_name}}
						</div>
						<div class="col-sm-1" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 8.333333333333332%;">
							{{$transaction->cart_detail()->quantity}}
						</div>
						<div class="col-sm-3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 25%;">
							Rp {{$transaction->cart_detail()->item_detail()->discounted_item_price * $transaction->cart_detail()->quantity}}
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@if($coupon)
		<div class="row margin-left" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: 30px;">
			<div class="col-sm-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;width: 100%;">
				<div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
					<div class="col-sm-9" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 75%;">
						<b style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-weight: bold;">Kupon</b>
					</div>
					<div class="col-sm-3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 25%;">
						<b style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-weight: bold;">Rp {{$coupon->coupon_details()->nominal}}</b>
					</div>
				</div>
			</div>			
		</div>
		@endif
		<div class="row margin-left" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: 30px;">
			<div class="col-sm-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;width: 100%;">
				<div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
					<div class="col-sm-1 col-sm-offset-8" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 8.333333333333332%;margin-left: 66.66666666666666%;">
						<b style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-weight: bold;">Total</b>
					</div>
					<div class="col-sm-3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 25%;">
						<b style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-weight: bold;">Rp {{$total}}</b>
					</div>
				</div>
			</div>			
		</div>
		<br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
		<div style="display:inline-block; margin-top: 30px;">
			Silakan siapkan uang tunai untuk pembayaran <br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"><br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
			Untuk bantuan lebih lanjut, silakan hubungi 022 8778875 / 082231845323 atau email Customer Service kami di admin@tokita.com
		</div>
	</div>
	<div class="invoice-footer" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-top: 30px;padding-top: 45px;padding-bottom: 45px;border-bottom: solid #FCB812 3px;-webkit-print-color-adjust: exact;background-color: #E6E6E6 !important; overflow: hidden;">
		<div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
			<div class="col-sm-3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 25%;">
				<div class="invoice-footer-logo" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;text-align: center;">
					<img src="http://projects.dextrovert.com/tokita/public/images/logo/logo_min_nobg.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;position: relative;margin-left: auto;margin-right: auto;margin-top: 30px;max-width: 100%!important;">
				</div>
			</div>
			<div class="col-sm-4 footerContent" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 33.33333333333333%;color: #999999;padding-top: 5px;padding-bottom: 5px;">
				Villa Bukit Mas Blok Baru no. 4 <br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
				Bojong Koneng, Cikutra, Bandung <br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
				022 87788751 / 082231845323
			</div>
			<div class="col-sm-5" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 41.66666666666667%;">
				<div class="pull-right" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;float: right!important;">
					<div class="invoice-sosmed" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-top: 15px;margin-right: 45px;">
						<a href="{{$settings['facebook']}}"><img src="http://projects.dextrovert.com/tokita/public/images/icon/facebook24.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;"></a>
						<a href="{{$settings['twitter']}}"><img src="http://projects.dextrovert.com/tokita/public/images/icon/twitter-button.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;"></a>
						<a href="{{$settings['google']}}"><img src="http://projects.dextrovert.com/tokita/public/images/icon/google2.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;"></a>
						<a href="{{$settings['instagram']}}"><img src="http://projects.dextrovert.com/tokita/public/images/icon/youtube18.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

</html>