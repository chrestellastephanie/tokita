@include('admin.navbar')

<div class="contentpanel">
	<div class="pull-left breadcrumb_admin clear_both">
		<div class="pull-left page_title theme_color">
			<h1>Coupons</h1>
			<h2 class="">Subtitle goes here...</h2>
		</div>
	</div>
	<div class="container clear_both padding_fix">
		<div class="main-content">
			<div class="page-content">
				<div class="row">
					<div class="col-md-12">
						<div class="block-web">
							<div class="header">
								<h3 class="content-header">
									Semua Kupon
								</h3>
							</div>
							<div class="porlets-content">
								<div class="adv-table editable-table">
									<div class="clearfix">
										<div class="btn-group">
											<button href="#" data-toggle="modal" data-target="#addCoupon" id="editable-sample_new" class="btn btn-primary btn-addCoupon">
												Add New <i class="fa fa-plus"></i>
											</button>
										</div>
									</div>
									<div class="margin-top-10"></div>
									<table class="table table-striped table-hover table-bordered" id="editable-sample">
										<thead>
											<tr>
												<th>No</th>
												<th>Nominal Kupon</th>
												<th>Gambar</th>
												<th>Poin</th>
												<th>Edit</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>
											@foreach($coupons as $key => $coupon)
											<tr>
												<td>{{$key+1}}</td>
												<td>{{$coupon->nominal}}</td>
												@if($coupon->thumbnail == "")
												<td class="center"><img class="image-thumbnail-product" style="max-width: 20%;" src="http://placehold.it/200x200?text=No+image"/></td>
												@else
												<td class="center"><img class="image-thumbnail-product" style="max-width: 20%;" src="{{asset('images/coupons/'.$coupon->thumbnail)}}"/></td>
												@endif
												<td>{{$coupon->point}}</td>
												<td><a href="#" data-toggle="modal" data-target="#addCoupon" data-nominal="{{$coupon->nominal}}" data-id="{{$coupon->id}}" data-point="{{$coupon->point}}" class="coupon-edit">Edit</a></td>
												<td><a href="{{URL::to('/tk-admin/coupon/delete/'.$coupon->id)}}">Delete</a></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="addCoupon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal row-border" action="" id="form-coupon" enctype="multipart/form-data" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel2">Tambah Kupon Baru</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Nominal Kupon</label>
							</div>
							<div class="col-sm-8">
								<input type="text" name="nominal" id="coupon-nominal" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Gambar</label>
							</div>
							<div class="col-sm-8">
								<input id="coupon-image" type="file" name="image">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Poin</label>
							</div>
							<div class="col-sm-8">
								<input type="text" name="point" id="coupon-point" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Tambahkan</button>
				</div>
			</div>
		</form>
	</div>
</div>


@include('admin.footer')