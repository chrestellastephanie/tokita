@include('admin.navbar')
  <div class="contentpanel">
      <!--\\\\\\\ contentpanel start\\\\\\-->
      <div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1>Products</h1>
          <h2 class="">Subtitle goes here...</h2>
        </div>
      </div>
      <div class="container clear_both padding_fix">

           <div id="main-content">
        <div class="page-content">
           <div class="row">
            <div class="col-md-12">
              <div class="block-web">
                <div class="header">
                  <!-- <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div> -->
                  <h3 class="content-header">{{$current_category->category_name}}</h3>
                </div>
              <div class="porlets-content">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                              <button href"#" data-toggle="modal" data-target="#myModal" id="editable-sample_new" class="btn btn-primary btn-addProduct">
                                  Add New <i class="fa fa-plus"></i>
                              </button>
                        </div>
                        <!-- <div class="btn-group pull-right">
                            <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#">Print</a></li>
                                <li><a href="#">Save as PDF</a></li>
                                <li><a href="#">Export to Excel</a></li>
                            </ul>
                        </div> -->
                    </div>
                    <div class="margin-top-10"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Harga</th>
                            <th>Harga Diskon</th>
                            <th>Gambar</th>
                            <th>Deskripsi</th>
                            <th>Sub-kategori</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                        <tr class="">
                            <td>{{$item->item_name}}</td>
                            <td>{{$item->item_price}}</td>
                            <td>{{$item->discounted_item_price}}</td>
                            @if($item->item_thumbnail == "")
                            <td class="center"><img class="image-thumbnail-product" src="http://placehold.it/200x200?text=No+image"/></td>
                            @else
                            <td class="center"><img class="image-thumbnail-product" src="{{asset('images/products/'.$item->item_thumbnail)}}"/></td>
                            @endif
                            <td>{{$item->description}}</td>
                            <td>{{$item->subcategory()->subcategory_name}}</td>
                            <td><a class="product-edit" data-toggle="modal"
                                                data-target="#myModal"
                                                data-id="{{$item->id}}"
                                                data-name="{{$item->item_name}}"
                                                data-barcode="{{$item->barcode}}"
                                                data-description="{{$item->description}}"
                                                data-price="{{$item->item_price}}"
                                                data-discounted="{{$item->discounted_item_price}}"
                                                data-image="{{$item->item_thumbnail}}"
                                                data-stock="{{$item->stock}}"
                                                data-category="{{$item->category()->id}}"
                                                data-subcategory="{{$item->subcategory()->id}}">Edit</a></td>
                            <td><a class="product-delete" href="{{URL::to('/tk-admin/product/delete/'.$item->id)}}">Delete</a></td>
                        </tr>
                        @endforeach
                        
                        </tbody>
                    </table>
                </div>
              </div><!--/porlets-content-->  
            </div><!--/block-web--> 
            </div><!--/col-md-12--> 
          </div><!--/row-->
        </div><!--/page-content end--> 
      </div><!--/main-content end--> 
      </div>
      <!--\\\\\\\ container  end \\\\\\-->
    </div>
    <!--\\\\\\\ content panel end \\\\\\-->
  </div>
  <!--\\\\\\\ inner end\\\\\\-->
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Tambah Produk Baru</h4>
      </div>
      <div class="modal-body">
        <form action="{{URL::to('/tk-admin/product/save')}}" class="form-horizontal row-border" method="post" id="form-product" enctype="multipart/form-data">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Nama Produk</label>
                  <div class="col-sm-9">
                    <input id="product-name" type="text" name="name" class="form-control">
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Barcode</label>
                  <div class="col-sm-9">
                    <input id="product-barcode" type="text" name="barcode" class="form-control">
                  </div>
                </div><!--/form-group--> 
                <div class="form-group">
                  <label class="col-sm-3 control-label">Harga Produk</label>
                  <div class="col-sm-9">
                    <input id="product-price" type="text" name="price" class="form-control">
                  </div>
                </div><!--/form-group-->
                <div class="form-group">
                  <label class="col-sm-3 control-label">Harga Mendapat Diskon</label>
                  <div class="col-sm-9">
                    <input id="product-discount" type="checkbox" class="form-control">
                  </div>
                </div><!--/form-group-->
                <div class="form-group">
                  <label class="col-sm-3 control-label">Harga Diskon</label>
                  <div class="col-sm-9">
                    <input id="product-discounted" type="text" name="discounted_price" class="form-control" readonly>
                  </div>
                </div><!--/form-group-->
                <div class="form-group">
                  <label class="col-sm-3 control-label">Gambar</label>
                  <div class="col-sm-9">
                    <!-- <span class="btn btn-success fileinput-button"> <i class="glyphicon glyphicon-plus"></i> <span>Add files...</span> -->
                    <input id="product-image" type="file" name="image">
                  </div>
                </div><!--/form-group-->
                <div class="form-group">
                  <label class="col-sm-3 control-label">Deskripsi</label>
                  <div class="col-sm-9">
                    <textarea id="product-description" class="form-control" name="description"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Kategori</label>
                  <div class="col-sm-9">
                    <select class="form-control" id="product-category" name="category">
                    @foreach($categories as $category)
                      <option value="{{$category->id}}"
                      @if($current_category->id == $category->id)
                        selected
                      @endif
                      >{{$category->category_name}}</option>
                    @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Subkategori</label>
                  <div class="col-sm-9">
                    <select class="form-control" id="product-subcategory" name="subcategory">
                    @foreach($subcategories as $key => $subcategory)
                      <option value="{{$subcategory->id}}"
                        @if($key == 0)
                          selected
                        @endif
                      >{{$subcategory->subcategory_name}}</option>
                    @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" name="stock">Stok</label>
                  <div class="col-sm-9">
                    <input id="product-stock" name="stock" type="text" class="form-control">
                  </div>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
              
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambahkan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  
</script>
@include('admin.footer')