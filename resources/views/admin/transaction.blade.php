@include('admin.navbar')

<div class="contentpanel">
	<div class="pull-left breadcrumb_admin clear_both">
		<div class="pull-left page_title theme_color">
			<h1>Transactions</h1>
			<h2 class="">Subtitle goes here...</h2>
		</div>
	</div>
	<div class="container clear_both padding_fix">
		<div class="main-content">
			<div class="page-content">
				<div class="row">
					<div class="col-md-12">
						<div class="block-web">
							<div class="header">
								<h3 class="content-header">
									Daftar Transaksi
								</h3>
							</div>
							<div class="porlets-content">
								<div class="adv-table editable-table">
									<!-- <div class="clearfix">
										<div class="btn-group">
											<button href="#" data-toggle="modal" data-target="#addSubCategory" id="editable-sample_new" class="btn btn-primary">
												Add New <i class="fa fa-plus"></i>
											</button>
										</div>
									</div> -->
									<div class="margin-top-10"></div>
									<table class="table table-striped table-hover table-bordered" id="editable-sample">
										<thead>
											<tr>
												<th>Lihat Transaksi</th>
												<th>Tanggal</th>
												<th>Nama Customer</th>
												<th>Nominal Pembayaran</th>
												<th>Telepon</th>
												<th>Alamat</th>
											</tr>
										</thead>
										<tbody>
											@foreach($transactions as $trans)
											<tr>
												<td><button onclick="showCart({{$trans->transaction_detail()->id}})">lihat transaksi</button></td>
												<td>{{date('j M Y', strtotime($trans->transaction_detail()->created_at))}}</td>
												<td>{{$trans->transaction_detail()->customer_name}}</td>
												<td>Rp {{number_format($trans->transaction_detail()->customer_payment, 0, ",", ".")}}</td>
												<td>{{$trans->transaction_detail()->customer_phone}}</td>
												<td>{{$trans->transaction_detail()->customer_address}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function showCart(i){
		window.location = "{{URL::to('/tk-admin/cart/')}}"+'/'+i;
	}
</script>
<!-- modal -->
<!-- <div class="modal fade" id="addSubCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal row-border" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Tambah Sub Kategori Baru</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Nama Sub Kategori</label>
							</div>
							<div class="col-sm-8">
								<input type="text" name="" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Tambahkan</button>
				</div>
			</div>
		</form>
	</div>
</div> -->

@include('admin.footer')