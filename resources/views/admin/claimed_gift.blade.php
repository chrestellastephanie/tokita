@include('admin.navbar')

<div class="contentpanel">
	<div class="pull-left breadcrumb_admin clear_both">
		<div class="pull-left page_title theme_color">
			<h1>Hadiah</h1>
		</div>
	</div>
	<div class="container clear_both padding_fix">
		<div class="main-content">
			<div class="page-content">
				<div class="row">
					<div class="col-md-12">
						<div class="block-web">
							<div class="header">
								<h3 class="content-header">
									Pengantaran Hadiah
								</h3>
							</div>
							<div class="porlets-content">
								<div class="adv-table editable-table">
									<!-- <div class="clearfix">
										<div class="btn-group">
											<button href="#" data-toggle="modal" data-target="#addSubCategory" id="editable-sample_new" class="btn btn-primary">
												Add New <i class="fa fa-plus"></i>
											</button>
										</div>
									</div> -->
									<div class="margin-top-10"></div>
									<table class="table table-striped table-hover table-bordered" id="editable-sample">
										<thead>
											<tr>
												<th>Nama Customer</th>
												<th>Jenis Hadiah</th>
												<th>Tanggal Pengambilan</th>
												<th>Status Pengantaran</th>
											</tr>
										</thead>
										<tbody>
											@foreach($claimedGifts as $gift)
											<tr>
												<td>{{$gift->customer_details()->customer_name}}</td>
												<td>{{$gift->gift_details()->name}}</td>
												<td>{{date('j M Y', strtotime($gift->created_at))}}</td>
												<td><button
												@if($gift->status == 0)
													class="btn btn-danger" onclick="changeStatus({{$gift->id}})" >belum diantar</button></td>
												@else
													class="btn btn-success" onclick="changeStatus({{$gift->id}})" >sudah diantar</button></td>
												@endif
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function changeStatus(i){
		window.location = "{{URL::to('/tk-admin/claimedgift/changeStatus')}}"+'/'+i;
	}
</script>
<!-- modal -->
<!-- <div class="modal fade" id="addSubCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal row-border" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Tambah Sub Kategori Baru</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Nama Sub Kategori</label>
							</div>
							<div class="col-sm-8">
								<input type="text" name="" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Tambahkan</button>
				</div>
			</div>
		</form>
	</div>
</div> -->

@include('admin.footer')