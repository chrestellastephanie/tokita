
<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="UTF-8" />
<title>Products</title>
<base href="http://demo.opencart.com/admin/" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">


<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<!--
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" />
//-->


<link href="view/javascript/bootstrap/less/bootstrap.less" rel="stylesheet/less" />
<script src="view/javascript/bootstrap/less-1.7.4.min.js"></script>

<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link href="view/javascript/summernote/summernote.css" rel="stylesheet">
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<script src="view/javascript/jquery/datetimepicker/moment.min.js" type="text/javascript"></script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />

<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />


<script src="view/javascript/common.js" type="text/javascript"></script>
</head>
<body>
<div id="container">
<header id="header" class="navbar navbar-static-top">
  <div class="navbar-header">
        <a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>
        <a href="http://demo.opencart.com/admin/index.php?route=common/dashboard&amp;token=6e21ba49cf5a928c42a3676e618a3ddb" class="navbar-brand"><img src="view/image/logo.png" alt="OpenCart" title="OpenCart" /></a></div>
    <ul class="nav pull-right">
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><span class="label label-danger pull-left">719</span> <i class="fa fa-bell fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right alerts-dropdown">
        <li class="dropdown-header">Orders</li>
        <li><a href="http://demo.opencart.com/admin/index.php?route=sale/order&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;filter_order_status=2" style="display: block; overflow: auto;">Pending <span class="label label-warning pull-right">0</span></a></li>
        <li><a href="http://demo.opencart.com/admin/index.php?route=sale/order&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;filter_order_status=5">Completed <span class="label label-success pull-right">0</span></a></li>
        <li><a href="http://demo.opencart.com/admin/index.php?route=sale/return&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Returns <span class="label label-danger pull-right">57</span></a></li>
        <li class="divider"></li>
        <li class="dropdown-header">Customers</li>
        <li><a href="http://demo.opencart.com/admin/index.php?route=report/customer_online&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Customers Online <span class="label label-success pull-right">0</span></a></li>
        <li><a href="http://demo.opencart.com/admin/index.php?route=sale/customer&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;filter_approved=0">Pending approval <span class="label label-danger pull-right">0</span></a></li>
        <li class="divider"></li>
        <li class="dropdown-header">Products</li>
        <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/product&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;filter_quantity=0">Out of stock <span class="label label-danger pull-right">16</a></li>
        <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/review&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;filter_status=0">Reviews <span class="label label-danger pull-right">646</span></a></li>
        <li class="divider"></li>
        <li class="dropdown-header">Affiliates</li>
        <li><a href="http://demo.opencart.com/admin/index.php?route=marketing/affiliate&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;filter_approved=1">Pending approval <span class="label label-danger pull-right">0</span></a></li>
      </ul>
    </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-life-ring fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right">
        <li class="dropdown-header">Stores <i class="fa fa-shopping-cart"></i></li>
                <li><a href="http://demo.opencart.com/" target="_blank">Your Store</a></li>
                <li class="divider"></li>
        <li class="dropdown-header">Help <i class="fa fa-life-ring"></i></li>
        <li><a href="http://www.opencart.com" target="_blank">Homepage</a></li>
        <li><a href="http://docs.opencart.com" target="_blank">Documentation</a></li>
        <li><a href="http://forum.opencart.com" target="_blank">Support Forum</a></li>
      </ul>
    </li>
    <li><a href="http://demo.opencart.com/admin/index.php?route=common/logout&amp;token=6e21ba49cf5a928c42a3676e618a3ddb"><span class="hidden-xs hidden-sm hidden-md">Logout</span> <i class="fa fa-sign-out fa-lg"></i></a></li>
  </ul>
  </header>
<nav id="column-left"><div id="profile">
  <div><a class="dropdown-toggle" data-toggle="dropdown"><img src="http://demo.opencart.com/image/cache/no_image-45x45.png" alt="demo demo" title="demo"class="img-circle" /></a></div>
  <div>
    <h4>demo demo</h4>
    <small>Demonstration</small></div>
</div>
<ul id="menu">
  <li id="dashboard"><a href="http://demo.opencart.com/admin/index.php?route=common/dashboard&amp;token=6e21ba49cf5a928c42a3676e618a3ddb"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a></li>
  <li id="catalog"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span>Catalog</span></a>
    <ul>
      <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/category&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Categories</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/product&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Products</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/recurring&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Recurring Profiles</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/filter&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Filters</a></li>
      <li><a class="parent">Attributes</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/attribute&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Attributes</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/attribute_group&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Attribute Groups</a></li>
        </ul>
      </li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/option&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Options</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/manufacturer&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Manufacturers</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/download&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Downloads</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/review&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Reviews</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/information&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Information</a></li>
    </ul>
  </li>
  <li id="extension"><a class="parent"><i class="fa fa-puzzle-piece fa-fw"></i> <span>Extensions</span></a>
    <ul>
      <li><a href="http://demo.opencart.com/admin/index.php?route=extension/installer&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Extension Installer</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=extension/modification&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Modifications</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=extension/module&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Modules</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=extension/shipping&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Shipping</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=extension/payment&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Payments</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=extension/total&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Order Totals</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=extension/feed&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Feeds</a></li>
          </ul>
  </li>
  <li id="sale"><a class="parent"><i class="fa fa-shopping-cart fa-fw"></i> <span>Sales</span></a>
    <ul>
      <li><a href="http://demo.opencart.com/admin/index.php?route=sale/order&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Orders</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=sale/recurring&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Recurring Orders</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=sale/return&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Returns</a></li>
      <li><a class="parent">Customers</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=sale/customer&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Customers</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=sale/customer_group&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Customer Groups</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=sale/custom_field&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Custom Fields</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=sale/customer_ban_ip&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Banned IP</a></li>
        </ul>
      </li>
      <li><a class="parent">Gift Vouchers</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=sale/voucher&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Gift Vouchers</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=sale/voucher_theme&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Voucher Themes</a></li>
        </ul>
      </li>
      <li><a class="parent">PayPal</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=payment/pp_express/search&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Search</a></li>
        </ul>
      </li>
    </ul>
  </li>
  <li><a class="parent"><i class="fa fa-share-alt fa-fw"></i> <span>Marketing</span></a>
    <ul>
      <li><a href="http://demo.opencart.com/admin/index.php?route=marketing/marketing&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Marketing</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=marketing/affiliate&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Affiliates</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=marketing/coupon&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Coupons</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=marketing/contact&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Mail</a></li>
    </ul>
  </li>
  <li id="system"><a class="parent"><i class="fa fa-cog fa-fw"></i> <span>System</span></a>
    <ul>
      <li><a href="http://demo.opencart.com/admin/index.php?route=setting/store&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Settings</a></li>
      <li><a class="parent">Design</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=design/layout&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Layouts</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=design/banner&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Banners</a></li>
        </ul>
      </li>
      <li><a class="parent">Users</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=user/user&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Users</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=user/user_permission&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">User Groups</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=user/api&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">API</a></li>
        </ul>
      </li>
      <li><a class="parent">Localisation</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/location&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Store Location</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/language&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Languages</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/currency&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Currencies</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/stock_status&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Stock Statuses</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/order_status&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Order Statuses</a></li>
          <li><a class="parent">Returns</a>
            <ul>
              <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/return_status&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Return Statuses</a></li>
              <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/return_action&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Return Actions</a></li>
              <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/return_reason&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Return Reasons</a></li>
            </ul>
          </li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/country&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Countries</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/zone&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Zones</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/geo_zone&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Geo Zones</a></li>
          <li><a class="parent">Taxes</a>
            <ul>
              <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/tax_class&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Tax Classes</a></li>
              <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/tax_rate&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Tax Rates</a></li>
            </ul>
          </li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/length_class&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Length Classes</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=localisation/weight_class&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Weight Classes</a></li>
        </ul>
      </li>
    </ul>
  </li>
  <li id="tools"><a class="parent"><i class="fa fa-wrench fa-fw"></i> <span>Tools</span></a>
    <ul>
      <li><a href="http://demo.opencart.com/admin/index.php?route=tool/upload&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Uploads</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=tool/backup&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Backup / Restore</a></li>
      <li><a href="http://demo.opencart.com/admin/index.php?route=tool/error_log&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Error Logs</a></li>
    </ul>
  </li>
  <li id="reports"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span>Reports</span></a>
    <ul>
      <li><a class="parent">Sales</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/sale_order&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Orders</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/sale_tax&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Tax</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/sale_shipping&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Shipping</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/sale_return&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Returns</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/sale_coupon&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Coupons</a></li>
        </ul>
      </li>
      <li><a class="parent">Products</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/product_viewed&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Viewed</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/product_purchased&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Purchased</a></li>
        </ul>
      </li>
      <li><a class="parent">Customers</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/customer_online&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Customers Online</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/customer_activity&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Customer Activity</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/customer_order&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Orders</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/customer_reward&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Reward Points</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/customer_credit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Credit</a></li>
        </ul>
      </li>
      <li><a class="parent">Marketing</a>
        <ul>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/marketing&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Marketing</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/affiliate&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Affiliates</a></li>
          <li><a href="http://demo.opencart.com/admin/index.php?route=report/affiliate_activity&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Affiliate Activity</a></li>
        </ul>
      </li>
    </ul>
  </li>
</ul>
<div id="stats">
  <ul>
    <li>
      <div>Orders Completed <span class="pull-right">0%</span></div>
      <div class="progress">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">0%</span> </div>
      </div>
    </li>
    <li>
      <div>Orders Processing <span class="pull-right">0%</span></div>
      <div class="progress">
        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">0%</span> </div>
      </div>
    </li>
    <li>
      <div>Other Statuses <span class="pull-right">100%</span></div>
      <div class="progress">
        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">100%</span> </div>
      </div>
    </li>
  </ul>
</div>
</nav>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/add&amp;token=6e21ba49cf5a928c42a3676e618a3ddb" data-toggle="tooltip" title="Add New" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="submit" form="form-product" formaction="http://demo.opencart.com/admin/index.php?route=catalog/product/copy&amp;token=6e21ba49cf5a928c42a3676e618a3ddb" data-toggle="tooltip" title="Copy" class="btn btn-default"><i class="fa fa-copy"></i></button>
        <button type="button" data-toggle="tooltip" title="Delete" class="btn btn-danger" onclick="confirm('Delete/Uninstall cannot be undone! Are you sure you want to do this?') ? $('#form-product').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1>Products</h1>
      <ul class="breadcrumb">
                <li><a href="http://demo.opencart.com/admin/index.php?route=common/dashboard&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Home</a></li>
                <li><a href="http://demo.opencart.com/admin/index.php?route=catalog/product&amp;token=6e21ba49cf5a928c42a3676e618a3ddb">Products</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
            <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Product List</h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-name">Product Name</label>
                <input type="text" name="filter_name" value="" placeholder="Product Name" id="input-name" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-model">Model</label>
                <input type="text" name="filter_model" value="" placeholder="Model" id="input-model" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-price">Price</label>
                <input type="text" name="filter_price" value="" placeholder="Price" id="input-price" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-quantity">Quantity</label>
                <input type="text" name="filter_quantity" value="" placeholder="Quantity" id="input-quantity" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status">Status</label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                                    <option value="1">Enabled</option>
                                                      <option value="0">Disabled</option>
                                  </select>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Filter</button>
            </div>
          </div>
        </div>
        <form action="http://demo.opencart.com/admin/index.php?route=catalog/product/delete&amp;token=6e21ba49cf5a928c42a3676e618a3ddb" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-center">Image</td>
                  <td class="text-left">                    <a href="http://demo.opencart.com/admin/index.php?route=catalog/product&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;sort=pd.name&amp;order=DESC" class="asc">Product Name</a>
                    </td>
                  <td class="text-left">                    <a href="http://demo.opencart.com/admin/index.php?route=catalog/product&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;sort=p.model&amp;order=DESC">Model</a>
                    </td>
                  <td class="text-left">                    <a href="http://demo.opencart.com/admin/index.php?route=catalog/product&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;sort=p.price&amp;order=DESC">Price</a>
                    </td>
                  <td class="text-right">                    <a href="http://demo.opencart.com/admin/index.php?route=catalog/product&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;sort=p.quantity&amp;order=DESC">Quantity</a>
                    </td>
                  <td class="text-left">                    <a href="http://demo.opencart.com/admin/index.php?route=catalog/product&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;sort=p.status&amp;order=DESC">Status</a>
                    </td>
                  <td class="text-right">Action</td>
                </tr>
              </thead>
              <tbody>
                                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="42" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/apple_cinema_30-40x40.jpg" alt="Apple Cinema 30&quot;" class="img-thumbnail" />
                    </td>
                  <td class="text-left">Apple Cinema 30&quot;</td>
                  <td class="text-left">Product 15</td>
                  <td class="text-left">                    <span style="text-decoration: line-through;">100.0000</span><br/>
                    <div class="text-danger">90.0000</div>
                    </td>
                  <td class="text-right">                    <span class="label label-success">990</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=42" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="30" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/canon_eos_5d_1-40x40.jpg" alt="Canon EOS 5D" class="img-thumbnail" />
                    </td>
                  <td class="text-left">Canon EOS 5D</td>
                  <td class="text-left">Product 3</td>
                  <td class="text-left">                    <span style="text-decoration: line-through;">100.0000</span><br/>
                    <div class="text-danger">80.0000</div>
                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=30" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="47" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/hp_1-40x40.jpg" alt="HP LP3065" class="img-thumbnail" />
                    </td>
                  <td class="text-left">HP LP3065</td>
                  <td class="text-left">Product 21</td>
                  <td class="text-left">                    100.0000                    </td>
                  <td class="text-right">                    <span class="label label-success">1000</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=47" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="28" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/htc_touch_hd_1-40x40.jpg" alt="HTC Touch HD" class="img-thumbnail" />
                    </td>
                  <td class="text-left">HTC Touch HD</td>
                  <td class="text-left">Product 1</td>
                  <td class="text-left">                    100.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=28" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="41" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/imac_1-40x40.jpg" alt="iMac" class="img-thumbnail" />
                    </td>
                  <td class="text-left">iMac</td>
                  <td class="text-left">Product 14</td>
                  <td class="text-left">                    100.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=41" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="40" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/iphone_1-40x40.jpg" alt="iPhone" class="img-thumbnail" />
                    </td>
                  <td class="text-left">iPhone</td>
                  <td class="text-left">product 11</td>
                  <td class="text-left">                    101.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=40" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="48" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/ipod_classic_1-40x40.jpg" alt="iPod Classic" class="img-thumbnail" />
                    </td>
                  <td class="text-left">iPod Classic</td>
                  <td class="text-left">product 20</td>
                  <td class="text-left">                    100.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=48" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="36" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/ipod_nano_1-40x40.jpg" alt="iPod Nano" class="img-thumbnail" />
                    </td>
                  <td class="text-left">iPod Nano</td>
                  <td class="text-left">Product 9</td>
                  <td class="text-left">                    100.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=36" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="34" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/ipod_shuffle_1-40x40.jpg" alt="iPod Shuffle" class="img-thumbnail" />
                    </td>
                  <td class="text-left">iPod Shuffle</td>
                  <td class="text-left">Product 7</td>
                  <td class="text-left">                    100.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=34" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="32" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/ipod_touch_1-40x40.jpg" alt="iPod Touch" class="img-thumbnail" />
                    </td>
                  <td class="text-left">iPod Touch</td>
                  <td class="text-left">Product 5</td>
                  <td class="text-left">                    100.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=32" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="43" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/macbook_1-40x40.jpg" alt="MacBook" class="img-thumbnail" />
                    </td>
                  <td class="text-left">MacBook</td>
                  <td class="text-left">Product 16</td>
                  <td class="text-left">                    500.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=43" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="44" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/macbook_air_1-40x40.jpg" alt="MacBook Air" class="img-thumbnail" />
                    </td>
                  <td class="text-left">MacBook Air</td>
                  <td class="text-left">Product 17</td>
                  <td class="text-left">                    1000.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=44" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="45" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/macbook_pro_1-40x40.jpg" alt="MacBook Pro" class="img-thumbnail" />
                    </td>
                  <td class="text-left">MacBook Pro</td>
                  <td class="text-left">Product 18</td>
                  <td class="text-left">                    2000.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=45" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="31" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/nikon_d300_1-40x40.jpg" alt="Nikon D300" class="img-thumbnail" />
                    </td>
                  <td class="text-left">Nikon D300</td>
                  <td class="text-left">Product 4</td>
                  <td class="text-left">                    80.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=31" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="29" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/palm_treo_pro_1-40x40.jpg" alt="Palm Treo Pro" class="img-thumbnail" />
                    </td>
                  <td class="text-left">Palm Treo Pro</td>
                  <td class="text-left">Product 2</td>
                  <td class="text-left">                    279.9900                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=29" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="35" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/no_image-40x40.png" alt="Product 8" class="img-thumbnail" />
                    </td>
                  <td class="text-left">Product 8</td>
                  <td class="text-left">Product 8</td>
                  <td class="text-left">                    100.0000                    </td>
                  <td class="text-right">                    <span class="label label-success">970</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=35" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="49" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/samsung_tab_1-40x40.jpg" alt="Samsung Galaxy Tab 10.1" class="img-thumbnail" />
                    </td>
                  <td class="text-left">Samsung Galaxy Tab 10.1</td>
                  <td class="text-left">SAM1</td>
                  <td class="text-left">                    199.9900                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=49" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="33" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/samsung_syncmaster_941bw-40x40.jpg" alt="Samsung SyncMaster 941BW" class="img-thumbnail" />
                    </td>
                  <td class="text-left">Samsung SyncMaster 941BW</td>
                  <td class="text-left">Product 6</td>
                  <td class="text-left">                    200.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=33" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                <tr>
                  <td class="text-center">                    <input type="checkbox" name="selected[]" value="46" />
                    </td>
                  <td class="text-center">                    <img src="http://demo.opencart.com/image/cache/catalog/demo/sony_vaio_1-40x40.jpg" alt="Sony VAIO" class="img-thumbnail" />
                    </td>
                  <td class="text-left">Sony VAIO</td>
                  <td class="text-left">Product 19</td>
                  <td class="text-left">                    1000.0000                    </td>
                  <td class="text-right">                    <span class="label label-warning">0</span>
                    </td>
                  <td class="text-left">Enabled</td>
                  <td class="text-right"><a href="http://demo.opencart.com/admin/index.php?route=catalog/product/edit&amp;token=6e21ba49cf5a928c42a3676e618a3ddb&amp;product_id=46" data-toggle="tooltip" title="Edit" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                                              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"></div>
          <div class="col-sm-6 text-right">Showing 1 to 19 of 19 (1 Pages)</div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
$('#button-filter').on('click', function() {
  var url = 'index.php?route=catalog/product&token=6e21ba49cf5a928c42a3676e618a3ddb';

  var filter_name = $('input[name=\'filter_name\']').val();

  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_model = $('input[name=\'filter_model\']').val();

  if (filter_model) {
    url += '&filter_model=' + encodeURIComponent(filter_model);
  }

  var filter_price = $('input[name=\'filter_price\']').val();

  if (filter_price) {
    url += '&filter_price=' + encodeURIComponent(filter_price);
  }

  var filter_quantity = $('input[name=\'filter_quantity\']').val();

  if (filter_quantity) {
    url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
  }

  var filter_status = $('select[name=\'filter_status\']').val();

  if (filter_status != '*') {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/product/autocomplete&token=6e21ba49cf5a928c42a3676e618a3ddb&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_name\']').val(item['label']);
  }
});

$('input[name=\'filter_model\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/product/autocomplete&token=6e21ba49cf5a928c42a3676e618a3ddb&filter_model=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['model'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_model\']').val(item['label']);
  }
});
//--></script></div>
<footer id="footer"><a href="http://www.opencart.com">OpenCart</a> &copy; 2009-2015 All Rights Reserved.<br />Version 2.0.0.1b</footer></div>
</body></html>