@include('admin.navbar')

<div class="contentpanel">
	<div class="pull-left breadcrumb_admin clear_both">
		<div class="pull-left page_title theme_color">
			<h1>Categories</h1>
			<h2 class="">Subtitle goes here...</h2>
		</div>
	</div>
	<div class="container clear_both padding_fix">
		<div class="main-content">
			<div class="page-content">
				<div class="row">
					<div class="col-md-12">
						<div class="block-web">
							<div class="header">
								<h3 class="content-header">
									Semua Kategori
								</h3>
							</div>
							<div class="porlets-content">
								<div class="adv-table editable-table">
									<!-- <div class="clearfix">
										<div class="btn-group">
											<button href="#" data-toggle="modal" data-target="#addCategory" id="editable-sample_new" class="btn btn-primary btn-addCategory">
												Add New <i class="fa fa-plus"></i>
											</button>
										</div>
									</div> -->
									<div class="margin-top-10"></div>
									<table class="table table-striped table-hover table-bordered" id="editable-sample">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Kategori</th>
												<th>Sub</th>
												<th>Edit</th>
											</tr>
										</thead>
										<tbody>
											@foreach($categories as $key => $cat)
											<tr>
												<td>{{$key+1}}</td>
												<td>{{$cat->category_name}}</td>
												<td><a href="{{URL::to('/tk-admin/subcategory/'.$cat->id)}}">{{$cat->count_sub}}</a></td>
												<td><a href="#" data-toggle="modal" data-target="#addCategory" data-name="{{$cat->category_name}}" data-id="{{$cat->id}}" class="category-edit">Edit</a></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- modal -->
<div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal row-border" action="" id="form-category" enctype="multipart/form-data" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Tambah Kategori Baru</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Nama Kategori</label>
							</div>
							<div class="col-sm-8">
								<input type="text" name="name" id="category-name" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Tambahkan</button>
				</div>
			</div>
		</form>
	</div>
</div>

@include('admin.footer')