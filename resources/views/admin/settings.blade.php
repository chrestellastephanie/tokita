@include('admin.navbar')
    <div class="contentpanel">
      <!--\\\\\\\ contentpanel start\\\\\\-->
      <div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1>Settings</h1>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <!--\\\\\\\ container  start \\\\\\-->
       
       <div class="row">
       	<div class="col-sm-12">
        
       
       <div class="todo_body ">
       	<h5 class="red_bg"> Ganti Password </h5>
		<ul class="group_sortable1">
        	<li>
            	<form style="padding: 10px;" action="{{URL::to('/tk-admin/changepassword')}}" method="post">
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-3">
                      <label><p><strong>Password Lama</strong></p></label>
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" name="password_old" type="password" id="password_old">
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-3">
                      <label><p><strong>Password Baru</strong></p></label>
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" name="password_new" type="password" id="password_new">
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-3">
                      <label><p><strong>Konfirmasi Password</strong></p></label>
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" name="confirm_password" type="password" id="confirm_password">
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-9">
                      <input type="hidden" name="_token" value="{{csrf_token()}}">
                      <button type="submit" class="btn btn-default pull-right">Submit</button>
                    </div>
                   </div>
                </div>
              </form>
            </li>
        </ul>
        <h5 class="red_bg"> Pengaturan Poin </h5>
    <ul class="group_sortable1">
          <li>
              <form style="padding: 10px;" action="{{URL::to('/tk-admin/changepoin')}}" method="post">
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-2">
                      <label><p><strong>Poin</strong></p></label>
                    </div>
                    <div class="col-md-2">
                      <input class="form-control" name="poin_given" type="number" value="{{$settings['poin_given']}}">
                    </div>
                    <div class="col-md-2">
                      <label><p><strong>Setiap Pembelian</strong></p></label>
                    </div>
                    <div class="col-md-2">
                      <input type="number" name="purchase_for_poin" class="form-control" value="{{$settings['purchase_for_poin']}}" required>
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-9">
                      <input type="hidden" name="_token" value="{{csrf_token()}}">
                      <button type="submit" class="btn btn-default pull-right">Submit</button>
                    </div>
                   </div>
                </div>
              </form>
            </li>
        </ul>
        <h5 class="red_bg"> Accounts & Contacts </h5>
    <ul class="group_sortable1">
          <li>
              <form style="padding: 10px;" action="{{URL::to('/tk-admin/savecontacts')}}" method="post">
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-3">
                      <label><p><strong>Telepon</strong></p></label>
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" name="phone" type="text" id="phone" value="{{$settings['phone']}}">
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-3">
                      <label><p><strong>Email</strong></p></label>
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" name="email" type="email" id="email" value="{{$settings['email']}}">
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-3">
                      <label><p><strong>Facebook</strong></p></label>
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" name="facebook" type="text" id="facebook" value="{{$settings['facebook']}}">
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-3">
                      <label><p><strong>Twitter</strong></p></label>
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" name="twitter" type="text" id="twitter" value="{{$settings['twitter']}}">
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-3">
                      <label><p><strong>Google Plus</strong></p></label>
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" name="google" type="text" id="google" value="{{$settings['google']}}">
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-3">
                      <label><p><strong>Instagram</strong></p></label>
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" name="instagram" type="text" id="instagram" value="{{$settings['instagram']}}">
                    </div>
                   </div>
                </div>
                <div class="form-group">
                  <div class="row vertical-margin">
                    <div class="col-md-9">
                      <input type="hidden" name="_token" value="{{csrf_token()}}">
                      <button type="submit" class="btn btn-default pull-right">Submit</button>
                    </div>
                   </div>
                </div>
              </form>
            </li>
        </ul>
       </div>
        
        
              
        </div>
       </div>
        
      
    </div>
    <!--\\\\\\\ container  end \\\\\\-->
  </div>
  <!--\\\\\\\ content panel end \\\\\\-->
</div>
<!--\\\\\\\ inner end\\\\\\-->
</div>
<!--\\\\\\\ wrapper end\\\\\\-->
@include('admin.footer')