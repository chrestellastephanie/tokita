@include('admin.navbar')

<div class="contentpanel">
	<div class="pull-left breadcrumb_admin clear_both">
		<div class="pull-left page_title theme_color">
			<h1>Static Content</h1>
		</div>
	</div>
	<div class="container clear_both padding_fix">
		<div class="main-content">
			<div class="page-content">
				<div class="row">
					<div class="col-md-12">
						<div class="block-web">
							<div class="header">
								<h3 class="content-header">
									Static Content
								</h3>
							</div>
							<div class="porlets-content">
								<div class="adv-table editable-table">
									<div class="margin-top-10"></div>
									<form enctype="multipart/form-data" id="form-static" action="{{URL::to('/tk-admin/staticcontent/edit/'.$static->id)}}" method="post">
										<table class="table table-striped table-hover table-bordered" id="editable-sample">
											<tbody>
												<tr>
													<td>Judul</td>
													<td><input type="text" name="title" value="{{$static->title}}"></td>
												</tr>
												@if($static->id != 2)
												<tr>
													<td>Featured Image</td>
													<td>
														<input type="file" name="image">
														<br>
														<img class="image-thumbnail-product" src="{{asset('images/contents')}}/{{$static->featured_image}}">
													</td>
												</tr>
												@endif
												<tr>
													<td>Konten</td>
													<td><textarea class="form-control ckeditor" name="content" rows="6">{{$static->content}}</textarea></td>
												</tr>
											</tbody>
											<input type="hidden" name="_token" value="{{csrf_token()}}">
										</table>
										<div class="clearfix">
											<div class="btn-group">
												<button type="submit" class="btn btn-primary">Save</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('admin.footer')