@include('admin.navbar')

<div class="contentpanel">
	<div class="pull-left breadcrumb_admin clear_both">
		<div class="pull-left page_title theme_color">
			<h1>Subcategories</h1>
			<h2 class="">Subtitle goes here...</h2>
		</div>
	</div>
	<div class="container clear_both padding_fix">
		<div class="main-content">
			<div class="page-content">
				<div class="row">
					<div class="col-md-12">
						<div class="block-web">
							<div class="header">
								<h3 class="content-header">
									{{$current_category->category_name}}
								</h3>
							</div>
							<div class="porlets-content">
								<div class="adv-table editable-table">
									<div class="clearfix">
										<div class="btn-group">
											<button href="#" data-toggle="modal" data-target="#addSubCategory" id="editable-sample_new" class="btn btn-primary btn-addSubcategory">
												Add New <i class="fa fa-plus"></i>
											</button>
										</div>
									</div>
									<div class="margin-top-10"></div>
									<table class="table table-striped table-hover table-bordered" id="editable-sample">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama</th>
												<th>Edit</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>
											@foreach($subs as $key => $sub)
											<tr>
												<td>{{$key+1}}</td>
												<td>{{$sub->subcategory_name}}</td>
												<td><a href="#" data-toggle="modal" data-target="#addSubCategory" data-name="{{$sub->subcategory_name}}" data-id="{{$sub->id}}" class="subcategory-edit">Edit</a></td>
												<td><a href="{{URL::to('/tk-admin/subcategory/delete/'.$sub->id)}}">Delete</a></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- modal -->
<div class="modal fade" id="addSubCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal row-border" action="" id="form-subcategory" enctype="multipart/form-data" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Tambah Subkategori Baru</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Nama Subkategori</label>
							</div>
							<div class="col-sm-8">
								<input type="text" name="name" id="subcategory-name" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="category_id" value="{{$current_category->id}}">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Tambahkan</button>
				</div>
			</div>
		</form>
	</div>
</div>


@include('admin.footer')