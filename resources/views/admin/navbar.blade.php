<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TOKITA - Admin</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link href="{{asset('admin/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/css/animate.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/css/admin.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/css/jquerysctipttop.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('admin/plugins/kalendar/kalendar.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('admin/plugins/scroll/nanoscroller.css')}}">
<link href="{{asset('admin/plugins/morris/morris.css')}}" rel="stylesheet" />
<link href="{{asset('admin/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('admin/plugins/file-uploader/css/jquery.fileupload.css')}}">
<link rel="stylesheet" href="{{asset('admin/plugins/file-uploader/css/jquery.fileupload-ui.css')}}">

</head>
<body class="light_theme  fixed_header left_nav_fixed">
<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->
  <div class="header_bar">
    <!--\\\\\\\ header Start \\\\\\-->
    <div class="brand">
      <!--\\\\\\\ brand Start \\\\\\-->
      <div class="logo" style="display:block;"><img src="{{asset('images/logo/logo.png')}}" style="width: 70%;"></div>
      <div class="small_logo" style="display:none"><img src="{{asset('admin/images/s-logo.png')}}" width="50" height="47" alt="s-logo" /> <img src="{{asset('admin/images/r-logo.png')}}" width="122" height="20" alt="r-logo" /></div>
    </div>
    <!--\\\\\\\ brand end \\\\\\-->
    <div class="header_top_bar">
      <!--\\\\\\\ header top bar start \\\\\\-->
      <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
      <!-- <a href="javascript:void(0);" class="add_user" data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus-square"></i> <span> New Task</span> </a> -->
      <!-- <button href="#" data-toggle="modal" data-target="#uploadCSV" class="btn btn-primary">
        Upload CSV <i class="fa fa-plus"></i>
      </button> -->
    </div>
    <!--\\\\\\\ header top bar end \\\\\\-->
  </div>
  <!--\\\\\\\ header end \\\\\\-->
  <div class="inner">
    <!--\\\\\\\ inner start \\\\\\--><div class="left_nav">

      <!--\\\\\\\left_nav start \\\\\\-->
     <!--  <div class="search_bar"> <i class="fa fa-search"></i>
        <input name="" type="text" class="search" placeholder="Search Dashboard..." />
      </div> -->
      <div class="left_nav_slidebar">
        <ul>
          <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>PRODUK<span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul>
              @foreach($categories as $category)
              <li> <a href="{{URL::to('tk-admin/product/'.$category->id)}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">{{$category->category_name}}</b> </a> </li>
              @endforeach
            </ul>
          </li>
          <li class="left_nav_active theme_border"><a href="{{URL::to('tk-admin/category/')}}"><i class="fa fa-bars"></i>KATEGORI<span class="left_nav_pointer"></span></a>
          </li>
          <li class="left_nav_active theme_border"><a href="{{URL::to('tk-admin/transaction/')}}"><i class="fa fa-shopping-cart"></i>TRANSAKSI<span class="left_nav_pointer"></span></a>
          </li>
          <li class="left_nav_active theme_border"><a href="{{URL::to('tk-admin/customer/')}}"><i class="fa fa-male"></i>CUSTOMER<span class="left_nav_pointer"></span></a>
          </li>
          <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-gift"></i>HADIAH & KUPON<span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul>
              <li><a href="{{URL::to('/tk-admin/gift')}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">Hadiah</b> </a> </li>
              <li><a href="{{URL::to('/tk-admin/coupon')}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">Kupon</b> </a> </li>
              <li><a href="{{URL::to('/tk-admin/claimedgift')}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">Pengantaran Hadiah</b> </a> </li>
              <li><a href="{{URL::to('/tk-admin/claimedcoupon')}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">Pemakaian Kupon</b> </a> </li>
            </ul>
          </li>
          <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-file-o"></i>CONTENTS<span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
            <ul>
              <li> <a href="{{URL::to('/tk-admin/staticcontent/2')}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">FAQ</b> </a> </li>
              <li> <a href="{{URL::to('/tk-admin/staticcontent/1')}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">About Us</b> </a> </li>
              <li> <a href="{{URL::to('/tk-admin/staticcontent/3')}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">CEO</b> </a> </li>
              <li> <a href="{{URL::to('/tk-admin/staticcontent/4')}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">CFO</b> </a> </li>
              <li> <a href="{{URL::to('/tk-admin/slider')}}"> <span>&nbsp;</span> <i class="fa fa-circle theme_color"></i> <b class="theme_color">Sliders and Banners</b> </a> </li>
            </ul>
          </li>            
          <li class="left_nav_active theme_border"><a href="{{URL::to('tk-admin/settings')}}"><i class="fa fa-gear"></i>SETTINGS<span class="left_nav_pointer"></span></a>
          </li>
          <li class="left_nav_active theme_border"><a href="{{URL::to('tk-admin/logout')}}"><i class="fa fa-sign-out"></i>LOGOUT<span class="left_nav_pointer"></span></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
