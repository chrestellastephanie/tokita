@include('admin.navbar')

<div class="contentpanel">
	<div class="pull-left breadcrumb_admin clear_both">
		<div class="pull-left page_title theme_color">
			<h1>Image Slider</h1>
			<h2 class="">Subtitle goes here...</h2>
		</div>
	</div>
	<div class="container clear_both padding_fix">
		<div class="main-content">
			<div class="page-content">
				<div class="row">
					<div class="col-md-12">
						<div class="block-web">
							<div class="header">
								<h3 class="content-header">
									Semua Slider
								</h3>
							</div>
							<div class="porlets-content">
								<div class="adv-table editable-table">
									<div class="clearfix">
										<div class="btn-group">
											<button href="#" data-toggle="modal" data-target="#addSlider" id="editable-sample_new" class="btn btn-primary btn-addSlider">
												Add New <i class="fa fa-plus"></i>
											</button>
										</div>
									</div>
									<div class="margin-top-10"></div>
									<table class="table table-striped table-hover table-bordered" id="editable-sample">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama</th>
												<th>Gambar</th>
												<th>Gambar Mobile</th>
												<th>URL</th>
												<th>Edit</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>
											@foreach($sliders as $key => $slider)
											<tr>
												<td>{{$key+1}}</td>
												<td>{{$slider->name}}</td>
												@if($slider->image == "")
												<td class="center" style="max-width: 200px"><img class="image-thumbnail-product" style="max-width: 40%;" src="http://placehold.it/200x200?text=No+image"/></td>
												@else
												<td class="center" style="max-width: 200px"><img class="image-thumbnail-product" style="max-width: 40%;" src="{{asset('images/sliders/'.$slider->image)}}"/></td>
												@endif
												@if($slider->mobile_image == "")
												<td class="center" style="max-width: 200px"><img class="image-thumbnail-product" style="max-width: 40%;" src="http://placehold.it/200x200?text=No+image"/></td>
												@else
												<td class="center" style="max-width: 200px"><img class="image-thumbnail-product" style="max-width: 40%;" src="{{asset('images/mobile_sliders/'.$slider->mobile_image)}}"/></td>
												@endif
												<td><a href="{{$slider->url}}">{{$slider->url}}</a></td>
												<td><a href="#" data-toggle="modal" data-target="#addSlider" data-name="{{$slider->name}}" data-id="{{$slider->id}}" data-image="{{$slider->image}}" data-type="{{$slider->type}}" data-url="{{$slider->url}}" class="slider-edit">Edit</a></td>
												<td><a href="{{URL::to('/tk-admin/slider/delete/'.$slider->id)}}">Delete</a></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="addSlider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal row-border" action="" id="form-slider" enctype="multipart/form-data" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel2">Tambah Slider Baru</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Nama</label>
							</div>
							<div class="col-sm-8">
								<input type="text" name="name" id="slider-name" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Gambar</label>
							</div>
							<div class="col-sm-8">
								<input id="slider-image" type="file" name="image">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Gambar Mobile</label>
							</div>
							<div class="col-sm-8">
								<input id="slider-mobile-image" type="file" name="image2">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">URL</label>
							</div>
							<div class="col-sm-8">
								<input type="text" name="url" id="slider-url" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<div class="col-sm-4">
								<label class="control-label">Type</label>
							</div>
							<div class="col-sm-8">
								<select id="slider-type" name="type" class="form-control">
									<option value="slider">Slider</option>
									<option value="banner">Banner</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Tambahkan</button>
				</div>
			</div>
		</form>
	</div>
</div>

@include('admin.footer')