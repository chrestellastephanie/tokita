<script src="{{asset('admin/js/jquery-2.1.0.js')}}"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/common-script.js')}}"></script>
<script src="{{asset('admin/js/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('admin/plugins/data-tables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/data-tables/DT_bootstrap.js')}}"></script>
<script src="{{asset('admin/plugins/data-tables/dynamic_table_init.js')}}"></script>
<script src="{{asset('admin/plugins/edit-table/edit-table.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/plugins/dropzone/dropzone.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('admin/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('js/jquery.bootstrap-growl.min.js') }}"></script>
<script>
          jQuery(document).ready(function() {
              EditableTable.init();
          });
 </script>
 
 <script src="{{asset('admin/js/jPushMenu.js')}}"></script> 
<script src="{{asset('admin/js/side-chats.js')}}"></script>

 <script type="text/javascript">
	var sub = {};
	@if(isset($product))
		@foreach($categories as $cat)
	        sub[{{$cat->id}}] = [
	        @foreach($cat->subcat as $key => $sub)
	        	"<option value='{{$sub->id}}'>{{$sub->subcategory_name}}</option>"
	        	@if($key != count($cat->subcat)-1)
	        		,
	        	@endif
	        @endforeach
	        ];
	    @endforeach
    @endif
	$("#product-category").change(function () {
	    var val = $(this).val();
	    $("#product-subcategory").html(sub[val]);
	});
	$("#product-price").keyup(function () {
		if(!($("#product-discount").is(':checked'))){
	    	$("#product-discounted").val($(this).val());
	    }
	});
	$("#product-discount").change(function() {
	    if(this.checked) {
	        $("#product-discounted").attr("readonly", false);
	    }
	    else{
	    	$("#product-discounted").attr("readonly", true);
	    	if($('#product-discounted').val() != $('#product-price').val()){
	    		$('#product-discounted').val($('#product-price').val());
	    	}
	    }
	});
 	$('.product-edit').click(function(){
 		$("#form-product").attr("action", "{{URL::to('/tk-admin/product/edit/')}}"+'/'+$(this).data('id'));
 		$('#myModalLabel').html("Edit Produk");
 		$('#product-name').val($(this).data('name'));
 		$('#product-barcode').val($(this).data('barcode'));
 		$('#product-price').val($(this).data('price'));
 		$('#product-discounted').val($(this).data('discounted'));
 		$('#product-description').html($(this).data('description'));
 		$('#product-stock').val($(this).data('stock'));
 		$('#product-category').val($(this).data('category'));
 		$('#product-subcategory').val($(this).data('subcategory'));
 		if($('#product-discounted').val() != $('#product-price').val()){
    		$('#product-discount').attr('checked', true);
    		$("#product-discounted").attr("readonly", false);
    	}
    	else{
    		$('#product-discount').attr('checked', false);
    		$("#product-discounted").attr("readonly", true);
    	}
 	});

 	$('.btn-addProduct').click(function(){
 		$("#form-product").attr("action", "{{URL::to('/tk-admin/product/save')}}");
 		$('#myModalLabel').html("Tambah Produk");
 		$('#product-name').val('');
 		$('#product-barcode').val('');
 		$('#product-price').val('');
 		$('#product-discounted').val('');
 		$('#product-description').html('');
 		$('#product-stock').val('');
 	});

 	$('.category-edit').click(function(){
 		$("#form-category").attr("action", "{{URL::to('/tk-admin/category/edit/')}}"+'/'+$(this).data('id'));
 		$('#myModalLabel').html("Edit Kategori");
 		$('#category-name').val($(this).data('name'));
 	});

 	$('.subcategory-edit').click(function(){
 		$("#form-subcategory").attr("action", "{{URL::to('/tk-admin/subcategory/edit/')}}"+'/'+$(this).data('id'));
 		$('#myModalLabel').html("Edit Kategori");
 		$('#subcategory-name').val($(this).data('name'));
 	});

 	$('.btn-addSubcategory').click(function(){
 		$("#form-subcategory").attr("action", "{{URL::to('/tk-admin/subcategory/save')}}");
 		$('#myModalLabel').html("Tambah Subkategori");
 		$('#subcategory-name').val('');
 	});

 	$('.btn-addGift').click(function(){
 		$("#form-gift").attr("action", "{{URL::to('/tk-admin/gift/save')}}");
 		$('#myModalLabel').html("Tambah Gift");
 		$('#gift-name').val('');
 		$('#gift-point').val('');
 	});

 	$('.gift-edit').click(function(){
 		$("#form-gift").attr("action", "{{URL::to('/tk-admin/gift/edit/')}}"+'/'+$(this).data('id'));
 		$('#myModalLabel').html("Edit Hadiah");
 		$('#gift-name').val($(this).data('name'));
 		$('#gift-point').val($(this).data('point'));
 	});

 	$('.btn-addCoupon').click(function(){
 		$("#form-coupon").attr("action", "{{URL::to('/tk-admin/coupon/save')}}");
 		$('#myModalLabel2').html("Tambah Coupon");
 		$('#coupon-name').val('');
 		$('#coupon-point').val('');
 	});

 	$('.coupon-edit').click(function(){
 		$("#form-coupon").attr("action", "{{URL::to('/tk-admin/coupon/edit/')}}"+'/'+$(this).data('id'));
 		$('#myModalLabel2').html("Edit Kupon");
 		$('#coupon-nominal').val($(this).data('nominal'));
 		$('#coupon-point').val($(this).data('point'));
 	});

 	$('.btn-addSlider').click(function(){
 		$("#form-slider").attr("action", "{{URL::to('/tk-admin/slider/save')}}");
 		$('#myModalLabel').html("Tambah Slider");
 		$('#slider-name').val('');
 		$('#slider-url').val('');
 		$('#slider-type').val('');
 	});

 	$('.slider-edit').click(function(){
 		$("#form-slider").attr("action", "{{URL::to('/tk-admin/slider/edit')}}"+'/'+$(this).data('id'));
 		$('#myModalLabel').html("Edit Slider");
 		$('#slider-name').val($(this).data('name'));
 		$('#slider-url').val($(this).data('url'));
 		$('#slider-type').val($(this).data('type'));
 	});
 </script>
 @if(isset($success_message))
<script type="text/javascript">
	$(function(){
		setTimeout(function() {
	        $.bootstrapGrowl("{{$success_message}}", {
	            type: 'success',
	            align: 'center',
	            width: 'auto',
	            offset: {from: 'top', amount: 120},
	            allow_dismiss: true,
	        });
	    }, 100);	
	});
</script>
@endif
@if(isset($error_message))
<script type="text/javascript">
	$(function(){
		setTimeout(function() {
	        $.bootstrapGrowl("{{$error_message}}", {
	            type: 'danger',
	            align: 'center',
	            width: 'auto',
	            offset: {from: 'top', amount: 120},
	            allow_dismiss: true,
	        });
	    }, 100);	
	});
</script>
@endif

</body>
</html>
