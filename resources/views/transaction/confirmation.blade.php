@extends('main')

@section('left-navbar')
	<div class="menu">
		@include('template.sidebar-event')
		@include('template.sidebar-all-category') 
	</div>
@endsection

@section('pagetitle')
	<div class="row pageTitleSmall">
		<div class="col-sm-6">
			<div class=" sectionTitle-title">
				<span>
		            <img src="{{ asset('images/icon/keranjang.png') }}"/>
		            Rangkuman Keranjang
		        </span>
			</div>
		</div>
		<div class="col-sm-6 align-right">
			<button class="btn-default buttonTokita buttonMedium buttonCancelOrder">CANCEL ORDER</button>
		</div>
	</div>	
@endsection

@section('content')
	<div class="cartListWrapper">
		<div class="cartListHeading">
			<div class="row">
				<div class="col-xs-7">				
					<i>Produk yang dibeli</i>
				</div>
				<div class="col-xs-5">
					<i>Subtotal</i>
				</div>
			</div>
		</div>
		<form class="form-rows" action="{{URL::to('/setSessionTransaction')}}" method="post">
			<input type="hidden" name="customer_id" value="{{$loggedin->id}}" />
			@foreach($cart_items as $cart)
			<div class="cartProductRow">
				<div class="row">
					<div class="col-xs-6 col-sm-7">
						<div class="cartProductName">
							{{$cart->item_detail()->item_name}}
						</div>
						<div class="row">
							<div class="col-sm-7">
								<div class="cartProductPrice">
									Rp {{$cart->item_detail()->discounted_item_price}}
								</div>
							</div>
							<div class="col-sm-4">
								<div class="input input-small detailAmount">
									<input id="qty-{{$cart->id}}" class="form-inline" type="number" value="{{$cart->quantity}}" readonly/>
									<input id="cart{{$cart->id}}" type="hidden" name="carts[]" class="form-inline" value="{{$cart->id}}" readonly/>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="cartProductSubtotal">
							Rp {{$cart->quantity * $cart->item_detail()->discounted_item_price}}
						</div>
					</div>
				</div>
			</div>
			@endforeach
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="row coupon-selector">
				<div class="col-xs-6 col-sm-4">
					<b>Kupon</b>
				</div>
				<div class="col-xs-6 col-sm-3">
					<select class="form-control" id="list-coupon" name="coupon" onchange="changePrice(this)">
						<option value="0">Tanpa Kupon - Rp 0</option>
					@foreach($claimedCoupons as $coupon)
						<option value="{{$coupon->id}}">Kupon {{substr($coupon->identifier, -6)}} - Rp {{$coupon->coupon_details()->nominal}}</option>
					@endforeach
					</select>
				</div>
				<div class="col-xs-6 col-sm-3">Rp <span id="couponNominal">0</span></div>
			</div>
		</div>		
	</div>	
	<div class="row">
		<div class="col-md-12">
			<div class="cartTotalPrice row">
				<div class="col-xs-6 col-sm-3 col-sm-offset-4">
					TOTAL
				</div>
				<div class="col-xs-6 col-sm-3">
					Rp <span id="totalOrder">{{$total}}</span>
				</div>
			</div>
		</div>		
	</div>	

	<div class=" sectionTitle-title">
		<span>
            Data Pengiriman
        </span>
	</div>
	<div class="requiredWarning">
		* Wajib diisi
	</div>
	<div class="form-margin-top">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="row">
				<div class="col-sm-3">
					Nama<textRed>*</textRed>
				</div>
				<div class="col-sm-9">
					<input type="text" name="name" class="form-control" placeholder="Nama Anda" value="{{$loggedin->customer_name}}" required>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					No Telp<textRed>*</textRed>
				</div>
				<div class="col-sm-9">
					<input type="text" name="phone" class="form-control" placeholder="08xxxxxxxxx" value="{{$loggedin->customer_phone}}" required>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					Email<textRed>*</textRed>
				</div>
				<div class="col-sm-9">
					<input type="email" name="email" class="form-control" placeholder="anonimus@tokita.com" value="{{$loggedin->customer_email}}" required>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					Alamat<textRed>*</textRed>
				</div>
				<div class="col-sm-9">
					<textarea class="form-control" name="address" placeholder="Jalan Sumatra no 1" required>{{$loggedin->customer_address}}</textarea>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					Permintaan Khusus
				</div>
				<div class="col-sm-9">
					<input type="text" name="request" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					Nominal Pembayaran<textRed>*</textRed>
				</div>
				<div class="col-sm-9">
					<input type="number" name="payment" class="form-control" placeholder="100000" required>
				</div>
			</div>
	</div>
	<div class="row">
		<div class="col-md-12 align-right cta_cart">
			<button type="submit" class="button btn-default buttonYellow pull-right">SELESAI</button>
		</div>
		</form>
	</div>
	<div class="row">
		<div class="col-sm-4 col-md-3 col-sm-offset-8 col-md-offset-9">
			<div class="cartNote">
				<i>Setelah checkout, item tidak dapat diedit kembali</i>
			</div>
		</div>
	</div>	
@endsection
@section('scripts')
<script type="text/javascript">
	function changePrice(elm){
		text = $("#list-coupon option[value='"+elm.value+"']").text()
		var res = text.split("Rp "); 
		total = parseInt($('#totalOrder').html(), 10);
		prev = parseInt($('#couponNominal').html(), 10);

		$('#couponNominal').html(res[1]);
		$('#totalOrder').html(total+prev-parseInt(res[1], 10));
	}
</script>
@endsection
