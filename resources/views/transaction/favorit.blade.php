@extends('main')

@section('left-navbar')
    <div class="menu">
        @if(isset($current_cat))
            @include('template.sidebar-category')
        @endif
        @include('template.sidebar-all-category')
    </div>
@endsection

@section('pagetitle')
	<div class="pageTitleSmall sectionTitle-title">
		<span>
            <img src="{{ asset('images/icon/favorit.png') }}"/>
            Produk Favorit
        </span>
	</div>
@endsection

@section('content')
    <div class="HPsection">
        <div class="sectionProducts">
            <!-- ini kalo ga ada produknya : <br> -->
            @if(sizeof($favs) == 0)
            <div class="noProductWarning">
                <span>
                    Belum ada produk favorit 
                </span>
            </div>
            @else
            <!-- ini kalo ada produknya : <br> -->
            <div class="row">                
                @foreach($items as $item)
                  @include('template.product-panel')
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection
