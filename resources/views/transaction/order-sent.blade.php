@extends('main')

@section('left-navbar')
	<div class="menu">
		@include('template.sidebar-event')
		@include('template.sidebar-all-category')
	</div>
@endsection

@section('pagetitle')
	<div class="pageTitleSmall">
		<div class=" sectionTitle-title">
		</div>
	</div>	
@endsection

@section('content')
	<div class="sentWrapper">
		<img src="{{ asset('images/logo/logo_min.png') }}"/>
		<div class="sentWrapperTitle">
			Terima Kasih!
		</div>
		<div class="sentWrapperContent">
			Pesanan Anda sudah diproses. Tokita menjanjikan pengantaran paling lambat 30 menit. Kontak kami di 022 87788751 / 082231845323 atau admin@tokita.com apabila terjadi kesalahan atau keterlambatan
		</div>
	</div>
@endsection