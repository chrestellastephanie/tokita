@extends('main')

@section('left-navbar')
	<div class="menu">
		@include('template.sidebar-event')
		@include('template.sidebar-all-category')
	</div>
@endsection

@section('pagetitle')
	<div class="row pageTitleSmall">
		<div class="col-sm-6">
			<div class=" sectionTitle-title">
				<span>
		            <img src="{{ asset('images/icon/keranjang.png') }}"/>
		            Rangkuman Keranjang
		        </span>
			</div>
		</div>
		<div class="col-sm-6 align-right">
			<a href="{{URL::to('/cart/deleteAll')}}"><button class="btn-default buttonTokita buttonMedium buttonCancelOrder">CANCEL ORDER</button></a>
		</div>
	</div>	
@endsection

@section('content')
	@if(sizeof($cart_items) == 0)
    <div class="noProductWarning">
        <span>
            Keranjang belanja kosong 
        </span>
    </div>
    @else
		<div class="cartListWrapper">
			<div class="cartListHeading">
				<div class="row">
					<div class="col-xs-7">				
						<i>Produk yang dibeli</i>
					</div>
					<div class="col-xs-5">
						<i>Subtotal</i>
					</div>
				</div>
			</div>
			<form method="post" action="{{URL::to('cart/editCart')}}">
			<input type="hidden" name="customer_id" value="{{$loggedin->id}}" />
			<input type="hidden" name="_token" value="{{csrf_token()}}">
				@foreach($cart_items as $cart)
				<div class="cartProductRow">
					<div class="row">
						<div class="col-xs-6 col-sm-7">
							<div class="cartProductName">
								{{$cart->item_detail()->item_name}}
							</div>
							<div class="row">
								<div class="col-sm-7">
									<div class="cartProductPrice">
										Rp {{$cart->item_detail()->discounted_item_price}}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input input-small detailAmount">
										<span class="arrowButton arrowButton-small arrowButtonDown" id="arrowButtonDown-{{$cart->id}}">
											<button>-</button>
										</span>
										<input id="qty-{{$cart->id}}" name="qty{{$cart->id}}" class="form-inline" type="number" value="{{$cart->quantity}}" readonly/>
										<span class="arrowButton arrowButton-small arrowButtonUp" id="arrowButtonUp-{{$cart->id}}">
											<button>+</button>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="cartProductSubtotal">
								Rp <span id="cartProductSubtotal{{$cart->id}}">{{$cart->quantity * $cart->item_detail()->discounted_item_price}}</span>
							</div>
						</div>
						<div class="col-xs-2 col-sm-1">
							<div class="cartDelete">
								<a href="" data-toggle="modal" data-id="{{$cart->id}}" data-target="#modalDeleteItemFromCart" class="delete">
									<span aria-hidden="true">&times;</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				@endforeach
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="cartTotalPrice">
					<div class="col-xs-6 col-sm-3 col-sm-offset-4">
						TOTAL
					</div>
					<div class="col-xs-6 col-sm-3">
						Rp <span id="totalOrder">{{$total}}</span>
					</div>
				</div>
			</div>
			<div class="col-md-12 align-right cta_cart">
				<button type="submit" name="edit" class="buttonTokita buttonMedium align-right">SIMPAN</button>
				<button type="submit" name="continue" class="btn-default buttonYellow buttonMedium align-right">CHECKOUT</button></a>
			</div>
			</form>
			<div class="col-sm-4 col-md-3 col-sm-offset-8 col-md-offset-9">
				<div class="cartNote">
					<i>Setelah checkout, item tidak dapat diedit kembali</i>
				</div>
			</div>
			
		</div>	
	@endif
@endsection
@section('scripts')
	<script type="text/javascript">
		$(document).ready(function(e){
		@foreach($cart_items as $cart)
			$('#arrowButtonDown-{{$cart->id}}').click(function(e){
				e.preventDefault();
				if(parseInt($('#qty-{{$cart->id}}').val(), 10)>1){
					$('#qty-{{$cart->id}}').val(parseInt($('#qty-{{$cart->id}}').val(), 10)-1);
					$('#cartProductSubtotal{{$cart->id}}').html(parseInt($('#cartProductSubtotal{{$cart->id}}').html(), 10)-parseInt("{{$cart->item_detail()->discounted_item_price}}", 10));
					$('#totalOrder').html(parseInt($('#totalOrder').html(), 10)-parseInt("{{$cart->item_detail()->discounted_item_price}}", 10));
				}
			});
			$('#arrowButtonUp-{{$cart->id}}').click(function(e){
				e.preventDefault();
				if(parseInt($('#qty-{{$cart->id}}').val(), 10)<{{$cart->item_detail()->stock}}+{{$cart->quantity}}){
					$('#qty-{{$cart->id}}').val(parseInt($('#qty-{{$cart->id}}').val(), 10)+1);
					$('#cartProductSubtotal{{$cart->id}}').html(parseInt($('#cartProductSubtotal{{$cart->id}}').html(), 10)+parseInt("{{$cart->item_detail()->discounted_item_price}}", 10));
					$('#totalOrder').html(parseInt($('#totalOrder').html(), 10)+parseInt("{{$cart->item_detail()->discounted_item_price}}", 10));
				}
			});
		@endforeach
			$('.delete').click(function(){
				$('#link-delete').attr("href","{{URL::to('/cart/delete')}}"+"/"+$(this).data("id"));
			});
		});	
	</script>
@endsection
	

@section('modal')
	<div id="modalDeleteItemFromCart" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm modal-small">
		<div class="modal-content ">
			<div class="align-center">
				<img src="{{ asset('images/logo/logo_min.png')}}" />
			</div>
			<div class="modal-close">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="modalContent align-center">
				Apakah anda yakin mau menghapus item ini?
				<div class="row">
					<div class="col-xs-4 col-xs-offset-2">
						<a href="" id="link-delete"><button class="button-default buttonTokita buttonMedium fullWidth">ya</button></a>
					</div>
					<div class="col-xs-4">
						<button data-dismiss="modal" class="button-default btn buttonYellow buttonMedium fullWidth">tidak</button>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>
@endsection
