@extends('main')

@section('left-navbar')
	<div class="menu">
		@include('template.sidebar-event')
		@include('template.sidebar-all-category') 
	</div>
@endsection

@section('pagetitle')
	<div class="row pageTitleSmall">
		<div class="col-sm-6">
			<div class=" sectionTitle-title">
				<span>
		            <img src="{{ asset('images/icon/keranjang.png') }}"/>
		            Rangkuman Keranjang
		        </span>
			</div>
		</div>
		<div class="col-sm-6 align-right">
			<button class="btn-default buttonTokita buttonMedium buttonCancelOrder">CANCEL ORDER</button>
		</div>
	</div>	
@endsection

@section('content')
	<div class="cartListWrapper">
		<div class="cartListHeading">
			<div class="row">
				<div class="col-xs-7">				
					<i>Produk yang dibeli</i>
				</div>
				<div class="col-xs-5">
					<i>Subtotal</i>
				</div>
			</div>
		</div>
		<form method="post" action="/tokita/public/editCart">
			<input type="hidden" name="customer_id" value="{{$loggedin->id}}" />
			@foreach($cart_items as $cart)
			<div class="cartProductRow">
				<div class="row">
					<div class="col-xs-6 col-sm-7">
						<div class="cartProductName">
							{{$cart->item_detail()->item_name}}
						</div>
						<div class="row">
							<div class="col-sm-7">
								<div class="cartProductPrice">
									Rp {{$cart->item_detail()->discounted_item_price}}
								</div>
							</div>
							<div class="col-sm-4">
								<div class="input input-small detailAmount">
									<input id="qty-{{$cart->id}}" class="form-inline" type="number" value="{{$cart->quantity}}" readonly/>
								</div>
							</div>	
						</div>
					</div>
					<div class="col-xs-4">
						<div class="cartProductSubtotal">
							Rp {{$cart->quantity * $cart->item_detail()->discounted_item_price}}
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</form>
	</div>
	@if($coupon)
	<div class="row">
		<div class="col-md-12">
			<div class="row coupon-selector">
				<div class="col-xs-6 col-sm-4">
					<b>Kupon</b>
				</div>
				<div class="col-xs-6 col-sm-3">
					Kupon {{substr($coupon->identifier, -6)}}
				</div>
				<div class="col-xs-6 col-sm-3">
					Rp {{$coupon->coupon_details()->nominal}}
				</div>
			</div>
		</div>		
	</div>
	@endif
	<div class="row">
		<div class="col-md-12">
			<div class="cartTotalPrice row">
				<div class="col-xs-6 col-sm-3 col-sm-offset-4">
					TOTAL
				</div>
				<div class="col-xs-6 col-sm-3">
					Rp {{$total}}
				</div>
			</div>
		</div>		
	</div>	
	<div class=" sectionTitle-title">
		<span>
            Data Pengiriman
        </span>
	</div>

	<form class="form-rows">
		<b>
			<div class="row">
				<div class="col-sm-3">
					Nama
				</div>
				<div class="col-sm-9">
					{{$transaction['name']}}
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					No Telp
				</div>
				<div class="col-sm-9">
					{{$transaction['phone']}}
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					Email
				</div>
				<div class="col-sm-9">
					{{$transaction['email']}}
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					Alamat
				</div>
				<div class="col-sm-9">
					{{$transaction['address']}}
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					Permintaan Khusus
				</div>
				<div class="col-sm-9">
					{{$transaction['request']}}
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					Nominal Pembayaran
				</div>
				<div class="col-sm-9">
					{{$transaction['payment']}}
				</div>
			</div>
		</b>
	</form>
	<div class="row">
		<div class="col-md-12 align-right cta_cart">
			<a href="order-sent"><button class="button btn-default buttonYellow">KIRIM</button></a>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4 col-md-3 col-sm-offset-8 col-md-offset-9">
			<div class="cartNote">
				<i>Semua data harus dipastikan benar
					atau ubah data kembali <a href="cart"><u>di sini</u></a></i>
			</div>
		</div>
	</div>	
@endsection