<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>Tokita</title>
<base href="http://127.0.0.1:8080/tokita/public/" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="{{asset('/javascript/bootstrap/opencart/opencart.css')}}" type="text/css" rel="stylesheet" />
<link href="{{asset('/javascript/font-awesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet" />
<link href="{{asset('/javascript/summernote/summernote.css')}}" rel="stylesheet" />
<script type="text/javascript" src="{{asset('/javascript/summernote/summernote.js')}}"></script>
<script src="{{asset('/javascript/jquery/datetimepicker/moment.js')}}" type="text/javascript"></script>
<script src="{{asset('/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<link href="{{asset('/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css')}}" type="text/css" rel="stylesheet" media="screen" />
<link type="text/css" href="{{asset('/css/stylesheet.css')}}" rel="stylesheet" media="screen" />
<script src="{{asset('/javascript/common.js')}}" type="text/javascript"></script>
</head>
<body>
<div id="container">
<header id="header" class="navbar navbar-static-top">
  <div class="navbar-header">
    @if(isset($loggedin))
    <a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>
    @endif
    <a href="home" class="navbar-brand"><img src="view/image/logo.png" alt="title" title="tokita" /></a></div>
  @if(isset($loggedin))
  <ul class="nav pull-right">
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><span class="label label-danger pull-left"><?php echo $alerts; ?></span> <i class="fa fa-bell fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right alerts-dropdown">
        <li class="dropdown-header">Order</li>
        <li><a href="orederStatus" style="display: block; overflow: auto;"><span class="label label-warning pull-right">Order Status Total</span>Order Status</a></li>
        <li><a href="completeStatus"><span class="label label-success pull-right">complete status total</span>Complete Status</a></li>
        <li><a href="return"><span class="label label-danger pull-right">return total</span>Return</a></li>
        <li class="divider"></li>
        <!-- <li class="dropdown-header">Customer</li>
        <li><a href="online"><span class="label label-success pull-right"><?php echo $online_total; ?></span><?php echo $text_online; ?></a></li>
        <li><a href="customer_approval"><span class="label label-danger pull-right"><?php echo $customer_total; ?></span><?php echo $text_approval; ?></a></li>
        <li class="divider"></li>
        <li class="dropdown-header"><?php echo $text_product; ?></li>
        <li><a href="<?php echo $product; ?>"><span class="label label-danger pull-right"><?php echo $product_total; ?></span><?php echo $text_stock; ?></a></li>
        <li><a href="<?php echo $review; ?>"><span class="label label-danger pull-right"><?php echo $review_total; ?></span><?php echo $text_review; ?></a></li>
        <li class="divider"></li>
        <li class="dropdown-header"><?php echo $text_affiliate; ?></li>
        <li><a href="<?php echo $affiliate_approval; ?>"><span class="label label-danger pull-right"><?php echo $affiliate_total; ?></span><?php echo $text_approval; ?></a></li> -->
      </ul>
    </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-life-ring fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right">
        <li class="dropdown-header">Store<i class="fa fa-shopping-cart"></i></li>
        <li class="divider"></li>
        <!-- <li class="dropdown-header"><?php echo $text_help; ?> <i class="fa fa-life-ring"></i></li>
        <li><a href="http://www.opencart.com" target="_blank"><?php echo $text_homepage; ?></a></li>
        <li><a href="http://docs.opencart.com" target="_blank"><?php echo $text_documentation; ?></a></li>
        <li><a href="http://forum.opencart.com" target="_blank"><?php echo $text_support; ?></a></li> -->
      </ul>
    </li>
    <li><a href="logout"><span class="hidden-xs hidden-sm hidden-md">Logout</span> <i class="fa fa-sign-out fa-lg"></i></a></li>
  </ul>
  @endif
</header>
