@extends('main')

@section('pagetitle')
	<img class="fullWidth" src="{{ asset('images/'.$about->featured_image) }}" alt="tokita"/>
	<div class="pageTitleSmall">
		<div class=" sectionTitle-title">
			<span>
				ABOUT US
	        </span>
		</div>
	</div>	
@endsection

@section('left-navbar')
	<div class="menu ">
		@include('template.sidebar-event')
        @include('template.sidebar-all-category')
	</div>
@endsection

@section('content')
	<div class="form-top-margin">
		<div class="row">
			<div class="col-sm-9">
				{!!$about->content!!}
			</div>
			<div class="col-sm-3 hidden-xs">
				<div class="owner-profile">
					<img class="fullWidth" src="{{ asset('images/'.$ceo->featured_image) }}" alt="tokita"/>
					<textred>
						<b>{{$ceo->title}}<br>CEO/OWNER TOKITA</b>
					</textred>
					<br>
					<i class="fontSmall">{!!$ceo->content!!}</i>
				</div>

				<div class="owner-profile">
					<img class="fullWidth" src="{{ asset('images/'.$cfo->featured_image) }}" alt="tokita"/>
					<textred>
						<b>{{$cfo->title}}<br>CFO/OWNER TOKITA</b>
					</textred>
					<br>
					<i class="fontSmall">{!!$cfo->content!!}</i>
				</div>
			</div>
		</div>
	</div>
@endsection