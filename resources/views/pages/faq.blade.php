@extends('main')

@section('pagetitle')
	<div class="pageTitleSmall">
		<div class=" sectionTitle-title">
			<div class="hidden-xs">
				<span>
					FREQUENTLY ASKED QUESTION (FAQ)
	        	</span>
			</div>
			<div class="visible-xs">
				<span>
					FAQ
	        	</span>
			</div>
		</div>
	</div>	
@endsection

@section('left-navbar')
	<div class="menu ">
		@include('template.sidebar-event')
        @include('template.sidebar-all-category')
	</div>
@endsection

@section('content')
	<div class="form-top-margin">
		<div class="faq-item">
		{!!$faq->content!!}
		</div>
	</div>
@endsection