@extends('main')

@section('left-navbar')
    <div class="menu">
        @if(isset($subcategories))
            @include('template.sidebar-category')    
        @endif
        @include('template.filter-harga')
        @include('template.sidebar-event')
        @include('template.sidebar-all-category')
        @include('template.sidebar-ad')
    </div>
@endsection

@section('pagetitle')
    @if(isset($query))
    <div class="pageTitleSmall">
        <div class=" sectionTitle-title">
            <span>
                Hasil Pencarian : {{$query}}
            </span>
        </div>
    </div>
    @endif
@endsection

@section('content')
    <div class="visible-xs">
        <div class="align-right">
            <button class="button btn-default buttonTokita buttonMedium filtermobilebutton"><img src="{{ asset('images/icon/filtermobile.png') }}" alt="" /></button>
        </div>
        <div class="filtermobile">
            @include('template.filter-harga')
        </div>
    </div>
    @if(sizeof($items) != 0)
        <div class="filterSection">
            <div class="row">
                 <div class="col-xs-12 col-sm-4 col-sm-offset-8 align-right">
                    <select class="form-control" onchange="changeSortOrder(this);">
                        <option value="1">Urutkan dari paling baru</option>
                        <option value="2">Urutkan dari paling murah</option>
                        <option value="3">Urutkan dari paling mahal</option>
                        <option value="4">Urutkan dari A-Z</option>
                        <option value="5">Urutkan dari Z-A</option>
<!--                         <option value="6">Urutkan dari diskon paling besar</option> -->
                    </select>
                </div>
            </div>
        </div>
    @endif
	<div class="HPsection">
        <div class="sectionProducts">
            @if(sizeof($items) == 0)
            <div class="noProductWarning">
                <span>
                    Hasil tidak ditemukan 
                </span>
            </div>
            @else
            <div class="row">
                @foreach($items as $item)
                  @include('template.product-panel')
                @endforeach
            </div>
            @endif
        </div>
    </div>

<script type="text/javascript">
$(window).load(function(){
    $(".filtermobile").slideUp();
    $('.filtermobilebutton').click(function(e){
        e.preventDefault();
        if ($('.filtermobile').is(':visible')){
            $(".filtermobile").slideUp();
        }else{
            $(".filtermobile").slideDown();
        }
        
    });
});
$(window).resize(function(){
    $(".filtermobile").slideUp();
});
function changeSortOrder(elm){
    if(elm.value == 1){
        window.location="{{Request::url()}}"+"?sort_by=created_at&order=desc";
    }
    else if(elm.value == 2){
        window.location="{{Request::url()}}"+"?sort_by=discounted_item_price&order=asc";
    }
    else if(elm.value == 3){
        window.location="{{Request::url()}}"+"?sort_by=discounted_item_price&order=desc";
    }
    else if(elm.value == 4){
        window.location="{{Request::url()}}"+"?sort_by=item_name&order=asc";
    }
    else if(elm.value == 5){
        window.location="{{Request::url()}}"+"?sort_by=item_name&order=desc";
    }
}
</script>
@endsection