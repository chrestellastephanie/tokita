<!DOCTYPE html><html lang="en" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-family: 'Open Sans', sans-serif;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 62.5%;-webkit-tap-highlight-color: rgba(0,0,0,0);position: relative;min-height: 100%;">
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
<head style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<title style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">Tokita</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<link href="http://projects.dextrovert.com/tokita/public/css/bootstrap.min.css" rel="stylesheet" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<link href="http://projects.dextrovert.com/tokita/public/css/style.css" rel="stylesheet" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<script src="http://projects.dextrovert.com/tokita/public/js/jquery.min.js" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"></script>
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"></script>
	<script src="http://projects.dextrovert.com/tokita/public/js/bootstrap.min.js" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"></script> 
</head>

<body style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin: 0;font-family: &quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size: 14px;line-height: 1.428571429;color: #333;background-color: #fff;">
<div class="container" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-right: 0px;padding-left: 0px;margin-right: auto;margin-left: auto;">
	<div class="invoice-header" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
		<div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
			<div class="col-sm-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;width: 100%;">
				<div class="pull-left" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;float: left!important;">
					<a style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background: transparent;color: #393939;text-decoration: none !important;"><img src="http://projects.dextrovert.com/tokita/public/images/logo/tagline_navbar.png" alt="" class="display-block " style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;display: block;width: 200px;max-width: 100%!important;"></a>
					<a style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background: transparent;color: #393939;text-decoration: none !important;"><img src="http://projects.dextrovert.com/tokita/public/images/logo/logo.png" alt="" class="display-block invoice-logo" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;display: block;margin-top: 5px;width: 200px;max-width: 100%!important;"></a>
				</div>
			</div>
		</div>
	</div>	
	<div class="invoice-body" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-top: 45px;display:inline-block;">
		<div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: 0px;">
			<div class="col-sm-12" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;width: 100%;">	
				Dear Bapak/Ibu {{$name}},
				<br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"><br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
				Email ini adalah respon otomatis dari proses registrasi. Untuk mendaftarkan alamat {{$email}}, silakan klik <br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
				{{URL::to('/verify')}}?user={{$email}}&verify={{$token}}
				<br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
				Jika Anda tidak merasa mendaftarkan diri, silakan hapus e-mail ini.
			</div>
		</div>
	</div>
	<div class="invoice-footer" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-top: 30px;padding-top: 45px;padding-bottom: 45px;border-bottom: solid #FCB812 3px;-webkit-print-color-adjust: exact;background-color: #E6E6E6 !important; overflow: hidden;">
		<div class="row" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
			<div class="col-sm-3" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 25%;">
				<div class="invoice-footer-logo" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;text-align: center;">
					<img src="http://projects.dextrovert.com/tokita/public/images/logo/logo_min_nobg.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;position: relative;margin-left: auto;margin-right: auto;margin-top: 30px;max-width: 100%!important;">
				</div>
			</div>
			<div class="col-sm-4 footerContent" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 33.33333333333333%;color: #999999;padding-top: 5px;padding-bottom: 5px;">
				Villa Bukit Mas Blok Baru no. 4 <br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
				Bojong Koneng, Cikutra, Bandung <br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
				022 87788751 / 082231845323
			</div>
			<div class="col-sm-5" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;float: left;width: 41.66666666666667%;">
				<div class="pull-right" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;float: right!important;">
					<div class="invoice-sosmed" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-top: 15px;margin-right: 45px;">
						<a href="{{$settings['facebook']}}"><img src="http://projects.dextrovert.com/tokita/public/images/icon/facebook24.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;"></a>
						<a href="{{$settings['twitter']}}"><img src="http://projects.dextrovert.com/tokita/public/images/icon/twitter-button.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;"></a>
						<a href="{{$settings['google']}}"><img src="http://projects.dextrovert.com/tokita/public/images/icon/google2.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;"></a>
						<a href="{{$settings['instagram']}}"><img src="http://projects.dextrovert.com/tokita/public/images/icon/youtube18.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

</html>