<div class="align-center">
	<div class="banner-ad">
	@foreach($banners as $banner)
		@if($banner->url != "")
            <a href="{{$banner->url}}">
        @endif
		<img src="{{ asset('images/sliders/'.$banner->image) }}" alt="" />
		@if($banner->url != "")
            </a>
        @endif
	@endforeach
	</div>
</div>
