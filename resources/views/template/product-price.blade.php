<!-- diulang dari sini -->
<a href="" data-toggle="modal" data-target="#ModalDetailProduct">
    <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail productPanel">
             <div class="productImageWrapper" style="background:url({{ asset('images/products/'.$item->item_thumbnail) }}); background-size:cover; background-position: center;">
            </div> 
            <div class="productInfo">
                <div class="productName">
                    {{$item->item_name}}
                </div>
                <div class="productPrice">
                    Rp {{$item->item_price}}
                </div>
            </div>
        </div>
    </div>
</a>
