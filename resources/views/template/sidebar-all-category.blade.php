<div class="subitem1"><li class="dropdown "><span>SEMUA KATEGORI</span></div>
    <ul>
    @foreach($categories as $category)
        <li class="subitem2"><span class="miniicon"><img src="{{asset('images/icon/'.strtolower($category->category_name).'.png')}}"></span><a href="{{URL::to('/products/view/'.$category->id)}}">{{$category->category_name}}</a></li>
    @endforeach
    </ul>
</li>

