<div class="hidden-xs">
  <div class="subitem1"><li><a href="#">Saring Berdasarkan Harga</a></div>
  <div class="align-center">
  	<div id="slider-3" class="priceSlider"></div>
  </div>
  <div class="priceRange">
  	<!-- <label for="price">Price range:</label> -->
  	<input type="text" id="price-min" class="form-control form-inline price-min" style="border:0; color:#b9cd6d; font-weight:bold;"> -
  	<input type="text" id="price-max" class="price-max form-control" style="border:0; color:#b9cd6d; font-weight:bold;">
  </div>
  <div class="padding-left padding-right vertical-margin">
  	<button class="menuSlider_button btn buttonTokita fullWidth filterBtn">Filter</button>
  </div>
</div>

<div class="visible-xs">
  <div class="subitem1"><li><a href="#">Saring Berdasarkan Harga</a></div>
  <div class="align-center">
    <div id="slider-3-mobile" class="priceSlider"></div>
  </div>
  <div class="priceRange">
    <!-- <label for="price">Price range:</label> -->
    <input type="text" id="price-min-mobile" class="form-control form-inline price-min" style="border:0; color:#b9cd6d; font-weight:bold;"> -
    <input type="text" id="price-max-mobile" class="price-max form-control" style="border:0; color:#b9cd6d; font-weight:bold;">
  </div>
  <div class="padding-left padding-right vertical-margin">
    <button class="menuSlider_button btn buttonTokita fullWidth filterBtn">Filter</button>
  </div>
</div>

@section('scripts')
<script>
         $(function() {
         	var defaultMin = {{$lowest}};
         	var defaultMax = {{$highest}};
            $( ".priceSlider" ).slider({
               range:true,
               min: defaultMin,
               max: defaultMax,
               values: [
               @if(isset($bottom_range)) 
               {{$bottom_range}},
               @else
               defaultMin,
               @endif
               @if(isset($top_range))
               {{$top_range}}
               @else
               defaultMax
               @endif
               ],
               slide: function( event, ui ) {
                  $( ".price-min" ).val(ui.values[ 0 ]);
                  $( ".price-max" ).val(ui.values[ 1 ] );
               }
           });
         $( "#price" ).val( "Rp " + $( ".priceSlider" ).slider( "values", 0 ) +
            " - Rp " + $( ".priceSlider" ).slider( "values", 1 ) );


         	$( ".price-min" ).val($( ".priceSlider" ).slider( "values", 0 ));
	        $( ".price-max" ).val($( ".priceSlider" ).slider( "values", 1 ));
          $('.filterBtn').click(function(){
              window.location = "{{Request::url()}}"+"?bottom_range="+$('.price-min').val()+"&top_range="+$('.price-max').val();
            
          });
         });
</script>

@endsection