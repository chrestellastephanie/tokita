<!-- diulang dari sini -->
@if(isset($loggedin))
<a href="" class="discounted_product" data-toggle="modal" data-target="#ModalDetailProduct_{{$item->id}}_{{$item->type}}" data-customer_id="{{$loggedin->id}}" data-id="{{$item->id}}" data-displayed_price="Rp {{number_format($item->discounted_item_price, 0, ',', '.')}}" data-thumbnail="{{ asset('images/products/'.$item->item_thumbnail) }}" data-name="{{ucwords(strtolower($item->item_name))}}" data-description="{{$item->description}}">
@else
<a href="" class="discounted_product" data-toggle="modal" data-target="#ModalDetailProduct_{{$item->id}}_{{$item->type}}" data-id="{{$item->id}}" data-displayed_price="Rp {{number_format($item->discounted_item_price, 0, ',', '.')}}" data-thumbnail="{{ asset('images/products/'.$item->item_thumbnail) }}" data-name="{{ucwords(strtolower($item->item_name))}}" data-description="{{$item->description}}">
@endif
    <div class="col-xs-6 col-sm-3 col-md-3">
        <div class="thumbnail productPanel">
            @if($item->discounted_item_price < $item->item_price)
                <div class="discount">
                    <p>{{floor(($item->item_price-$item->discounted_item_price)*100/$item->item_price)}}%</p>
                </div>
            @endif
            @if($item->item_thumbnail == "")
            <div class="productImageWrapper" style="background:url('http://placehold.it/200x200?text=No+image'); background-size:cover; background-position: center;">
            @else
            <div class="productImageWrapper" style="background:url('{{ asset('images/products/'.$item->item_thumbnail) }}'); background-size:cover; background-position: center;">
            @endif
            </div>   
            <div class="productInfo">         
                <div class="productName">
                    @if(strlen($item->item_name) <= 30)
                        {{ucwords(strtolower($item->item_name))}}
                    @else
                        {{substr(ucwords(strtolower($item->item_name)), 0, 30)}}...
                    @endif
                </div>
                @if($item->discounted_item_price < $item->item_price)
                    <div class="priceAsli display-inline">
                        Rp {{number_format($item->item_price, 0, ",", ".")}}
                    </div>
                    <div class="pricePromo productPrice display-inline">
                        Rp {{number_format($item->discounted_item_price, 0, ",", ".")}}
                    </div>
                @else
                    <div class="productPrice">
                        Rp {{number_format($item->item_price, 0, ",", ".")}}
                    </div>
                @endif
            </div>
            @if($item->stock > 0)
                <div class="stock-status">
                    Stok tersedia
                </div>
            @else
                <div class="stock-status stock-status--outofstock">
                    Stok habis
                </div>
            @endif
        </div>
    </div>
</a>
<div id="ModalDetailProduct_{{$item->id}}_{{$item->type}}" class="modal fade detailProductModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="closeModalButton">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        </div>
        <div class="row detailProductWrapper">
            <div class="col-md-12 detailProductName vertical-margin">
            </div>
            <div class="col-sm-5 detailProductImage">
            </div>
            <div class="col-sm-7">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="detailProductPrice"></div>
                    </div>
                    @if($item->stock == 0)
                    <div class="col-sm-6 out-of-stock-alert">
                        Maaf, Stok Habis
                    </div>
                    @endif
                </div>
                <div class="detailProductDesc">
                    -
                </div>
                <form action="" method="post" id="detailForm_{{$item->id}}_{{$item->type}}">
                    <div class="row">
                        <div class="col-xs-6">
                            @if($item->stock > 0)
                            <div class="detailAmount">
                                <span id="arrowButtonDown_{{$item->id}}_{{$item->type}}" class="arrowButton arrowButtonDown"><button>-</button></span>
                                <input id="quantity_{{$item->id}}_{{$item->type}}" name="quantity" class="form-inline amount" type="number" value="1" readonly/>
                                <input id="item_id" name="item_id" value="{{$item->id}}" type="hidden"/>
                                @if(isset($loggedin))
                                <input id="customer_id" name="customer_id" type="hidden" value="{{$loggedin->id}}"/>
                                @endif
                                <input type="hidden" id="token_{{$item->id}}_{{$item->type}}" name="_token" value="{{csrf_token()}}">
                                <span id="arrowButtonUp_{{$item->id}}_{{$item->type}}" class="arrowButton arrowButtonUp"><button>+</button></span>
                            </div>
                            @endif
                        </div>
                        <div class="col-xs-6">
                            @if($item->stock > 0)
                            <button class="detailBuyButton btn-default buttonYellow buyButton_{{$item->id}}_{{$item->type}}" type="submit">
                                BELI
                            </button>
                            @endif
                            @if(isset($loggedin))
                                @if(!$item->isFav)
                                <button class="btn-default detailFavButton favButton_{{$item->id}}_{{$item->type}}">
                                    <img src="{{ asset('images/icon/favorit_2.png') }}" alt=""/>
                                    FAVORIT
                                </button>
                                @else
                                <button class="btn-default detailFavButton unfavButton_{{$item->id}}_{{$item->type}}">
                                    <img src="{{ asset('images/icon/unfavorit.png') }}" alt=""/>
                                    HAPUS DARI FAVORIT
                                </button>
                                @endif
                            @else
                                <button class="btn-default detailFavButton favButton_{{$item->id}}_{{$item->type}}">
                                    <img src="{{ asset('images/icon/favorit_2.png') }}" alt=""/>
                                    FAVORIT
                                </button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>          
    </div>
  </div>
</div>
@if(isset($loggedin))
<script type="text/javascript">
    $('.buyButton_{{$item->id}}_{{$item->type}}').click(function(e){
        e.preventDefault();
        $('#detailForm_{{$item->id}}_{{$item->type}}').attr('action',"{{URL::to('addToCart?ref='.Request::url())}}");
        $('#detailForm_{{$item->id}}_{{$item->type}}').submit();
    });
</script>
@else
<script type="text/javascript">
    $('.buyButton_{{$item->id}}_{{$item->type}}').click(function(e){
        e.preventDefault();
        $('#ModalDetailProduct_{{$item->id}}_{{$item->type}}').modal('hide');
        $('#ModalLogin').modal('show');
    });
</script>
@endif

@if(isset($loggedin))
<script type="text/javascript">
    $('.favButton_{{$item->id}}_{{$item->type}}').click(function(e){
        e.preventDefault();
        $('#detailForm_{{$item->id}}_{{$item->type}}').attr('action',"{{URL::to('addToFav?ref='.Request::url())}}");
        $('#detailForm_{{$item->id}}_{{$item->type}}').submit();
    });
</script>
@else
<script type="text/javascript">
    $('.favButton_{{$item->id}}_{{$item->type}}').click(function(e){
        e.preventDefault();
        $('#ModalDetailProduct').modal('hide');
        $('#ModalLogin').modal('show');
    });
</script>
@endif

@if(isset($loggedin))
<script type="text/javascript">
    $('.unfavButton_{{$item->id}}_{{$item->type}}').click(function(e){
        e.preventDefault();
        $('#detailForm_{{$item->id}}_{{$item->type}}').attr('action',"{{URL::to('removeFav?ref='.Request::url())}}");
        $('#detailForm_{{$item->id}}_{{$item->type}}').submit();
    });
</script>
@else
<script type="text/javascript">
    $('.unfavButton_{{$item->id}}_{{$item->type}}').click(function(e){
        e.preventDefault();
        $('#ModalDetailProduct').modal('hide');
        $('#ModalLogin').modal('show');
    });
</script>
@endif

<script type="text/javascript">
    (function ($) {
      $('#arrowButtonUp_{{$item->id}}_{{$item->type}}').on('click', function(e) {
        e.preventDefault();
        if(parseInt($('#quantity_{{$item->id}}_{{$item->type}}').val(), 10)<{{$item->stock}}){
            $('#quantity_{{$item->id}}_{{$item->type}}').val( parseInt($('#quantity_{{$item->id}}_{{$item->type}}').val(), 10) + 1);
        }
        $('#token_{{$item->id}}_{{$item->type}}').val("{{csrf_token()}}");
      });
      $('#arrowButtonDown_{{$item->id}}_{{$item->type}}').on('click', function(e) {
        e.preventDefault();
        if(parseInt($('#quantity_{{$item->id}}_{{$item->type}}').val(), 10)>0){
            $('#quantity_{{$item->id}}_{{$item->type}}').val( parseInt($('#quantity_{{$item->id}}_{{$item->type}}').val(), 10) - 1);
        }
        $('#token_{{$item->id}}_{{$item->type}}').val("{{csrf_token()}}");
      });
    })(jQuery);
</script>

<script type="text/javascript">
    // if($(window).width()<768){
    //     // alert('a');
    //     // alert($('.productName').text().substring(1,4));
    //     $('.productName').each(function(){
    //         // $(this).text($(this).text().substring(1,4));
    //         $(this).text($(this).text()+"a");
    //     });
    // };
</script>