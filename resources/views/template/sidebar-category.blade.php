<div class="categoryList">
	<div class="subitem1 subitem--categoryName"><a>{{$current_cat->category_name}}</a></div>
	    <ul>
	    @foreach($subcategories as $subcat)
	        <li class="subitem2"><a href="{{Request::url().'/'.$subcat->id}}">{{$subcat->subcategory_name}}</a></li>
	    @endforeach
	    </ul>
	</li>
</div>