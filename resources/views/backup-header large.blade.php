<div class="row">
				<div class="col-md-3" style="z-index: 3;">
					<h1><a href="<?=$baseurl;?>/"><img src="{{ asset('images/logo/logo.png') }}" alt="" class="img-responsive" /></a></h1>
				</div>
				<div class="col-md-4" align="center">
					<div class="form-search search-only navTopMargin">
						<div class="input-group">
						  <input type="text" class="form-control" placeholder="Search for...">
						  <span class="input-group-btn">
							<button class="btn buttonTokita" type="button">Search</button>
						  </span>
						</div><!-- /input-group -->
					</div>
				</div> 
				<div class="col-md-2 hidden-sm hidden-xs" align="center">
				   <div class="smallIcon" align="right">
						<a class="cart" href="<?=$baseurl;?>/cart" ><img src="{{ asset('images/icon/cart-red.png') }}" alt="" class="img-responsive"/></a>
						<a href="" ><img src="{{ asset('images/icon/cart-red.png') }}" alt="" class="img-responsive"/></a>
					</div>
				</div> 
				<div class="col-md-3 hidden-sm hidden-xs smallIcon" align="center">
				   @if(isset($loggedin))
					   <div class="dropdown" style="text-align:right; ">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
						    {{$loggedin->customer_name}}
						    <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
						    <li role="presentation"><a href="<?=$baseurl;?>/edit-akun">Edit Akun</a></li>
						    <li role="presentation"><a href="" data-toggle="modal" data-target="#ModalChangePassword">Ubah Password</a></li>
						    <li role="presentation"><a href="" data-toggle="modal" data-target="#ModalPoint">Point</a></li>
						    <li role="presentation"><a href="" data-toggle="modal" data-target="#ModalGift">Hadiah</a></li>
						    <li role="presentation"><a href="" data-toggle="modal" data-target="#ModalCoupon">Kupon</a></li>
						    <li role="presentation"><a href="<?=$baseurl;?>/logout">Logout</a></li>
						  </ul>
						</div>
					@else
						<div class="align-right">
							<a href="" data-toggle="modal" data-target="#ModalLogin"><button class="btn btn-default buttonLogin" type="button">LOGIN</button></a>
					   		<a href="<?=$baseurl;?>/register"><button class="btn btn-default buttonRegister" type="button">DAFTAR</button></a>
					   	</div>
					@endif
				</div> 
			  </div>