<!DOCTYPE html><html lang="en">
<?php $baseurl = URL::to('/');?>
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<head>
	<title>Tokita</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script> 
</head>

<body>
<div class="container">
	<div class="invoice-header">
		<div class="row">
			<div class="col-sm-12">
				<div class="pull-left">
					<a><img src="{{ asset('images/logo/tagline_navbar.png') }}" alt="" class="display-block " /></a>
					<a><img src="{{ asset('images/logo/logo.png') }}" alt="" class="display-block invoice-logo" /></a>
				</div>
				<div class="pull-right">
					<div class="invoice-menu">
						<ul>
							<li>Lanjutkan Belanja</li>
							<li>FAQ</li>
							<li>Tentang Kami</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="invoice-body">
		<div class="row">
			<div class="col-sm-12">
				Dear Bapak/Ibu,
				<br><br>
				Salah satu sahabat Tokita sedang dalam perjalanan mengirimkan pesanan Anda, yaitu: <br><br>
			</div>
		</div>
		<div class="row margin-left">
			<div class="col-sm-12">
				<div class="invoice-list">
					<div class="row invoice-table-heading">
						<div class="col-sm-8">
							Nama produk
						</div>
						<div class="col-sm-1">
							Qty
						</div>
						<div class="col-sm-3">
							Subtotal
						</div>
					</div>
					<div class="row">
						<div class="col-sm-8">
							Bertolli Extra Virgin Olive Botol 250ml
						</div>
						<div class="col-sm-1">
							1
						</div>
						<div class="col-sm-3">
							Rp 36.000
						</div>
					</div>
					<div class="row">
						<div class="col-sm-8">
							Munik Bumbu Sambal Goreng Kentang Udang
						</div>
						<div class="col-sm-1">
							10
						</div>
						<div class="col-sm-3">
							Rp 20.000
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row margin-left">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-9">
						<b>Kupon</b>
					</div>
					<div class="col-sm-3">
						<b>Rp 3.600.000</b>
					</div>
				</div>
			</div>			
		</div>

		<div class="row margin-left">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1 col-sm-offset-8">
						<b>Total</b>
					</div>
					<div class="col-sm-3">
						<b>Rp 3.600.000</b>
					</div>
				</div>
			</div>			
		</div>
		<br>
		Silakan siapkan uang tunai untuk pembayaran <br><br>
		Untuk bantuan lebih lanjut, silakan hubungi 022 87788751 / 082231845323 atau email Customer Service kami di admin@tokita.com
	</div>
	<div class="invoice-footer">
		<div class="row">
			<div class="col-sm-3">
				<div class="invoice-footer-logo">
					<img src="{{ asset('images/logo/logo_min_nobg.png') }}"/>
				</div>
			</div>
			<div class="col-sm-4 footerContent">
				Villa Bukit Mas Blok Baru no. 4 <br>
				Bojong Koneng, Cikutra, Bandung <br>
				022 87788751 / 082231845323
			</div>
			<div class="col-sm-5">
				<div class="pull-right">
					<div class="invoice-sosmed">
						<img src="{{ asset('images/icon/facebook24.png') }}"/>
						<img src="{{ asset('images/icon/twitter-button.png') }}"/>
						<img src="{{ asset('images/icon/google2.png') }}"/>
						<img src="{{ asset('images/icon/youtube18.png') }}"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

</html>