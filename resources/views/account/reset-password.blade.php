@extends('main')

@section('left-navbar')
    @include('template.sidebar-all-category')
@endsection

@section('pagetitle')
	<div class="pageTitle">
		Reset Password	
	</div>	
@endsection

@section('content')
	<div class="col-md-12 vertical-margin">
	<form class="col-md-12 align-left vertical-margin" method="post" action="changePassword">
		<table class="col-md-12">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="user_id" value="{{$user_id}}">
			<tr>
				<td class="col-md-3">Password</td>
				<td class="col-md-9"><input class="form-control" name="password" type="password" id="password" onkeyup="checkPassword()"></td>
				<input type="hidden" id="passwordValid" value="false">
			</tr>
			<tr class="error" id="errorPassword" style="display: none;">
				<td class="col-md-3"></td><td class="col-md-9"><div class="wrongInput">Panjang password harus minimal 8 karakter.</div></td>
			</tr>
			<tr>
				<td class="col-md-3">Konfirmasi Password</td>
				<td class="col-md-9"><input class="form-control" name="confirm_password" type="password" id="confirm_password" onkeyup="checkConfirmPassword()"></td>
				<input type="hidden" id="confirmPasswordValid" value="false">
			</tr>
			<tr class="error" id="errorConfirmPassword" style="display: none;">
				<td class="col-md-3"></td>
				<td class="col-md-9">
					<div class="wrongInput">
						Password tidak cocok
					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="col-md-6">
					<button class="pull-right btn buttonTokita display-inline-block" type="submit" id="regButton">kirim</button>
				</td>
			</tr>
		</table>
	</form>
	</div>
@endsection


@section('scripts')
	<script type="text/javascript">
		
		function checkPassword(){
			var content = document.getElementById("password").value;
			if(content.length < 8){
				$( '#errorPassword' ).show();
				document.getElementById("passwordValid").value = "false";
			} else{
				$( '#errorPassword' ).hide();
				document.getElementById("passwordValid").value = "true";
			}
			checkConfirmPassword();
			invalidateButton();
		}

		function checkConfirmPassword(){
			var content = document.getElementById("password").value;
			var content2 = document.getElementById("confirm_password").value;
			if(content != content2){
				$( '#errorConfirmPassword' ).show();
				document.getElementById("confirmPasswordValid").value = "false";
			} else{
				$( '#errorConfirmPassword' ).hide();
				document.getElementById("confirmPasswordValid").value = "true";
			}
			invalidateButton();
		}

		function invalidateButton(){
			var isValid = (document.getElementById("passwordValid").value == "true")&&
						  (document.getElementById("confirmPasswordValid").value == "true");
			if(isValid){
				$('#regButton').removeAttr('disabled');
			}
			else{
				$('#regButton').attr('disabled','disabled');
			}
		}

		$(document).ready(function() {
		     $('#regButton').attr('disabled','disabled');
		 });
	</script>
@endsection