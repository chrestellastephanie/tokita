@extends('main')

@section('left-navbar')
	<div class="menu">
		@include('template.sidebar-event')
		@include('template.sidebar-all-category')
	</div>
@endsection

@section('pagetitle')
	<div class="pageTitleSmall">
		<div class=" sectionTitle-title">
			<span>
	            Edit Akun
	        </span>
		</div>
	</div>	
@endsection

@section('content')
<div class="form-top-margin">
	<form method="post" action="updateAccount">
		<input type="hidden" name="_token" value="{{csrf_token()}}" id="_token">
		<input type="hidden" name="id_customer" value="{{$loggedin->id}}" id="id_customer">
		<div class="form-group">
			<div class="row vertical-margin">
				<div class="col-md-3"><label>Nama Lengkap</label></div>
				<div class="col-md-9"><input class="form-control" id="name" name="name" type="text" value="{{$loggedin->customer_name}}" onkeyup="checkName()"></div>
	         	<input type="hidden" id="nameValid" value="true">
	         </div>
		</div>
		<div class="form-group">
			<div class="row vertical-margin">
				<div class="col-md-3"><label>Email</label></div>
				<div class="col-md-9"><input class="form-control" name="email" type="email" id="email" value="{{$loggedin->customer_email}}" onkeyup="checkEmail()"></div>
	         	<input type="hidden" id="emailValid" value="true">
	         </div>
		</div>
		<div class="form-group error" id="errorEmail" style="display: none;">
			<div class="row vertical-margin">
				<div class="col-md-9 col-md-offset-3 wrongInput">Email sudah terdaftar</div>
	         </div>
		</div>
		<div class="form-group">
			<div class="row vertical-margin">
				<div class="col-md-3"><label>Handphone</label></div>
				<div class="col-md-9"><input class="form-control" type="text" name="phone" id="phone" value="{{$loggedin->customer_phone}}" onkeyup="checkPhone()"></div>
				<input type="hidden" id="phoneValid" value="true">
	         </div>
		</div>
		<div class="form-group">
			<div class="row vertical-margin">
				<div class="col-md-3"><label>Alamat</label></div>
				<div class="col-md-9"><input class="form-control" type="text" name="address" id="address" value="{{$loggedin->customer_address}}" onkeyup="checkAddress()"></div>
				<input type="hidden" id="addressValid" value="true">
	         </div>
		</div>
		<div class="vertical-margin">
			<input type="submit" id="regButton" value="Simpan" class="pull-right btn buttonTokita">
		</div>
	</form>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		function checkEmail(){
			var content = document.getElementById("email").value;
			$.ajax({
			  url: 'checkEmail',     
			  type: 'post', // performing a POST request
			  data : {
			    email : content, // will be accessible in $_POST['data1']
			    _token: document.getElementById("_token").value
			  },
			  dataType: 'json',
			  // error: function(data)         
			  // {
			  //   alert(JSON.stringify(data, null, 2));
			  // },
			  success: function(data)         
			  {
			    if(data == '0'){
			    	$( '#errorEmail' ).show();
			    	document.getElementById("emailValid").value = "false";
			    	
			    }
			    else{
			    	$( '#errorEmail' ).hide();
			    	document.getElementById("emailValid").value = "true";
			    }
			  } 
			});
			invalidateButton();
		}

		function checkName(){
			var content = document.getElementById("name").value;
			if(content.length > 0){
				document.getElementById("nameValid").value = "true";
			} else{
				document.getElementById("nameValid").value = "false";
			}
			invalidateButton();
		}

		function checkPhone(){
			var content = document.getElementById("phone").value;
			if(content.length > 0){
				document.getElementById("phoneValid").value = "true";
			} else{
				document.getElementById("phoneValid").value = "false";
			}
			invalidateButton();
		}

		function checkAddress(){
			var content = document.getElementById("address").value;
			if(content.length > 0){
				document.getElementById("addressValid").value = "true";
			} else{
				document.getElementById("addressValid").value = "false";
			}
			invalidateButton();
		}

		function checkPassword(){
			var content = document.getElementById("password").value;
			if(content.length < 8){
				$( '#errorPassword' ).show();
				document.getElementById("passwordValid").value = "false";
			} else{
				$( '#errorPassword' ).hide();
				document.getElementById("passwordValid").value = "true";
			}
			checkConfirmPassword();
			invalidateButton();
		}

		function checkConfirmPassword(){
			var content = document.getElementById("password").value;
			var content2 = document.getElementById("confirm_password").value;
			if(content != content2){
				$( '#errorConfirmPassword' ).show();
				document.getElementById("confirmPasswordValid").value = "false";
			} else{
				$( '#errorConfirmPassword' ).hide();
				document.getElementById("confirmPasswordValid").value = "true";
			}
			invalidateButton();
		}

		function invalidateButton(){
			var isValid = (document.getElementById("nameValid").value == "true")&&
						  (document.getElementById("emailValid").value == "true")&&
						  (document.getElementById("phoneValid").value == "true")&&
						  (document.getElementById("addressValid").value == "true");
			if(isValid){
				$('#regButton').removeAttr('disabled');
			}
			else{
				$('#regButton').attr('disabled','disabled');
			}
		}
	</script>
@endsection

