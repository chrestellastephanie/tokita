@extends('main')

@section('left-navbar')
	<div class="menu">
		@include('template.sidebar-event')
		@include('template.sidebar-all-category')
	</div>
@endsection

@section('pagetitle')
	<div class="pageTitleSmall">
		<div class=" sectionTitle-title">
		</div>
	</div>	
@endsection

@section('content')
	<div class="sentWrapper">
		<img src="{{ asset('images/logo/logo_min.png') }}"/>
		<div class="sentWrapperTitle">
			Halaman Tidak Ditemukan
		</div>
		<div class="sentWrapperContent">
			Mohon maaf, halaman yang Anda cari tidak dapat ditemukan. Silakan periksa kembali URL yang Anda tulis. Jika Anda merasa halaman tersebut seharusnya ada, kontak kami di 022 87788751 / 082231845323.
		</div>
	</div>
@endsection