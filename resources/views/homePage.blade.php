@extends('main')
@section('slider')
    <div class="carouselWrapper">
        <div class="carouselNavigation">
            <div class="carouselPrev">
                <a href="" class="prev"><img src="{{ asset('images/icon/carousel-prev.png') }}"/></a>
            </div>
            <div class="carouselNext">
                <a href="" class="next"><img src="{{ asset('images/icon/carousel-next.png') }}"/></a>
            </div>
        </div> 
        <div id="carousel" class="owl-carousel carousel visible-xs">
        @foreach($sliders as $slide)
            @if($slide->url != "")
                <a href="{{$slide->url}}">
            @endif
            <img src="{{ asset('images/mobile_sliders/'. $slide->mobile_image) }}" />
            @if($slide->url != "")
                </a>
            @endif
        @endforeach
        </div>
        <div id="carousel" class="owl-carousel carousel hidden-xs"> 
        @foreach($sliders as $slide) 
            @if($slide->url != "")
                <a href="{{$slide->url}}">
            @endif
            <img src="{{ asset('images/sliders/'. $slide->image) }}" />
            @if($slide->url != "")
                </a>
            @endif
        @endforeach
        </div>
    </div>
@endsection
    
@section('left-navbar')
    <div class="menu">
        @include('template.sidebar-event')
        @include('template.sidebar-all-category')
        @include('template.sidebar-ad')
    </div>
@endsection

@section('content')
    <!-- start section  -->
    <div class="HPsection">
        <div class="sectionTitle">
            <div class="row">
                <div class="col-sm-6 sectionTitle-title">
                    <span>
                        <img src="{{ asset('images/icon/diskon.png') }}" alt=""/>
                        Diskon Hari Ini
                    </span>
                </div>
                <div class="col-sm-6 sectionTitle-seemore">
                    <a href="products/view">Lihat Seluruhnya >></a>
                </div>
            </div>
        </div>
        <div class="sectionProducts">
            <!-- diulang dari sini -->
            <div class="row">
                @foreach($itemsDiscount as $item)
                    @include('template.product-panel')
                @endforeach
            </div>
        </div>
    </div>
    <!-- end section -->
    <!-- start section  -->
    <div class="HPsection">
       <div class="sectionTitle">
            <div class="row">
                <div class="col-sm-6 sectionTitle-title">
                    <span>
                        <img src="{{ asset('images/icon/baru.png') }}" alt=""/>
                        Produk Baru
                    </span>
                </div>
                <div class="col-sm-6 sectionTitle-seemore">
                    <a href="products/view">Lihat Seluruhnya >></a>
                </div>
            </div>
        </div>
        <div class="sectionProducts">
            <!-- diulang dari sini -->
            <div class="row">
                @foreach($itemsNew as $item)
                    @include('template.product-panel')
                @endforeach
            </div>
        </div>
    </div>
    <!-- end section -->
    <!-- start section  -->
    <div class="HPsection">
        <div class="sectionTitle">
            <div class="row">
                <div class="col-sm-6 sectionTitle-title">
                    <span>
                        <img src="{{ asset('images/icon/populer.png') }}" alt=""/>
                        Produk Populer
                    </span>
                </div>
                <div class="col-sm-6 sectionTitle-seemore">
                    <a href="products/view">Lihat Seluruhnya >></a>
                </div>
            </div>
        </div>
        <div class="sectionProducts">
            <!-- diulang dari sini -->
            <div class="row">
                @foreach($itemsPopular as $item)
                    @include('template.product-panel')
                @endforeach
            </div>
        </div>
    </div>
    <!-- end section -->
    <!-- start section  -->
    @if(isset($loggedin))
    <div class="HPsection">
        <div class="sectionTitle">
            <div class="row">
                <div class="col-sm-6 sectionTitle-title">
                    <span>Baru Dilihat</span>
                </div>
                <div class="col-sm-6 sectionTitle-seemore">
                    <a href="products/view">Lihat Seluruhnya >></a>
                </div>
            </div>
        </div>
        <div class="sectionProducts">
            <!-- diulang dari sini -->
            <div class="row">
                @foreach($itemsRecent as $item)
                    @include('template.product-panel')
                @endforeach
            </div>
        </div>
    </div>
    @endif
    <!-- end section -->

@endsection

