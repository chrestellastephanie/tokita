<!DOCTYPE html><html lang="en">
<?php $baseurl = URL::to('/');?>
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
<head>
	<title>Tokita</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/owl.carousel.css') }}" rel="stylesheet">
	<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
	<link rel="shortcut icon" href="images/logo/icon.png">
	

	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script> 
	<script src="{{ asset('js/jquery.bootstrap-growl.min.js') }}"></script> 
	<script src="{{ asset('js/owl.carousel.js') }}"></script>
	@yield('scripts')
</head>
<body>
	<div class="menuSlider container">
		<div class="menuItems">
			<div class="menuItem">
				@if(isset($loggedin))
				<div class="display-inline-block">
					<div class="dropdown" style="text-align:right;">
					  	<div class="menuDashboard menuDashboard-mobile buttonAccountName headerButtonAccount fullWidth">
						  	<nav class="display-inline">
						  		<ul>
						  			<li><button class="headerButtonAccount headerButtonAccount-mobile buttonAccountName headerButtonAccount-loggedin buttonNamaAkun-mobile btn">{{$loggedin->customer_name}}
						  					<span class="pull-right"><img src="{{ asset('images/icon/down-red.png') }}"/></span>
						  				</button>
						  				<ul>
						  					<a href="<?=$baseurl;?>/edit-akun">
						  						<li>
						  							Edit Akun
						  						</li>
						  					</a>
						  					<a href="" data-toggle="modal" data-target="#ModalChangePassword">	
						  						<li>
						  							Ubah Password
						  						</li>
						  					</a>
						  					<a href="" data-toggle="modal" data-target="#ModalPoint">
							  					<li>
							  						Poin
							  					</li>
							  				</a>
						  					<a href="" data-toggle="modal" data-target="#ModalGift">
						  						<li>
						  							Hadiah
						  						</li>
						  					</a>
						  					<a href="" data-toggle="modal" data-target="#ModalCoupon">
						  						<li>
							  						Kupon
							  					</li>
						  					</a>
						  					<a href="<?=$baseurl;?>/logout">
							  					<li>
							  						Logout
							  					</li>
							  				</a>
						  				</ul>
						  			</li>
						  		</ul>
						  	</nav>
					  	</div>
					</div>
				</div>
				@else
				<div class="display-inline-block">
				    <div class="dropdown" style="text-align:right;">
					  	<div class="menuDashboard menuDashboard-mobile buttonAccountName headerButtonAccount fullWidth">
						  	<nav class="display-inline">
						  		<ul>
						  			<li><button class="headerButtonAccount headerButtonAccount-mobile buttonAccountName headerButtonAccount-loggedin buttonNamaAkun-mobile btn">ATUR AKUN
						  					<span class="pull-right"><img src="{{ asset('images/icon/down-red.png') }}"/></span>
						  				</button>
						  				<ul>
						  					<a href="" data-toggle="modal" data-target="#ModalLogin">
						  						<li>
						  							Login
						  						</li>
						  					</a>
						  					<a href="<?=$baseurl;?>/register">
						  						<li>
						  							Daftar
						  						</li>
						  					</a>
						  				</ul>
						  			</li>
						  		</ul>
						  	</nav>
					  	</div>
					</div>
				</div>
				@endif
			</div>
			<div class="menuItem">
				<ul class="menuItem" style="margin-top: 0px; ">
			        <div class="subitem1"><li><a href="#">SEMUA EVENT</a></div>
			            <ul>
			                <li class="subitem2"><a href="#"><span class="miniicon"><img src="{{ asset('images/icon/diskon.png')}}"></span>Diskon hari ini</a></li>
			                <li class="subitem2"><a href="#"><span class="miniicon"><img src="{{ asset('images/icon/baru.png')}}"></span>Produk baru</a></li>
			                <li class="subitem2"><a href="#"><span class="miniicon"><img src="{{ asset('images/icon/populer.png')}}"></span>Produk populer</a></li>
			            </ul>
			        </li>
			        <div class="subitem1"><li class="dropdown "><a href="#">SEMUA KATEGORI</a></div>
			            <ul>
			        		@foreach($categories as $category)
       						  <li class="subitem2"><span class="miniicon"><img src="{{asset('images/icon/'.strtolower($category->category_name).'.png')}}"></span><a href="{{URL::to('/products/view/'.$category->id)}}">{{$category->category_name}}</a></li>
						    @endforeach
			            </ul>
			        </li>
			    </ul>
			</div>
			<div class="footerMenuSlider">
				<a href="{{URL::to('/about')}}">Tentang Tokita</a>
				<a href="{{URL::to('/faq')}}">FAQ</a>
			</div>
		</div>
	</div>
	<!-- header large screen -->
	<div class="header hidden-xs hidden-sm ">
		<div class="container">
	  		<!-- large-screen -->
	  		<div class="row headerMenu">
	  			<div class="col-sm-3">
	  				<div class="headerLogo">
						<a href="<?=$baseurl;?>/"><img src="{{ asset('images/logo/tagline_navbar.png') }}" alt="" class="display-block logoTagline" /></a>
						<a href="<?=$baseurl;?>/"><img src="{{ asset('images/logo/logo.png') }}" alt="" class="display-block logoBasic disappear-on-shrink" /></a>
					</div>
					<div class="headerLogo--shrink">
						<a href="<?=$baseurl;?>/"><img src="{{ asset('images/logo/logo_min.png') }}" alt="" class="display-block logoTagline" /></a>
					</div>
	  			</div>
	  			<div class="col-sm-6">
	  				<div class="headerSearch">
						<li>
							<div class="form-search search-only navTopMargin">
							<form name="searchForm" method="post" action="{{URL::to('/search/')}}">
								<div class="input-group">
									<input type="hidden" name="_token" value="{{csrf_token()}}">
								  <input type="text" class="inputSearch form-control" onkeyup="updateAction();" id="query" name="searchQuery" placeholder="Cari produk atau brand disini">
								</div>
								<script>
									function updateAction(){
										var elm = document.getElementById("query");
										document.searchForm.action = "{{URL::to('/search/')}}"+'/'+elm.value;
									}
								</script>
							</form>
							</div>
						</li>
					</div>
	  			</div>
	  			<div class="col-sm-3 align-right">
	  				<div class="headerRight">
	  					<div class="headerButtonIcon fav"><a href="<?=$baseurl;?>/user/favorit"><img src="{{ asset('images/icon/favorit_3.png') }}" alt="" /></a></div>
						<div class="headerButtonIcon cart">
							@if(isset($count_cart_items))
								@if($count_cart_items > 0)
									<div class="cartNotif">{{$count_cart_items}}</div>
								@endif
							@endif
							<a href="<?=$baseurl;?>/cart"><img src="{{ asset('images/icon/keranjang_3.png') }}" alt="" /></a>
						</div>
							
						@if(!isset($loggedin))
						    <div class="headerButtonAccount"><a href="" data-toggle="modal" data-target="#ModalLogin"><button class="btn btn-default buttonLogin buttonLogin-radius" type="button">LOGIN</button></a></div>
							<div class="headerButtonAccount"><a href="<?=$baseurl;?>/register"><button class="btn btn-default buttonRegister" type="button">DAFTAR</button></a></div>
						<!-- after login -->
						@else
							<div class="display-inline-block">
							    <div class="headerButtonAccount " style="text-align:right;">
								  	<div class="menuDashboard buttonAccountName">
									  	<nav class="display-inline">
									  		<ul>
									  			<li><button class="headerButtonAccount buttonAccountName headerButtonAccount-loggedin buttonNamaAkun-desktop btn">
									  			@if(strlen($loggedin->customer_name) > 10)
										  			{{substr($loggedin->customer_name, 0, 10)}}...
									  			@else
										  			{{$loggedin->customer_name}}
									  			@endif
									  					<span class="pull-right"><img src="{{ asset('images/icon/down-red.png') }}"/></span>
									  				</button>
									  				<ul>
									  					<a href="<?=$baseurl;?>/edit-akun">
									  						<li>
									  							Edit Akun
									  						</li>
									  					</a>
									  					<a href="" data-toggle="modal" data-target="#ModalChangePassword">	
									  						<li>
									  							Ubah Password
									  						</li>
									  					</a>
									  					<a href="" data-toggle="modal" data-target="#ModalPoint">
										  					<li>
										  						Poin
										  					</li>
										  				</a>
									  					<a href="" data-toggle="modal" data-target="#ModalGift">
									  						<li>
									  							Hadiah
									  						</li>
									  					</a>
									  					<a href="" data-toggle="modal" data-target="#ModalCoupon">
									  						<li>
										  						Kupon
										  					</li>
									  					</a>
									  					<a href="<?=$baseurl;?>/logout">
										  					<li>
										  						Logout
										  					</li>
										  				</a>
									  				</ul>
									  			</li>
									  		</ul>
									  	</nav>
								  	</div>
								</div>
							</div>
						@endif
							
	  				</div>					
	  			</div>	
			</div>
		</div>
	  <!-- insert header small screen -->
	</div>

	<!-- header mobile screen -->
	<div class="col-md-12 visible-xs visible-sm">
		<div class="headersmall container">
			<div class="headersmall_element">
				<div class="row">
					<div class="col-xs-5">
						<a href="" class="menuicon"><img src="{{ asset('images/icon/menu.png')}}"></a> 
						<a href="<?=$baseurl;?>/"><img class="logoMobile" src="{{ asset('images/logo/logo.png')}}"></a>
					</div>
					<div class="col-xs-7 align-right">
						<div class="headersmall_element--right">
							<div class="headerButtonIcon headerButtonIcon-mobile searchbuttonsmall"><a href=""><img src="{{ asset('images/icon/search.png') }}" alt="" /></a></div>
							<div class="headerButtonIcon headerButtonIcon-mobile fav"><a href="<?=$baseurl;?>/user/favorit"><img src="{{ asset('images/icon/favorit_3.png') }}" alt="" /></a></div>
							<div class="headerButtonIcon headerButtonIcon-mobile cart">
							@if(isset($count_cart_items))
								@if($count_cart_items > 0)
									<div class="cartNotif">{{$count_cart_items}}</div>
								@endif
							@endif
							<a href="<?=$baseurl;?>/cart"><img src="{{ asset('images/icon/keranjang_3.png') }}" alt="" /></a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="headerSmallSearchWrapper">
			<form name="searchFormMobile" method="post" action="{{URL::to('/search/')}}">
				<div class="row">
					<div class="col-xs-10">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="text" id="queryMobile" class="form-control" onkeyup="updateActionMobile();" name="searchQuery" placeholder="Cari produk atau brand di sini">
					</div>
					<div class="col-xs-2 noPadding">
						<button class="button btn-default buttonTokita buttonMedium"><img src="{{ asset('images/icon/search_2.png') }}" alt="" /></button>
					</div>
				</div>
				<script>
					function updateActionMobile(){
						var elm = document.getElementById("queryMobile");
						document.searchFormMobile.action = "{{URL::to('/search/')}}"+'/'+elm.value;
					}
				</script>
			</form>
		</div>	
	</div>	
	<div class="bodywrapper">		
		<div class="contentwrapper">
			<div class="overlay"></div>
			@yield('slider')			
			<div class="container">
				<div class="margin-top--normal">
					<div class="row">
						<div class="col-md-3">
							@yield('left-navbar')
							<!-- @include('template.sidebar-ad') -->
						</div>
						<div class="col-md-9">
							@yield('pagetitle')
							@yield('content')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>       
	
<!-- modals -->
	<div id="ModalComingSoon" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-large">
		<div class="modal-content openingsoonContent">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-6 align-center">
					<div class="align-center">
						<img src="{{ asset('images/logo/logo_min.png')}}" />
					</div>
					<div class="modal-close">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					</div>
					<div class="align-center">
						<div class="openingsoonTitle">
							<span><b>AKAN SEGERA <br> BEROPERASI</b></span>
						</div>
					</div>
					<div class="align-center text">
						<i>Mohon maaf, untuk sekarang kami belum beroperasi secara online.</i> <br>
						<i>Tapi Anda dapat melihat website kami secara normal walaupun tidak dapat melakukan transaksi apapun.</i> <br>
						<i>Hubungi 022 87788751 atau 082231845323 untuk informasi lebih lanjut</i>
					</div>
				</div>
			</div>
					
		</div>
	  </div>
	</div>


	<div id="ModalLogin" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modalLogin modal-small">
		<div class="modal-content">
				<div class="align-center">
					<img src="{{ asset('images/logo/logo_min.png')}}" />
				</div>
				<div class="modal-close">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				</div>
				<form action="{{URL::to('/login')}}" method="POST">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="modal-body modalLogin-body">
							<div class="form-group">
								<input type="text" name="email" class="form-control" placeholder="Email">
							</div>
							<div class="form-group">
								<input type="password" name="password" class="form-control" placeholder="Password">
							</div>
								<input type="checkbox">&nbsp;Ingat saya 
								<input type="submit" class="btn buttonTokita buttonLogin pull-right" value="LOGIN">
							<div class="vertical-margin">
								<a href="" data-dismiss="modal" data-toggle="modal" data-target="#ModalForgetPassword" class="pull-right fullWidth align-right"><i><u>Lupa password?</u></i></a>
							</div>
					</div>
					<!-- <a href="<?=$baseurl;?>/register"><div class="bottom-right-modal">belum memiliki akun?</div></a> -->
				</form>
			</div>
	  </div>
	</div>

	<div id="ModalPoint" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm modal-small">
		<div class="modal-content ">
			<div class="align-center">
				<img src="{{ asset('images/logo/logo_min.png')}}" />
			</div>
			<div class="modal-close">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="modalContent align-center">
				Poin Anda: 
				@if(isset($loggedin))
				<b>{{$loggedin->point}}</b>
				@endif
				poin
			</div>
		</div>
	  </div>
	</div>

	<div id="ModalGift" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm modal-small">
		<div class="modal-content">
			<div class="align-center">
				<img src="{{ asset('images/logo/logo_min.png')}}" />
			</div>
			<div class="modal-close">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="modalContent">
				<div>
					Poin Anda : {{$loggedin->point}}
				</div>
				<div class="giftsWrapper">
					@if(isset($gifts))
						@foreach($gifts as $gift)
							<div class="gift">
								<div class="row">
									<div class="col-md-5">
										<div class="gift_image"> 
											<img src="{{ asset('images/products/'.$gift->thumbnail) }}" />
										</div>
									</div>
									<div class="gift_info col-md-7">
										<div class="gift_info_point">Point : {{$gift->point}}</div>
										{{$gift->name}}
										<div><button class="btn buttonTokita" onclick="getGift({{$gift->id}})">Ambil</button></div>
									</div>
								</div>
							</div>
						@endforeach
					@else
						<div class="noProductWarning noProductWarning-noMargin ">
					        <span>
					            Tidak ada hadiah
					        </span>
					    </div>
					@endif
				</div>
			</div>
		</div>
	  </div>
	</div>

	<div id="ModalCoupon" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm modal-small">
		<div class="modal-content">
			<div class="align-center">
				<img src="{{ asset('images/logo/logo_min.png')}}" />
			</div>
			<div class="modal-close">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				</div>
			<div class="modalContent">
				<div>
					Poin Anda : {{$loggedin->point}}
				</div>
				<div class="giftsWrapper">
					@if(isset($coupons))
						@foreach($coupons as $coupon)
						<div class="gift">
							<div class="row">
								<div class="col-md-5">
									<div class="gift_image">
										<img src="{{ asset('images/products/'. $coupon->thumbnail) }}" />
									</div> 
								</div>
								<div class="gift_info col-md-7">
									<div class="gift_info_point">Point : {{$coupon->point}}</div>
									<div class="couponName">
										Kupon Belanja Rp {{number_format($coupon->nominal, 0, ",", ".")}}
									</div>
									<div>
										<button class="btn buttonTokita" onclick="getCoupon({{$coupon->id}})">AMBIL</button>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					@else
							<div class="noProductWarning noProductWarning-noMargin ">
						        <span>
						            Tidak ada kupon
						        </span>
						    </div>
						@endif
				</div>
			</div>
		</div>
	  </div>
	</div>

	<div id="ModalForgetPassword" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm modal-small">
		<div class="modal-content">
			<div class="align-center">
				<img src="{{ asset('images/logo/logo_min.png')}}" />
			</div>
			<div class="modal-close">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="modalContent align-center">
				<div class="vertical-margin">
					<div>
						Masukkan e-mail yang telah Anda daftarkan, 
					</div>
					<div>
						kami akan segera mengirimkan kata sandi baru
					</div>
				</div>
				<form class="align-center" method="post" action="sendNewPassword">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input class="form-control align-center" placeholder="email Anda" type="email" name="email"></input>
					<button class="pull-right btn buttonTokita" type="submit">kirim</button>
				</form>
			</div>
		</div>
	  </div>
	</div>

	<div id="ModalResetPassword" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm modal-small">
		<div class="modal-content">
			<div class="align-center">
				<img src="{{ asset('images/logo/logo_min.png')}}" />
			</div>
			<div class="modal-close">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="modalContent align-center">
				<div>
					<div>Silakan masukkan email dan kata sandi baru.</div>
					<div class="fontSmall">Untuk mengganti password dapat dilakukan pada menu Akun setelah melakukan sign in</div>
				</div>
				<form class="col-md-12 align-left vertical-margin">
					<table>
						<tr>
							<td class="col-md-4">Password</td>
							<td class="col-md-8"><input class="form-control" placeholder="email Anda" type="password"></input></td>
						</tr>
						<tr class="error" id="errorEmail" style="display: none;">
							<td class="col-md-4"></td>
							<td class="col-md-8">
								<div class="wrongInput">
									Pesan Error
								</div>
							</td>
						</tr>
						<tr>
							<td class="col-md-4">Konfirmasi Password</td>
							<td class="col-md-8"><input class="form-control" placeholder="password dari email" type="password"></input></td>
						</tr>
						<tr class="error" id="errorEmail" style="display: none;">
							<td class="col-md-4"></td>
							<td class="col-md-8">
								<div class="wrongInput">
									Pesan Error
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
							<td class="col-md-9">
								<button class="pull-right btn buttonTokita display-inline-block" type="submit">kirim</button>
							</td>
						</tr>
					</table>
				</form>
				<div class="align-left fontSmall">
					Anda tidak mendapatkan email kami? Cobalah cek folder spam email anda.	
				</div>
				<div class="pull-right bottom-right-modal">
					<a href=""><u>Klik di sini untuk melakukan kirim ulang</u></a>
				</div>
			</div>
		</div>
	  </div>
	</div>

	<div id="ModalChangePassword" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm modal-small">
		<div class="modal-content">
			<div class="align-center">
				<img src="{{ asset('images/logo/logo_min.png')}}" />
			</div>
			<div class="modal-close">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="modalContent align-center">
				<!-- <div>
					<div>Silakan memasukkan email dan kata sandi baru.</div>
					<div>Untuk mengganti password anda dapat dilakukan pada menu Akun setelah melakukan sign in</div>
				</div> -->
				<form class="col-md-12 align-left vertical-margin" method="post" action="changePassword">
					<table>
						@if(isset($loggedin))
						<input type="hidden" name="user_id" value="{{$loggedin->id}}">
						@endif
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<tr>
							<td class="col-md-4">Password Lama</td>
							<td class="col-md-8"><input class="form-control" placeholder="Password lama" type="password" name="old_password"></input></td>
						</tr>
						<tr>
							<td class="col-md-4">Password Baru</td>
							<td class="col-md-8"><input class="form-control" placeholder="Password baru" name="password" type="password" id="password" onkeyup="checkPassword1()"></td>
							<input type="hidden" id="passwordValid" value="false">
						</tr>
						<tr class="error" id="errorPassword" style="display: none;">
							<td class="col-md-4"></td>
							<td class="col-md-8"><div class="wrongInput">Panjang password harus minimal 8 karakter.</div></td>
						</tr>
						<tr>
							<td class="col-md-4">Konfirmasi Password</td>
							<td class="col-md-8"><input class="form-control" placeholder="Password baru" name="confirm_password" type="password" id="confirm_password" onkeyup="checkConfirmPassword1()"></td>
							<input type="hidden" id="confirmPasswordValid" value="false">
						</tr>
						<tr class="error" id="errorConfirmPassword" style="display: none;">
							<td class="col-md-4"></td>
							<td class="col-md-8">
								<div class="wrongInput">
									Password tidak cocok
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
							<td class="col-md-9">
								<button class="pull-right btn buttonTokita display-inline-block" type="submit" id="regButton1">SIMPAN</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	  </div>
	</div>

	@yield('modal')

<!-- modals -->

	<footer class="footer">
	  <div class="container">
	  	<div class="row">
			<div class="col-sm-4 col-md-3">
				<!-- LOGO -->
				<div class="logo-footer">
					<a href="<?=$baseurl;?>/"><img src="{{ asset('images/logo/logo_footer.png') }}" alt="tokita"/></a>
				</div>
			</div>
			<div class="col-sm-3 col-sm-offset-2 col-md-offset-3 col-lg-offset-3">
				<div class="footerSubtitle">
					<span>Kontak Kami</span>
				</div>
				<div class="footerContent">
					Villa Bukit Mas Blok Baru No. 4 <br>
					Bojong Koneng, Cikutra <br>
					Bandung<br>
					022 87788751 / 082231845323
				</div>
			</div>
			<div class="col-md-3 col-lg-3">
				<div class="footerSubtitle">
					<span>Temukan Kami di:</span>
				</div>
				<div class="footerSosMed">
					<!-- icon icon icon icon -->
					<div class="small-size-icon">
						<a href="{{$settings['facebook']}}"><img src="{{ asset('images/icon/facebook.png')}}"></a>
					</div>
					<div class="small-size-icon">
						<a href="{{$settings['twitter']}}"><img src="{{ asset('images/icon/twitter.png')}}"></a>	
					</div>
					<div class="small-size-icon">
						<a href="{{$settings['google']}}"><img src="{{ asset('images/icon/google.png')}}"></a>
					</div>
					<div class="small-size-icon">
						<a href="{{$settings['instagram']}}"><img src="{{ asset('images/icon/instagram.png')}}"></a>
					</div>
				</div>
				<div class="footerContent">
					<a href="{{URL::to('/about')}}"><b class="display-block">Tentang Tokita</b></a>
					<a href="{{URL::to('/faq')}}"><b class="display-block">FAQ</b></a>
				</div>
			</div>
		</div>
	  </div>
	</footer>
	
	@yield('scripts-bottom')
	<script>
		$(document).ready(function() {
		  	$('.headerLogo--shrink').css('display','none');
			var carousel = $(".carousel");
			carousel.owlCarousel({
				slideSpeed : 300,
				center : true,
				loop : true,
				// pagination : true,
				paginationSpeed : 400,
				autoHeight : false,
				// autoWidth : true,
				autoPlay : true,
				stopOnHover : true,
				singleItem : true,
				paginationNumber : true,
				itemsScaleUp:true
			});
			$(".next").click(function(e){
				e.preventDefault();
				carousel.trigger('owl.next');
			})
			$(".prev").click(function(e){
				e.preventDefault();
				carousel.trigger('owl.prev');
			})
		});

	</script>

	<script type="text/javascript">
		$(function() {
	  var shrinkHeader = 50;
	  $(window).scroll(function() {
		var scroll = getCurrentScroll();
		if (scroll >= shrinkHeader) {
		  $('.header').addClass('shrink');
		  $('.smallIcon').addClass('shrink');
		  $('.navTopMargin').addClass('shrink');
		  $('.pageTitle').addClass('shrink');  
		  $('.dropdown-menu').addClass('shrink');
		  // $('.disappear-on-shrink').addClass('shrink');
		  // $('.headerLogo').css('display','none');
		  // $('.headerLogo--shrink').css('display','block');

		  $('.headerLogo').fadeOut(150, function(){
		      $('.headerLogo--shrink').fadeIn(150);
		   });
		  // $('.headerLogo--shrink').find('img').slideDown();
		} else {
		  $('.header').removeClass('shrink');
		  $('.smallIcon').removeClass('shrink');
		  $('.navTopMargin').removeClass('shrink');
		  $('.pageTitle').removeClass('shrink');
		  $('.dropdown-menu').removeClass('shrink');
		  // $('.disappear-on-shrink').removeClass('shrink');
		  // $('.headerLogo').css('display','block');
		  // // $('.headerLogo--shrink').find('img').slideUp();
		  // $('.headerLogo--shrink').css('display','none');
		  $('.headerLogo--shrink').fadeOut(150, function(){
		      $('.headerLogo').fadeIn(150);
		   });
		}
	  });

	  function getCurrentScroll() {
		return window.pageYOffset || document.documentElement.scrollTop;
	  }
	});
	</script>
	
	<script type="text/javascript">
		
		function checkPassword1(){
			var content = document.getElementById("password").value;
			if(content.length < 8){
				$( '#errorPassword' ).show();
				document.getElementById("passwordValid").value = "false";
			} else{
				$( '#errorPassword' ).hide();
				document.getElementById("passwordValid").value = "true";
			}
			checkConfirmPassword1();
			invalidateButton1();
		}

		function checkConfirmPassword1(){
			var content = document.getElementById("password").value;
			var content2 = document.getElementById("confirm_password").value;
			if(content != content2){
				$( '#errorConfirmPassword' ).show();
				document.getElementById("confirmPasswordValid").value = "false";
			} else{
				$( '#errorConfirmPassword' ).hide();
				document.getElementById("confirmPasswordValid").value = "true";
			}
			invalidateButton1();
		}

		function invalidateButton1(){
			var isValid = (document.getElementById("passwordValid").value == "true")&&
						  (document.getElementById("confirmPasswordValid").value == "true");
			if(isValid){
				$('#regButton1').removeAttr('disabled');
			}
			else{
				$('#regButton1').attr('disabled','disabled');
			}
		}

		$(document).ready(function() {
		     $('#regButton1').attr('disabled','disabled');
		 });
	</script>
	@if(isset($loggedin))
	<script type="text/javascript">
		function getCoupon(couponId){
			window.location="{{URL::to('/coupon/get/')}}"+"/"+couponId;
		}
		function getGift(giftId){
			window.location="{{URL::to('/gift/get/')}}"+"/"+giftId;
		}
	</script>
	@endif

<!-- slide menu -->
	<script type="text/javascript">
	$(function() {     
		var menu = $('.menuicon img');
		var menuSlider = $('.menuSlider');
		var overlay = $('.overlay');

		menu.click(function(e) {
			e.preventDefault();
			if(!menuSlider.hasClass('active')) {
				menuSlider.addClass('active');
				overlay.addClass('overlayactive');
			} else {
				menuSlider.removeClass('active');
				overlay.removeClass('overlayactive');
			}
		}); 
	});

	$(document).ready(function() {
		var menuSliderLength = $('.menuSlider').children().width();
		$('.menuSlider').find('.menuDashboard').find('nav').find('ul').find('li').find('ul').find('li').slideDown();
		$('.menuSlider').find('.menuDashboard').find('nav').find('ul').find('li').find('ul').find('li').slideUp();
		$('.menuSlider').find('.headerButtonAccount-mobile').css("width",menuSliderLength);
		$('.menuSlider').find('.menuDashboard').find('nav').find('ul').find('ul').css("min-width",menuSliderLength);
		$('.fullWidthMenu').css("min-width",menuSliderLength);
		$('.menuSlider').find('.menuDashboard').find('nav').find('ul').find('li').click(function(){
			if(!($(this).find('ul').find('li').is(':visible'))){
				$(this).find('ul').find('li').slideDown();				
			}
			else{
				$(this).find('ul').find('li').slideUp();
			}
		});
		@if(Request::url() === URL::to('/'))
		    $('#ModalComingSoon').modal('show');
		@endif
	});
	$(window).resize(function() {
		var menuSliderLength = $('.menuSlider').children().width();
		$('.menuSlider').find('.headerButtonAccount-mobile').css("width",menuSliderLength);
		$('.menuSlider').find('.menuDashboard').find('nav').find('ul').find('ul').css("min-width",menuSliderLength);
		$('.fullWidthMenu').css("min-width",menuSliderLength);
	});	
	</script>

<!-- close menu slider everywhere -->
	<script type="text/javascript">
	$(function() {     
		var doc = $('.overlay');
		var menuSlider = $('.menuSlider');

		doc.click(function(e) {
			e.preventDefault();
			if(menuSlider.hasClass('active')) {
				menuSlider.removeClass('active');
				doc.removeClass('overlayactive');
			}
		}); 
	});
	</script>
<!-- auto size menu dashboard -->
	<script type="text/javascript">
		$(document).ready(function() {
			var length = $('.buttonNamaAkun-desktop').outerWidth();
			$('.menuDashboard').find('nav').find('ul').find('ul').css("width", length);
			// $('.menuItems').find('nav').find('ul').find('ul').css("display", length); 
		});
		$(window).resize(function() {
			var length = $('.buttonNamaAkun-desktop').outerWidth();
			$('.menuDashboard').find('nav').find('ul').find('ul').css("width", length);
			// $('.menuItems').find('nav').find('ul').find('ul').css("display", length);
		});
	</script>
<!-- square-sizer -->
<script type="text/javascript">
	$(document).ready(function(){
		var w = $('.productPanel').parent().width();
		$('.productImageWrapper').css("height",w);
	});
	$(window).resize(function(){
		var w = $('.productPanel').parent().width();
		$('.productImageWrapper').css("height",w);
	});
</script>
<!-- force footer to the bottom -->
<script type="text/javascript">
    $(document).ready(function(){
        var footerH = $('footer').outerHeight();
        var pageH = $(window).height();
        var headerH = $('.headersmall').outerHeight();
        var height = pageH-footerH-headerH;
        $('.bodywrapper').css("min-height", height);
    });
    $(window).resize(function(){
        var footerH = $('footer').outerHeight();
        var pageH = $(window).height();
        var headerH = $('.headersmall').outerHeight();
        var height = pageH-footerH-headerH;
        $('.bodywrapper').css("min-height", height);
    });
</script>
<!-- growl -->
@if(isset($error_message))
<script type="text/javascript">
	$(function(){
		setTimeout(function() {
	        $.bootstrapGrowl("{{$error_message}}", {
	            type: 'danger',
	            align: 'center',
	            width: 'auto',
	            offset: {from: 'top', amount: 120},
	            allow_dismiss: true,
	        });
	    }, 100);	
	});
</script>
@endif
<!-- search on small device -->
<script>
$(window).load(function(){
    $(".headerSmallSearchWrapper").slideUp();
    $('.searchbuttonsmall').click(function(e){
		e.preventDefault();
        if ($('.headerSmallSearchWrapper').is(':visible')){
        	$(".headerSmallSearchWrapper").slideUp();
        }else{
        	$(".headerSmallSearchWrapper").slideDown();
        }
        $('.headerSmallSearchWrapper').find('input').focus(function(){
        	$(".headerSmallSearchWrapper").slideDown();
        })
    });
});
$(window).resize(function(){
    // $(".headerSmallSearchWrapper").slideUp();
});
</script>
@if(isset($success_message))
<script type="text/javascript">
	$(function(){
		setTimeout(function() {
	        $.bootstrapGrowl("{{$success_message}}", {
	            type: 'success',
	            align: 'center',
	            width: 'auto',
	            offset: {from: 'top', amount: 120},
	            allow_dismiss: true,
	        });
	    }, 100);	
	});
</script>
@endif

<script>
	$('.discounted_product').click(function(){
		$('.amount').val(1);
		$('.detailProductPrice').html($(this).data('displayed_price'));
		$('.detailProductName').html($(this).data('name'));
		if($(this).data('thumbnail') == "{{URL::to('/images/products')}}"){
			$('.detailProductImage').html('<img src="'+"http://placehold.it/200x200?text=No+image"+'"/>');
		}
		else{
			$('.detailProductImage').html('<img src="'+$(this).data('thumbnail')+'"/>');	
		}
		
		$('.detailProductDesc').html($(this).data('description'));
		$('#item_id').val($(this).data('id'));
		var url = "{{URL::to('/product/addView/')}}"+"/"+$(this).data('id');
		$.ajax(url);
	});
</script>


@if(!isset($loggedin))
<script type="text/javascript">
	$('.cart').click(function(e){
		e.preventDefault();
		$('#ModalLogin').modal('show');
	});
	$('.fav').click(function(e){
		e.preventDefault();
		$('#ModalLogin').modal('show');
	});
</script>
@endif
</body>
</html>
