<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('quantity');
			$table->integer('customer_id')->unsigned();
			$table->integer('item_id')->unsigned();
			$table->integer('status');
			$table->foreign('customer_id')
			      ->references('id')->on('customers')
			      ->onDelete('cascade');
			$table->foreign('item_id')
			      ->references('id')->on('items')
			      ->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carts');
	}

}
