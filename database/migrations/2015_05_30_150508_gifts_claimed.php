<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GiftsClaimed extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claimed_gifts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('gift_id')->unsigned();
			$table->foreign('gift_id')
			      ->references('id')->on('gifts')
			      ->onDelete('cascade');
			$table->integer('customer_id')->unsigned();
			$table->foreign('customer_id')
			      ->references('id')->on('customers')
			      ->onDelete('cascade');
			$table->date('date_claimed');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claimed_gifts');
	}

}
