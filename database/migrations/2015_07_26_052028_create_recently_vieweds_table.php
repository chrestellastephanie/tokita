<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecentlyViewedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recently_vieweds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customer_id')->unsigned();
			$table->integer('item_id')->unsigned();
			$table->dateTime('last_seen');
			$table->foreign('customer_id')
			      ->references('id')->on('customers')
			      ->onDelete('cascade');
			$table->foreign('item_id')
			      ->references('id')->on('items')
			      ->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recently_vieweds');
	}
}
