<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimedCouponsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claimed_coupons', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customer_id')->unsigned();
			$table->foreign('customer_id')
			      ->references('id')->on('customers')
			      ->onDelete('cascade');
			$table->integer('coupon_id')->unsigned();
			$table->foreign('coupon_id')
			      ->references('id')->on('coupons')
			      ->onDelete('cascade');
			$table->integer('status');
			$table->string('identifier');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claimed_coupons');
	}

}
