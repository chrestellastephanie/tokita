<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAmount extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('gifts', function($table)
		{
		    $table->dropColumn('expired_at');
		    $table->dropColumn('amount');
		    $table->dropColumn('customer_id');
		    $table->dropColumn('product_id');
		});

		Schema::table('gifts', function($table)
		{
		    $table->integer('point');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
