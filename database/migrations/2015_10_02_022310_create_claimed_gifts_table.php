<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimedGiftsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claimed_gifts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('customer_id')->unsigned();
			$table->foreign('customer_id')
			      ->references('id')->on('customers')
			      ->onDelete('cascade');
			$table->integer('gift_id')->unsigned();
			$table->foreign('gift_id')
			      ->references('id')->on('gifts')
			      ->onDelete('cascade');
			$table->integer('status');
			$table->string('identifier');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claimed_gifts');
	}
}
