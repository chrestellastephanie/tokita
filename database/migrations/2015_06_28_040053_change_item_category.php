<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeItemCategory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('items', function($table)
		{
		    $table->integer('category_id')->unsigned();
			$table->integer('subcategory_id')->unsigned();
			$table->foreign('category_id')
				      ->references('id')->on('categories')
				      ->onDelete('cascade');
			$table->foreign('subcategory_id')
				      ->references('id')->on('subcategories')
				      ->onDelete('cascade');
			$table->dropColumn('item_category');
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('items', function($table)
		{
			$table->dropForeign('items_category_id_foreign');
			$table->dropForeign('items_subcategory_id_foreign');
			$table->dropColumn('category_id');
			$table->dropColumn('subcategory_id');
			$table->integer('item_category');
		});
	}

}
